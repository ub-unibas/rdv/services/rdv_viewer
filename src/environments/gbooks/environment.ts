import {
  ExtraInfoFieldType,
  FacetFieldType,
  FacetValueOrder,
  HistogramFieldModel,
  HistogramFieldType,
  initRdvLib,
  MiradorPlugins,
  SettingsModel,
  SortOrder,
} from '@rdv-lib';

import {environment as autographen} from '@env/autographen/environment';

export const environment: SettingsModel = {
  ...autographen,
  scaleDocThumbsByIiif: false,

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: true
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "Relevanz"
    },
    {
      field: "year",
      order: SortOrder.ASC,
      display: "Erscheinungsjahr"
    }
  ],

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "Objekttyp": {
      "field": "type.keyword",
      "facetType": FacetFieldType.CHECKBOX,
      "label": "Objekt / AutorIn",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10,
      "initiallyOpen": true,
      "help": "env.facetFields.type.help"
    },
    "Bibliothek": {
      "field": "lib.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Bibliothek",
      "operator": "OR",
      "order": 10,
      "expandAmount": 10,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL],
      "help": "env.facetFields.lib.help"
    },
    "Datenanalyse": {
      "field": "tag.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Datenanalyse",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 11,
      "expandAmount": 10,
      "size": 100,
    },
    "hierarchy_filter": {
      "field": "hierarchy_filter.keyword",
      "facetType": FacetFieldType.HIERARCHIC,
      "label": "MARC-Felder Struktur",
      "operator": "AND",
      "order": 12,
      "size": 100,
      "expandAmount": 100,
      "help": "env.facetFields.marc_struktur.help"
    },
    "Datenanalyse MarcFelder": {
      "field": "record_fields.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Marc-Felder",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 12,
      "expandAmount": 50,
      "size": 300,
      "searchWithin": true,
    },
    "Datenanalyse MarcSubFelder": {
      "field": "record_fields_subcodes.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Marc-SubFelder",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 13,
      "expandAmount": 50,
      "size": 1000,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "record_fields_count": {
      "field": "record_fields_count",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Anzahl Marc-Felder",
      "operator": "AND",
      "showAggs": true,
      "order": 14,
      "size": 31,
      "expandAmount": 31,
    } as HistogramFieldModel,
    "record_length": {
      "field": "record_length",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Länge MarcXML",
      "operator": "AND",
      "showAggs": true,
      "order": 15,
      "size": 31,
      "expandAmount": 31,
    } as HistogramFieldModel,
    "leadercode_data": {
      "field": "leadercode_data.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Marc-Leadercode",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 16,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "oai_datestamp": {
      "field": "oai_datestamp",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "OAI Zeitstempel",
      "operator": "AND",
      "showAggs": true,
      "order": 17,
      "size": 31,
      "expandAmount": 31,
      "help": "env.facetFields.oai_datestamp.help"
    } as HistogramFieldModel,
    "other_ids": {
      "field": "035_a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "IDs 035a",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 45,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL],
      "help": "env.facetFields.bib.help"
    },
    "title": {
      "field": "title.label.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Titel",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 46,
      "expandAmount": 20,
      "size": 100,
      "searchWithin": true,
      "autocomplete_size": 3,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "author": {
      "field": "author.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "AutorIn",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 50,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "autocomplete_size": 3,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL],
      "help": "env.facetFields.author.help"
    },
    "DruckerIn": {
      "field": "Drucker.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "DruckerIn",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 51,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "Druck-/Verlagsort": {
      "field": "751_a.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Druck-/Verlagsort (751_a)",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 52,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },

    "year": {
      "field": "year",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Erscheinungsjahr",
      "operator": "AND",
      "showAggs": true,
      "order": 53,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "Sprache": {
      "field": "lang.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Sprache",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 54,
      "expandAmount": 10,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "Gattung": {
      "field": "655_a_7.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Gattung (655_a_7)",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 55,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
    },

    "340_a": {
      "field": "340_a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Material (340_a)",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 56,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
    },

    "990_c": {
      "field": "990_c.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "lokaler Code (990_c)",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 57,
      "expandAmount": 20,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "Signatur": {
      "field": "split_signatur.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Signatur / Signaturgruppen",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 58,
      "expandAmount": 20,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "Umfang": {
      "field": "300_a_norm.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Datenmuster Umfang (300_a)",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 59,
      "expandAmount": 20,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT]
    },
    "Masse": {
      "field": "300_c_norm.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Datenmuster Maße (300_c)",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 60,
      "expandAmount": 20,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT]
    },

    "Anmerkung": {
      "field": "500_a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Anmerkung (500_a)",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 61,
      "expandAmount": 20,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "Bib Nachweis": {
      "field": "510_a_norm.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Bibliographischer Nachweis (510_a)",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 62,
      "expandAmount": 20,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "Literatur": {
      "field": "581_a_norm.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Literatur (581_a)",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 63,
      "expandAmount": 20,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "Exemplar": {
      "field": "590561563_a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Anmerkungen zum Exemplar (590561563_a)",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 64,
      "expandAmount": 20,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
    },

    "Personeneintrag (alle Rollen)": {
      "field": "700_abcd.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Personeneintrag (alle Rollen) (700_abcd)",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 66,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "Koerperschaftseintrag (alle Rollen)": {
      "field": "710_a.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Koerperschaftseintrag (alle Rollen) (710_a)",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 67,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "rel_persons2": {
      "field": "rel_persons2",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Personeneintrag Rolle",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 68,
      "expandAmount": 100,
      "size": 100,
      "help": "env.facetFields.rel_persons2.help"
    },
    "rel_persons2 Auswahl": {
      "field": "rel_persons2.scr.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Personeneintrag Auswahl",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 69,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
    },
    "digitalisiert": {
      "field": "digitalisiert.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Digitalisiert",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 70,
      "size": 100,
      "expandAmount": 5,
    },
    "910_c": {
      "field": "910_c.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Rechte (910_c)",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 71,
      "size": 100,
      "expandAmount": 5,
    },
    "Verlagsort_frei": {
      "field": "264_a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Verlagsort (frei) (264_a)",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1100,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "Verlags": {
      "field": "264_b.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Verlag 264b",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1100,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "Katalogisierung": {
      "field": "040_aed.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Katalogisierung (040_aed)",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1100,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "Dewey": {
      "field": "082_a2.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Dewey (082_a2)",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1100,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "Subject": {
      "field": "072_a2.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Subject (072_a2)",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1100,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "019": {
      "field": "019_a5.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "019_a5",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1100,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "900": {
      "field": "900_a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "900a",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1100,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "990f": {
      "field": "990_f.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "990f",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1100,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "650_a": {
      "field": "650_a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "650_a",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1100,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "505_t": {
      "field": "505_t.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "505_t",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1100,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "250_a": {
      "field": "250_a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "250_a",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1100,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "490_a": {
      "field": "490_a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "490_a",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1100,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "502_a": {
      "field": "502_a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "502_a",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1100,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "246_a": {
      "field": "246_a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "246_a",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1100,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "651_a": {
      "field": "651_a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "651_a",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1100,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "690_a": {
      "field": "690_a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "690_a",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1100,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "648_a": {
      "field": "648_a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "648_a",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1100,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "800_t": {
      "field": "800_t.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "800_t",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1100,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "series": {
      "field": "series.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "series",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1100,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "Geschlecht": {
      "field": "geschlecht.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "GND Geschlecht",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10023,
      "size": 100,
      "expandAmount": 10,
      "help": "env.facetFields.gnd.help"
    },
    "Land": {
      "field": "land.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "GND Land",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 10028,
      "size": 100,
      "expandAmount": 30,
    },
    "Berufe": {
      "field": "berufe.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "GND Beruf",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10024,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "Geburtsort": {
      "field": "geburtsort.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "GND Geburtsort",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 10025,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "Wirkungsort": {
      "field": "wirkungsort.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "GND Wirkungsort",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 10026,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "Sterbeort": {
      "field": "sterbeort.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "GND Sterbeort",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 10027,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "wiki_categories": {
      "field": "wiki_categories.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Wikipedia Kategorien",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10032,
      "size": 100,
      "expandAmount": 30,
      "searchWithin": true,
    },
    "wiki_orig_categories": {
      "field": "wiki_orig_categories.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Wikipedia vollst. Kategorien",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10032,
      "size": 100,
      "expandAmount": 30,
      "searchWithin": true,
    },
    "wiki_len": {
      "field": "wiki_length",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Länge Datensatz Wikipedia",
      "operator": "AND",
      "showAggs": true,
      "order": 10033,
      "size": 16,
      "expandAmount": 31,
      "help": "env.facetFields.wiki_length.help"
    } as HistogramFieldModel,
    "wiki_headers": {
      "field": "wiki_headers.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Wikipedia Ueberschriften",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10034,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "help": "env.facetFields.wiki_headers.help"
    },
    "Quellen_Wikidata": {
      "field": "wikidata_sources.label.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "in Wikidata verzeichnete Quellen",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10035,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "Quellen": {
      "field": "quellen.label.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "in der GND verzeichnete Quellen",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10036,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
  },


  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {},

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [5, 10, 25, 50],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 10,
    "offset": 0,
    "sortField": "_score",
    "sortOrder": SortOrder.DESC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
      "fct_location": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "fct_location",
      "label": "Geographikum"
      }
      },

  searchResultSettings: {
    showHitNumber: false,
    showHitObjectType: true,
    paginationMethod: "bar",
    showKeepFiltersButton: false,
    pageButtonsInBar: 5, // go to last page and check if there is no overflow!
    // order of view style buttons (left top side on the search result page)
    viewStyleButtons: [
      // "PAGE_SIZE_SELECTOR",
      //"LIST_STYLE_BUTTON",
      //"IIIF_SELECTOR",
      //"IIIF_BUTTON_GROUP"
    ]
  },

  i18n: {
    "de": {
      "top.headerSettings.name": "Google Books Candidates",
      "top.headerSettings.name.Dev": "Google Books Candidates (Dev)",
      "top.headerSettings.name.Loc": "Google Books Candidates(Loc)",
      "top.headerSettings.name.Test": "Google Books Candidates (Test)",
      "top.headerSettings.betaBarContact.name": "Martin Reisacher",
      "top.headerSettings.betaBarContact.email": "martin.reisacher@unibas.ch",
      "env.facetFields.wiki_length.help": "Zeichenanzahl der HTMl-Wikipedia Seite",
      "env.facetFields.wiki_headers.help": "Ueberschriften-Kategorien in der Wikipedia",
      "env.facetFields.marc_struktur.help": "vorhandene Marc bzw. Subfelder in einem Record (aufgeteilt auch nach Indikatoren), hierarchisch organisert",
      "env.facetFields.type.help": "Objektkategorie: Personeneintrag oder Bib-Eintrag, hat Auswirkung auf Anzeige der vorhandenen Facetten",
      "env.facetFields.lib.help": "laut Google Books Candidates Listen",
      "env.facetFields.oai_datestamp.help": "wann wurde der Record das letzte Mal via OAI PMH publiziert",
      "env.facetFields.author.help": "über die Facette AutorIn sind AutorInnen und Bib-Objekte auffindbar",
      "env.facetFields.bib.help": "Die folgenden Facetten beziehen sich auf die bibliographischen Objekte",
      "env.facetFields.gnd.help": "Die folgenden Facetten beziehen sich auf die AutorInnen-Objekte",
      "env.facetFields.rel_persons2.help": "Sobald ein Wert ausgewählt wird, taucht daunter eine neue Facette auf, " +
        "aus der ein Personeintrag ausgewählt werden kann"
    },
    "en": {
      "top.headerSettings.name": "Google Books Candidates",
      "top.headerSettings.name.Dev": "Google Books Candidates (Dev)",
      "top.headerSettings.name.Loc": "Google Books Candidates (Loc)",
      "top.headerSettings.name.Test": "Google Books Candidates (Test)",
      "top.headerSettings.betaBarContact.name": "Martin Reisacher",
      "top.headerSettings.betaBarContact.email": "martin.reisacher@unibas.ch",
    }
  },

};

initRdvLib(environment, [MiradorPlugins.PLUGIN_OCR_HELPER]);
