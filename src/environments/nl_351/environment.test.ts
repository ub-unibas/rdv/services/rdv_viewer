
const Proj = "nl_351-test";


import {environment as proj} from '@env/nl_351/environment';
import {environment as test} from '@env_temp/environment.type-test';
import {addTestNamePostfix} from "@env_temp/util";
import {initRdvLib, SettingsModel} from "@rdv-lib";

export const environment: SettingsModel = {
  ...test,
  ...proj,

  headerSettings: addTestNamePostfix(proj.headerSettings),

  proxyUrl : test.proxyUrl +  Proj + "/",
  moreProxyUrl: test.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: test.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: test.detailProxyUrl +  Proj + "/",
  documentViewerProxyUrl: test.documentViewerProxyUrl +  Proj + "/",
  navDetailProxyUrl: test.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: test.popupQueryProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: test.suggestSearchWordProxyUrl +  Proj + "/",
};
initRdvLib(environment);
