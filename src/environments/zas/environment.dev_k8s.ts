const Proj = "zas-dev_k8s";

import {initRdvLib, SettingsModel} from '@rdv-lib';
import {environment as proj} from '@env/afrikaportal/environment';
import {environment as dev} from '@env_temp/environment.type-dev_k8s';
import {addTestNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {

  ...dev,
  ...proj,
  headerSettings: addTestNamePostfix(proj.headerSettings),

  proxyUrl : "https://rdv-querybuilder-zas.ub-dd-prod.k8s-001.unibas.ch/v2/rdv_query/es_proxy/" +  Proj + "/",
  moreProxyUrl: "https://rdv-querybuilder-zas.ub-dd-prod.k8s-001.unibas.ch/v2/rdv_query/further_snippets/" +  Proj + "/",
  inFacetSearchProxyUrl: "https://rdv-querybuilder-zas.ub-dd-prod.k8s-001.unibas.ch/v2/rdv_query/facet_search/" +  Proj + "/",
  detailProxyUrl: "https://rdv-querybuilder-zas.ub-dd-prod.k8s-001.unibas.ch/v2/rdv_object/object_view/" +  Proj + "/",
  documentViewerProxyUrl: "https://rdv-querybuilder-zas.ub-dd-prod.k8s-001.unibas.ch/v2/rdv_query/iiif_flex_pres/" +  Proj + "/",
  navDetailProxyUrl: "https://rdv-querybuilder-zas.ub-dd-prod.k8s-001.unibas.ch/v2/rdv_query/next_objectview/" +  Proj + "/",
  popupQueryProxyUrl: "https://rdv-querybuilder-zas.ub-dd-prod.k8s-001.unibas.ch/v2/rdv_query/popup_query/" +  Proj + "/",
  suggestSearchWordProxyUrl: undefined
};

initRdvLib(environment);
