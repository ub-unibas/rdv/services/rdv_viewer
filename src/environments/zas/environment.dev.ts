import {initRdvLib, SettingsModel} from '@rdv-lib';
import {environment as proj} from '@env/zas/environment';
import {environment as dev} from '@env_temp/environment.type-dev';
import {addDevNamePostfix} from "@env_temp/util";

const Proj = "zas-kc";

export const environment: SettingsModel = {
  ...dev,
  ...proj,

  headerSettings: addDevNamePostfix(proj.headerSettings),

  proxyUrl : "https://ub-zastest-proxy.ub.unibas.ch/v2/rdv_query/es_proxy/" +  Proj + "/",
  moreProxyUrl: "https://ub-zastest-proxy.ub.unibas.ch/v2/rdv_query/further_snippets/" +  Proj + "/",
  inFacetSearchProxyUrl: "https://ub-zastest-proxy.ub.unibas.ch/v2/rdv_query/facet_search/" +  Proj + "/",
  detailProxyUrl: "https://ub-zastest-proxy.ub.unibas.ch/v2/rdv_object/object_view/" +  Proj + "/",
  documentViewerProxyUrl: "https://ub-zastest-proxy.ub.unibas.ch/v2/rdv_query/iiif_flex_pres/" +  Proj + "/",
  navDetailProxyUrl: "https://ub-zastest-proxy.ub.unibas.ch/v2/rdv_query/next_objectview/" +  Proj + "/",
  popupQueryProxyUrl: "https://ub-zastest-proxy.ub.unibas.ch/v2/rdv_query/popup_query/" +  Proj + "/",
  detailSuggestionProxyUrl: "https://ub-zastest-proxy.ub.unibas.ch/v2/rdv_query/form_query/" +  Proj + "/",

};

initRdvLib(environment);
