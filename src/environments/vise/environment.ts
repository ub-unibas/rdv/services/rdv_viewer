import {
  Backend,
  FacetFieldType,
  FacetValueOrder,
  HistogramFieldModel,
  HistogramFieldType, initRdvLib,
  SettingsModel,
  SortOrder
} from '@rdv-lib'

export const environment: SettingsModel = {
  production: false,

  showSimpleSearch: true,
  showImagePreview: true,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "http://ub-vise.ub.unibas.ch/",

  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,


  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      disableLanguageBar: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: false
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "prio.keyword",
      order: SortOrder.DESC,
      display: "select-sort-fields.prio_desc"
    },
    {
      field: "dateModified",
      order: SortOrder.DESC,
      display: "select-sort-fields.date_desc"
    }
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {

    "Aktualisiert": {
      "field": "dateModified",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Aktualisiert",
      "operator": "AND",
      "showAggs": true,
      "order": 9,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "Tags": {
      "field": "tags.tag.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Fachrichtungen",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 3,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL,
      "hidden": false
    },
    "hierarchy_filter": {
      "field": "hierarchy_filter.keyword",
      "facetType": FacetFieldType.HIERARCHIC,
      "label": "Semester / Lehrveranstaltung",
      "operator": "AND",
      "order": 0,
      "size": 100,
      "expandAmount": 100,
      "initiallyOpen": true
    },
  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {},

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [5, 10, 20, 50],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 10,
    "offset": 0,
    "sortField": "prio.keyword",
    "sortOrder": SortOrder.DESC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "prio.keyword",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {},

  searchResultSettings: {
    showHitNumber: false,
    showHitObjectType: true,
    paginationMethod: "bar",
    showKeepFiltersButton: false,
    pageButtonsInBar: 5, // go to last page and check if there is no overflow!
    // order of view style buttons (left top side on the search result page)
    viewStyleButtons: [
      // "PAGE_SIZE_SELECTOR",
      //"LIST_STYLE_BUTTON",
      //"IIIF_SELECTOR",
      //"IIIF_BUTTON_GROUP"
    ]
  },

  i18n: {
    "de": {
      "select-sort-fields.prio_desc": "Relevanz",
      "select-sort-fields.date_desc": "Aktualisiert (neueste)",
      "top.headerSettings.name": "Virtueller Semesterapparat Medizin",
      "top.headerSettings.name.Dev": "Virtueller Semesterapparat Medizin (Dev)",
      "top.headerSettings.name.Loc": "Virtueller Semesterapparat Medizin (Loc)",
      "top.headerSettings.name.Test": "Virtueller Semesterapparat Medizin (Test)",
      "top.headerSettings.betaBarContact.name": "UB Medizin",
      "top.headerSettings.betaBarContact.email": "info-med@unibas.ch",
      "top.headerSettings.logoSubTitle": "Universitätsbibliothek Medizin",
      "top.headerSettings.departmentUrl": "https://ub.unibas.ch/de/ub-medizin/",

    },
    "en": {
      "select-sort-fields.prio_desc": "Relevanz",
      "select-sort-fields.date_desc": "Aktualisiert (neueste)",
      "top.headerSettings.name": "Virtueller Semesterapparat Medizin",
      "top.headerSettings.name.Dev": "Virtueller Semesterapparat Medizin (Dev)",
      "top.headerSettings.name.Loc": "Virtueller Semesterapparat Medizin (Loc)",
      "top.headerSettings.name.Test": "Virtueller Semesterapparat Medizin (Test)",
      "top.headerSettings.betaBarContact.name": "UB Medizin",
      "top.headerSettings.betaBarContact.email": "info-med@unibas.ch",
      "top.headerSettings.logoSubTitle": "Universitätsbibliothek Medizin",
      "top.headerSettings.departmentUrl": "https://ub.unibas.ch/de/ub-medizin/",

    },
  },

};

initRdvLib(environment);

