import { environment as zas } from "@env/zas/environment";
import {
  FacetFieldType, FacetValueOrder,
  HistogramFieldModel,
  HistogramFieldType,
  initRdvLib,
  SettingsModel,
} from "@rdv-lib";

export const environment: SettingsModel = {
  ...zas,
  baseUrl: "https://ub-zas_int.ub.unibas.ch/",
  editable: true,
  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  embeddedIiifViewer: "right",
  narrowEmbeddedIiiFViewer: false,

  facetFields: {

    "Farbcode": {
      "field": "Farbcode.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Bearbeitungscode",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 0,
      "size": 100,
      "expandAmount": 10
    } ,
    ...zas.facetFields,

    "ki_descr_sach_prefLabel": {
      "field": "ki_stw_ids.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Themen KI (experimentell)",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 100,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT]
    },
    "ki_descr_place": {
      "field": "ki_descr_place.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Ort KI (experimentell)",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 101,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT]
    },
    "hide_from_public": {
      "field": "hide_from_public",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Ausblenden",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 105,
      "size": 100,
      "expandAmount": 10,
    },
    "sys_created": {
      "field": "sys_created",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Datum Verarbeitung",
      "operator": "AND",
      "showAggs": true,
      "order": 106,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "Systemnummer": {
      "field": "Systemnummer.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Systemnummer",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 110,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT]
    },
    "bsiz_id": {
      "field": "bsiz_id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "SLSP IZ-ID",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 111,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT]
    },
    "old_sysid": {
      "field": "old_sysid.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "alte Systemnummer",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 112,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT]
    },

    "setid": {
      "field": "setid.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "SetID",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 113,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT]
    },
    "zas_id": {
      "field": "zas_id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "ZAS ID",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 114,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT]
    },

    "Datenanalyse": {
      "field": "tag.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Datenanalyse",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 120,
      "expandAmount": 10,
      "size": 100,
    },
    "Bemerkung": {
      "field": "Bemerkung.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Bemerkung",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 120,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
    },
    "990_f": {
      "field": "990_f.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "990_f",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 122,
      "size": 100,
      "expandAmount": 20,
    },

    "Geschlecht": {
      "field": "geschlecht.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "GND Geschlecht",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10023,
      "size": 100,
      "expandAmount": 10,
      "help": "env.facetFields.gnd.help"
    },
    "Land": {
      "field": "land.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "GND Land",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10028,
      "size": 100,
      "expandAmount": 30,
    },
    "Berufe": {
      "field": "berufe.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "GND Beruf",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10024,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "Geburtsort": {
      "field": "geburtsort.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "GND Geburtsort",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10025,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "Wirkungsort": {
      "field": "wirkungsort.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "GND Wirkungsort",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10026,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "Sterbeort": {
      "field": "sterbeort.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "GND Sterbeort",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10027,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },

    "wiki_categories": {
      "field": "wiki_categories.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Wikipedia Kategorien",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10032,
      "size": 100,
      "expandAmount": 30,
      "searchWithin": true,
    },
    "wiki_orig_categories": {
      "field": "wiki_orig_categories.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Wikipedia vollst. Kategorien",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10032,
      "size": 100,
      "expandAmount": 30,
      "searchWithin": true,
    },
    "wiki_headers": {
      "field": "wiki_headers.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Wikipedia Ueberschriften",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10034,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "help": "env.facetFields.wiki_headers.help"
    },
    "Quellen_Wikidata": {
      "field": "wikidata_sources.label.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "in Wikidata verzeichnete Quellen",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10035,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "Quellen": {
      "field": "quellen.label.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "in der GND verzeichnete Quellen",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10036,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "wikipedia_relations": {
      "field": "wikipedia_relations.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "in Wikipedia enthaltene Verweise",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10036,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "wikipedia_int_relations": {
      "field": "wikipedia_int_relations.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "in Wikipedia enthaltene Zas interne Verweise",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10036,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    }
  },
  i18n: {
    de: {
      "header.logout": "Logout",
      "env.facetFields.remove_all": "Alle Facetten entfernen",
      "top.headerSettings.name": "Rechercheportal Zeitungsausschnitte intern",
      "top.headerSettings.name.Dev":
        "Rechercheportal Zeitungsausschnitte intern (Dev)",
      "top.headerSettings.name.Loc":
        "Rechercheportal Zeitungsausschnitte intern (Loc)",
      "top.headerSettings.name.Test":
        "Rechercheportal Zeitungsausschnitte intern (Test)",
      "top.headerSettings.betaBarContact.name": "UB Wirtschaft - SWA",
      "top.headerSettings.betaBarContact.email": "info-ubw-swa@unibas.ch",
    },
    en: {
      "top.headerSettings.name": "Search portal newspaper cuttings intern",
      "top.headerSettings.name.Dev":
        "Search portal newspaper cuttings intern (Dev)",
      "top.headerSettings.name.Loc":
        "Search portal newspaper cuttings intern (Loc)",
      "top.headerSettings.name.Test":
        "Search portal newspaper cuttings intern (Test)",
      "top.headerSettings.betaBarContact.name": "UB Wirtschaft - SWA",
      "top.headerSettings.betaBarContact.email": "info-ubw-swa@unibas.ch",
    },
  },
};

initRdvLib(environment);
