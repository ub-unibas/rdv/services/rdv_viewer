const Proj = "zas_int-dev";

import { environment as proj } from "@env/zas_int/environment";
import { environment as outermedia } from "@env_temp/environment.type-out";
import { addLocNamePostfix } from "@env_temp/util";
import { initRdvLib, MiradorPlugins, SettingsModel } from "@rdv-lib";

export const environment: SettingsModel = {
  ...proj,
  ...outermedia,
  headerSettings: addLocNamePostfix(proj.headerSettings),
  editable: true,
  proxyUrl: outermedia.proxyUrl + Proj + "/",
  moreProxyUrl: outermedia.moreProxyUrl + Proj + "/",
  inFacetSearchProxyUrl: outermedia.inFacetSearchProxyUrl + Proj + "/",
  detailProxyUrl: outermedia.detailProxyUrl + Proj + "/",
  documentViewerProxyUrl: outermedia.documentViewerProxyUrl + Proj + "/",
  navDetailProxyUrl: outermedia.navDetailProxyUrl + Proj + "/",
  popupQueryProxyUrl: outermedia.popupQueryProxyUrl + Proj + "/",
  detailSuggestionProxyUrl: outermedia.detailSuggestionProxyUrl + Proj + "/",
  detailEditProxyUrl: outermedia.detailEditProxyUrl + Proj + "/",
  suggestSearchWordProxyUrl: outermedia.suggestSearchWordProxyUrl + Proj + "/"
};

initRdvLib(environment, [MiradorPlugins.PLUGIN_TEXT_OVERLAY]);
