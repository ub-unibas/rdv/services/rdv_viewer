const Proj = "zas_int-prod";

import {initRdvLib, SettingsModel} from '@rdv-lib';
import {environment as proj} from '@env/zas_int/environment';
import {environment as prod} from '@env_temp/environment.type-prod';

export const environment: SettingsModel = {
  ...proj,
  ...prod,
  editable: true,


  proxyUrl : "https://ub-zas-proxy.ub.unibas.ch/v2/rdv_query/es_proxy/" +  Proj + "/",
  moreProxyUrl: "https://ub-zas-proxy.ub.unibas.ch/v2/rdv_query/further_snippets/" +  Proj + "/",
  inFacetSearchProxyUrl: "https://ub-zas-proxy.ub.unibas.ch/v2/rdv_query/facet_search/" +  Proj + "/",
  detailProxyUrl: "https://ub-zas-proxy.ub.unibas.ch/v2/rdv_object/object_view/" +  Proj + "/",
  navDetailProxyUrl: "https://ub-zas-proxy.ub.unibas.ch/v2/rdv_query/next_objectview/" +  Proj + "/",
  popupQueryProxyUrl: "https://ub-zas-proxy.ub.unibas.ch/v2/rdv_query/popup_query/" +  Proj + "/",
  suggestSearchWordProxyUrl: undefined,
  detailSuggestionProxyUrl: "https://ub-zas-proxy.ub.unibas.ch/v2/rdv_query/form_query/" +  Proj + "/",
  detailEditProxyUrl: "https://ub-zas-proxy.ub.unibas.ch/v2/rdv_object/object_edit/" +  Proj + "/",
  documentViewerProxyUrl: "https://ub-zas-proxy.ub.unibas.ch/v2/rdv_query/iiif_flex_pres/" +  Proj + "/",

};

initRdvLib(environment);
