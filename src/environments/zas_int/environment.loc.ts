const Proj = "zas_int-loc";

import {initRdvLib, SettingsModel} from '@rdv-lib'
import {environment as proj} from "@env/zas_int/environment";
import {environment as loc} from "@env_temp/environment.type-loc";
import {addLocNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...loc,
  ...proj,

  headerSettings: addLocNamePostfix(proj.headerSettings),
  editable: true,
  proxyUrl : loc.proxyUrl +  Proj + "/",
  moreProxyUrl: loc.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: loc.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: loc.detailProxyUrl +  Proj + "/",
  documentViewerProxyUrl: loc.documentViewerProxyUrl +  Proj + "/",
  navDetailProxyUrl: loc.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: loc.popupQueryProxyUrl +  Proj + "/",
  detailSuggestionProxyUrl: loc.detailSuggestionProxyUrl +  Proj + "/",
  detailEditProxyUrl: "http://127.0.0.1:5000/v1/rdv_object/object_edit/" +  Proj + "/",
  suggestSearchWordProxyUrl: loc.suggestSearchWordProxyUrl +  Proj + "/",
};

initRdvLib(environment);
