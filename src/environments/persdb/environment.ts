import {
  Backend,
  FacetFieldType,
  FacetValueOrder,
  initRdvLib,
  MiradorPlugins,
  Page,
  SettingsModel,
  SortOrder,
  ViewerType,
} from "@rdv-lib";

export const environment: SettingsModel = {
  production: false,

  showSimpleSearch: true,
  showImagePreview: true,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "http://ub-fortbild.ub.unibas.ch/",
  editable: false,

  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,

  documentViewer: {
    // [ViewerType.MIRADOR]: { // not working if aot/prod is built
    'Mirador': {
      enabled: true,
      type: ViewerType.MIRADOR,
      enabledForPage: [{view: Page.Search, order: 10}],
      viewerUrl: "/assets/mirador/init.html",
      disabledPlugins: ["text-overlay"],
      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador.json",
      // Optional kann pro Breakpoint eine andere Konfiguration geladen werden.
      // Prinzipiell werden die Konfigurationen verwendet, wo die aktuelle Browserbreite unterhalb des maxWidth-Wertes liegt.
      // Von allen in Frage kommenden Definitionen wird diejenige mit der kleinsten Differenz von
      // "maxWidth - <aktuelle Breite>" verwendet.
      // Anm.: Die Reihenfolge der Definitionen ist egal.
      //viewerConfigMobilUrl: [
      // {maxWidth: 700, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"},
      //{maxWidth: 500, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"}
      //],
      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'UV': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}, {view: Page.Detail, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/zas_en.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [

      ],
    },
    'UVScroll': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/scroll_view.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [

      ],
    },
    'MiradorSinglePage': {
      enabled: true,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_single.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [

      ],
    },
  },



  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: false
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "Name.keyword",
      order: SortOrder.DESC,
      display: "select-sort-fields.name_label_desc"
    },
    {
      field: "Name.keyword",
      order: SortOrder.ASC,
      display: "select-sort-fields.name_label_asc"
    },
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "Team": {
      "field": "Team.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Team",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 3,
      "size": 100,
      "expandAmount": 20,
      "valueOrder": FacetValueOrder.LABEL,
      "searchWithin": false,
    },
    "Abteilung": {
      "field": "Abteilung.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Abteilung",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 2,
      "size": 100,
      "expandAmount": 20,
      "valueOrder": FacetValueOrder.LABEL,
      "searchWithin": false,
    },
    "Name": {
      "field": "Name.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Name",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL,
      "autocomplete_size": 3
    },


  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {},

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [10, 20, 50, 100],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 20,
    "offset": 0,
    "sortField": "Name.keyword",
    "sortOrder": SortOrder.DESC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {},

  i18n: {
    "de": {
      "select-sort-fields.name_label_desc": "Name (Z-A)",
      "select-sort-fields.name_label_asc": "Name (A-Z)",
      "top.headerSettings.name": "Personaldatenbank",
      "top.headerSettings.name.Dev": "Personaldatenbank (Dev)",
      "top.headerSettings.name.Loc": "Personaldatenbank (Loc)",
      "top.headerSettings.name.Test": "Personaldatenbank (Test)",
    },
    "en": {
      "select-sort-fields.name_label_desc": "Name (Z-A)",
      "select-sort-fields.name_label_asc": "Name (A-Z)",
      "top.headerSettings.name": "Personaldatenbank",
      "top.headerSettings.name.Dev": "Personaldatenbank (Dev)",
      "top.headerSettings.name.Loc": "Personaldatenbank (Loc)",
      "top.headerSettings.name.Test": "Personaldatenbank (Test)",
    }
  },

};

initRdvLib(environment, [MiradorPlugins.PLUGIN_OCR_HELPER]);
