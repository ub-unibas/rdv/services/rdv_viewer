const Proj = "swaletters-dev";

import {FacetFieldType, HistogramFieldModel, HistogramFieldType, initRdvLib, SettingsModel, SortOrder} from '@rdv-lib';
import {environment as proj} from '@env/swaletters/environment';
import {environment as dev} from '@env_temp/environment.type-dev';
import {addDevNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...dev,
  ...proj,

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    ...proj.sortFields,
    {
      field: "portrait_person.label.keyword",
      order: SortOrder.ASC,
      display: "Dargestellte Person Alphabetisch"
    },
    {
      field: "geburtsjahr",
      order: SortOrder.ASC,
      display: "Dargestellte Person Geburtsjahr aufsteigend"
    },
    {
      field: "geburtsjahr",
      order: SortOrder.DESC,
      display: "Dargestellte Person Geburtsjahr absteigend"
    }
  ],

  headerSettings: addDevNamePostfix(proj.headerSettings),

  proxyUrl: "https://ub-rdv-dev22-proxy.ub.unibas.ch/v1/rdv_query/es_proxy/" +  Proj + "/",
  moreProxyUrl: "https://ub-rdv-dev22-proxy.ub.unibas.ch/v1/rdv_query/further_snippets/" +  Proj + "/",
  inFacetSearchProxyUrl: "https://ub-rdv-dev22-proxy.ub.unibas.ch/v1/rdv_query/facet_search/" +  Proj + "/",
  documentViewerProxyUrl: "https://ub-rdv-dev22-proxy.ub.unibas.ch/v1/rdv_query/iiif_flex_pres/" +  Proj + "/",
  detailProxyUrl: "https://ub-rdv-dev22-proxy.ub.unibas.ch/v1/rdv_object/object_view/" +  Proj + "/",
  detailEditProxyUrl: "https://ub-rdv-dev22-proxy.ub.unibas.ch/v1/rdv_object/object_edit/" +  Proj + "/",
  navDetailProxyUrl: "https://ub-rdv-dev22-proxy.ub.unibas.ch/v1/rdv_query/next_objectview/" +  Proj + "/",
  suggestSearchWordProxyUrl: "https://ub-rdv-dev22-proxy.ub.unibas.ch/v1/rdv_query/autocomplete/" +  Proj + "/",
};

initRdvLib(environment);
