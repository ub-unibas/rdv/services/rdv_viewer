import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  FacetValueOrder,
  HistogramFieldModel,
  HistogramFieldType,
  Page,
  initRdvLib,
  SettingsModel,
  SortOrder,
  ViewerType,
  MiradorPlugins
} from '@rdv-lib';

export const environment: SettingsModel = {
  scaleDocThumbsByIiif: false,
  documentViewer: {
    // [ViewerType.MIRADOR]: { // not working if aot/prod is built
    'Mirador': {
      enabled: true,
      type: ViewerType.MIRADOR,
      enabledForPage: [{view: Page.Search, order: 10}],
      viewerUrl: "/assets/mirador/init.html",
      disabledPlugins: ["text-overlay"],
      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador.json",
      // Optional kann pro Breakpoint eine andere Konfiguration geladen werden.
      // Prinzipiell werden die Konfigurationen verwendet, wo die aktuelle Browserbreite unterhalb des maxWidth-Wertes liegt.
      // Von allen in Frage kommenden Definitionen wird diejenige mit der kleinsten Differenz von
      // "maxWidth - <aktuelle Breite>" verwendet.
      // Anm.: Die Reihenfolge der Definitionen ist egal.
      //viewerConfigMobilUrl: [
      // {maxWidth: 700, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"},
      //{maxWidth: 500, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"}
      //],
      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "digi",
          messageTitleKey: "search-results.view-mode.UV.object_type.message.title",
          messageTextKey: "Nur digitalisierte Objekte können in dieser Ansicht angezeigt werden.",
          values: [
            {label: "Ja", value: {"id": "Ja"}},
          ]
        }
      ],
    },
    'UV': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}, {view: Page.Detail, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/zas_en.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "digi",
          messageTitleKey: "search-results.view-mode.UV.object_type.message.title",
          messageTextKey: "Nur digitalisierte Objekte können in dieser Ansicht angezeigt werden.",
          values: [
            {label: "Ja", value: {"id": "Ja"}},
          ]
        }
      ],
    },
    'UVScroll': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/scroll_view.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "digi",
          messageTitleKey: "search-results.view-mode.UV.object_type.message.title",
          messageTextKey: "Nur digitalisierte Objekte können in dieser Ansicht angezeigt werden.",
          values: [
            {label: "Ja", value: {"id": "Ja"}},
          ]
        }
      ],
    },
    'MiradorSinglePage': {
      enabled: true,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_single.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "digi",
          messageTitleKey: "search-results.view-mode.UV.object_type.message.title",
          messageTextKey: "Nur digitalisierte Objekte können in dieser Ansicht angezeigt werden.",
          values: [
            {label: "Ja", value: {"id": "Ja"}},
          ]
        }
      ],
    },
  },

  production: false,

  showSimpleSearch: true,
  // viewerExtraWide: true,
  // per Default werden Preview Images angezeigt
  // showImagePreview: false,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "https://ub-portraets.ub.unibas.ch/",

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: true
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  // footerSettings: {
  //   "default": {
  //     // den ganzen Footer ausblenden
  //     disable: false,
  //     // den "Nach Oben"-Link anzeigen
  //     displayToTop: true,
  //     // Host des Projekts im Footer anzeigen ("Universität Basel")
  //     displayHostUrl: true,
  //     // i18n key der URL des Host-Links (bei displayHostUrl: true)
  //     hostUrl: "footer.footerSettings.host.url",
  //     // i18n key des Namens des Host-Links (bei displayHostUrl: true)
  //     hostName: "footer.footerSettings.host.name",
  //     // Portal-URL im Footer anzeigen
  //     displayPortalUrl: true
  //   },
  //   // sprachspezifische Einstellungen (ohne Übersetzungen)
  //   "de": {
  //   },
  //   "en": {
  //   }
  // },

  // Dokument-Detail-Ansicht:
  // Mit 'bottom' oder 'top', wird in der Detail-Ansicht ein IIIF Viewer unterhalb oder oberhalb
  // der Felder eines Dokuments angezeigt. false schaltet die gleichzeitige Ansicht ab.
  narrowEmbeddedIiiFViewer: 'bottom',

  // UV/Mirador verwenden nur maximale Breite der app (false, undefined).
  // Wird diese Option auf true gesetzt, dann wird die volle Browser-Viewport-Breite verwendet.
  // viewerExtraWide: false,

  // die konkreten Werte, werden noch gesetzt
  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,

  externalAboutUrl: {
    "de": "https://ub.unibas.ch/de/portraets/",
    "en": "https://ub.unibas.ch/en/portraets/"
  },

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "select-sort-fields.score_desc"
    },
    {
      field: "year",
      order: SortOrder.ASC,
      display: "Ältestes zuerst"
    },
    {
      field: "year",
      order: SortOrder.DESC,
      display: "Neuestes zuerst"
    }
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "kategorie": {
      "field": "type.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Porträt / Person",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "expandAmount": 10,
      "size": 100,
      "initiallyOpen": true
    },
    "600": {
      "field": "600_abcd_1_7.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Dargestellte Person/en",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "initiallyOpen": true,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT],
      "autocomplete_size": 3
    },
    "100_1_a": {
      "field": "author.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Künstler/in, Fotograf/in",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 2,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "initiallyOpen": false,
      "autocomplete_size": 3
    },
    "245_ab": {
      "field": "title.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Titel",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 3,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "initiallyOpen": false,
      "autocomplete_size": 3
    },
    "300_ab": {
      "field": "300_ab.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Technik",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 6,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "initiallyOpen": false
    },
    "year": {
      "field": "year",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Datierung",
      "operator": "AND",
      "showAggs": true,
      "order": 7,
      "size": 31,
      "expandAmount": 31,
    } as HistogramFieldModel,
    "start_year": {
      "field": "start_year",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Start Datierung",
      "operator": "AND",
      "showAggs": true,
      "order": 7,
      "size": 31,
      "expandAmount": 31,
    } as HistogramFieldModel,
    "end_year": {
      "field": "end_year",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "End Datierung",
      "operator": "AND",
      "showAggs": true,
      "order": 7,
      "size": 31,
      "expandAmount": 31,
    } as HistogramFieldModel,
    "range__year": {
      "field": "range__year",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Range Datierung",
      "operator": "AND",
      "showAggs": true,
      "order": 7,
      "size": 31,
      "expandAmount": 31,
    } as HistogramFieldModel,
    "264__1_a": {
      "field": "264_a_1.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Entstehungsort",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 8,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "initiallyOpen": false
    },
    "digi": {
      "field": "digitalisiert.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Digitalisat vorhanden",
      "operator": "OR",
      "operators": [
        "OR", "NOT"
      ],
      "order": 9,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "initiallyOpen": false
    },
    "wiki_len": {
      "field": "wiki_length",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Länge Datensatz Wikipedia",
      "operator": "AND",
      "showAggs": true,
      "order": 10033,
      "size": 16,
      "expandAmount": 31,
      "help": "env.facetFields.wiki_length.help"
    } as HistogramFieldModel,
    "Quellen_Wikidata": {
      "field": "wikidata_sources.label.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "in Wikidata verzeichnete Quellen",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10035,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "Quellen": {
      "field": "quellen.label.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "in der GND verzeichnete Quellen",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10036,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "Datenanalyse": {
      "field": "tag.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Datenanalyse",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 11,
      "expandAmount": 10,
      "size": 100,
    },
    "wikipedia_relations": {
      "field": "wikipedia_relations.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "in Wikipedia enthaltene Verweise",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10036,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "wikipedia_int_relations": {
      "field": "wikipedia_int_relations.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "in Wikipedia enthaltene Zas interne Verweise",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10036,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "wiki_categories": {
      "field": "wiki_categories.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Wikipedia Kategorien",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10037,
      "size": 100,
      "expandAmount": 30,
      "searchWithin": true,
    },
    "wiki_orig_categories": {
      "field": "wiki_orig_categories.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Wikipedia volle Kategorien",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10037,
      "size": 100,
      "expandAmount": 30,
      "searchWithin": true,
    },
  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {},

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [5, 10, 25, 50, 200],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 10,
    "offset": 0,
    "sortField": "_score",
    "sortOrder": SortOrder.DESC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
      "fct_location": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "fct_location",
      "label": "Geographikum"
      }
      },

  i18n: {
    "de": {
      "top.headerSettings.name": "Porträtsammlung",
      "top.headerSettings.name.Dev": "Porträtsammlung (Dev)",
      "top.headerSettings.name.Loc": "Porträtsammlung (Loc)",
      "top.headerSettings.name.Test": "Porträtsammlung (Test)",
      "top.headerSettings.betaBarContact.name": "Noah Regenass",
      "top.headerSettings.betaBarContact.email": "noah.regenass@unibas.ch",
      "env.facetFields.gnd.help": "Die folgenden Facetten sind nur für mit der GND verknüpfte Personen vorhanden."
    },
    "en": {
      "top.headerSettings.name": "portrait collection",
      "top.headerSettings.name.Dev": "portrait collection (Dev)",
      "top.headerSettings.name.Loc": "portrait collection (Loc)",
      "top.headerSettings.name.Test": "portrait collection (Test)",
      "top.headerSettings.betaBarContact.name": "Noah Regenass",
      "top.headerSettings.betaBarContact.email": "noah.regenass@unibas.ch",
    }
  },

};

initRdvLib(environment, [MiradorPlugins.PLUGIN_OCR_HELPER]);
