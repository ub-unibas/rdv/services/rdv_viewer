
import {
  MiradorPlugins,
  initRdvLib,
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  FacetValueOrder,
  HistogramFieldModel,
  HistogramFieldType,
  Page,
  SettingsModel,
  SortOrder,
  ViewerType,
} from "@rdv-lib";

export const environment: SettingsModel = {
  documentViewer: {
    // [ViewerType.MIRADOR]: { // not working if aot/prod is built
    'Mirador': {
      enabled: true,
      type: ViewerType.MIRADOR,
      enabledForPage: [{view: Page.Search, order: 10}],
      viewerUrl: "/assets/mirador/init.html",
      disabledPlugins: ["text-overlay"],
      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador.json",
      // Optional kann pro Breakpoint eine andere Konfiguration geladen werden.
      // Prinzipiell werden die Konfigurationen verwendet, wo die aktuelle Browserbreite unterhalb des maxWidth-Wertes liegt.
      // Von allen in Frage kommenden Definitionen wird diejenige mit der kleinsten Differenz von
      // "maxWidth - <aktuelle Breite>" verwendet.
      // Anm.: Die Reihenfolge der Definitionen ist egal.
      //viewerConfigMobilUrl: [
      // {maxWidth: 700, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"},
      //{maxWidth: 500, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"}
      //],
      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "digi",
          messageTitleKey: "search-results.view-mode.UV.object_type.message.title",
          messageTextKey: "Nur digitalisierte Objekte können in dieser Ansicht angezeigt werden.",
          values: [
            {label: "Ja", value: {"id": "Ja"}},
          ]
        }
      ],
    },
    'UV': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}, {view: Page.Detail, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/zas_en.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "digi",
          messageTitleKey: "search-results.view-mode.UV.object_type.message.title",
          messageTextKey: "Nur digitalisierte Objekte können in dieser Ansicht angezeigt werden.",
          values: [
            {label: "Ja", value: {"id": "Ja"}},
          ]
        }
      ],
    },
    'UVScroll': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/scroll_view.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "digi",
          messageTitleKey: "search-results.view-mode.UV.object_type.message.title",
          messageTextKey: "Nur digitalisierte Objekte können in dieser Ansicht angezeigt werden.",
          values: [
            {label: "Ja", value: {"id": "Ja"}},
          ]
        }
      ],
    },
    'MiradorSinglePage': {
      enabled: true,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_single.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "object_type",
          messageTitleKey: "search-results.view-mode.Mirador.object_type.message.title",
          messageTextKey: "search-results.view-mode.Mirador.object_type.message.text",
          values: [
            {label: "Zeitungsausschnitt", value: {"id": "Zeitungsausschnitt"}},
            {label: "retrodigitalisierter Zeitungsausschnitt", value: {"id": "retrodigitalisierter Zeitungsausschnitt"}},
            {label: "elektronischer Zeitungsausschnitt (ab 2013)", value: {"id": "elektronischer Zeitungsausschnitt (ab 2013)"}}
          ]
        }
      ],
    },
  },

  production: false,

  showSimpleSearch: true,
  // viewerExtraWide: true,
  // per Default werden Preview Images angezeigt
  // showImagePreview: false,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "https://ub-mf226.ub.unibas.ch/",

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: true
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  // footerSettings: {
  //   "default": {
  //     // den ganzen Footer ausblenden
  //     disable: false,
  //     // den "Nach Oben"-Link anzeigen
  //     displayToTop: true,
  //     // Host des Projekts im Footer anzeigen ("Universität Basel")
  //     displayHostUrl: true,
  //     // i18n key der URL des Host-Links (bei displayHostUrl: true)
  //     hostUrl: "footer.footerSettings.host.url",
  //     // i18n key des Namens des Host-Links (bei displayHostUrl: true)
  //     hostName: "footer.footerSettings.host.name",
  //     // Portal-URL im Footer anzeigen
  //     displayPortalUrl: true
  //   },
  //   // sprachspezifische Einstellungen (ohne Übersetzungen)
  //   "de": {
  //   },
  //   "en": {
  //   }
  // },

  // Dokument-Detail-Ansicht:
  // Mit 'bottom' oder 'top', wird in der Detail-Ansicht ein IIIF Viewer unterhalb oder oberhalb
  // der Felder eines Dokuments angezeigt. false schaltet die gleichzeitige Ansicht ab.
  narrowEmbeddedIiiFViewer: 'bottom',

  // UV/Mirador verwenden nur maximale Breite der app (false, undefined).
  // Wird diese Option auf true gesetzt, dann wird die volle Browser-Viewport-Breite verwendet.
  // viewerExtraWide: false,

  // die konkreten Werte, werden noch gesetzt
  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,

  externalAboutUrl: "https://ub.unibas.ch/de/portraets/",

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "select-sort-fields.score_desc"
    },
    {
      field: "year",
      order: SortOrder.ASC,
      display: "Ältestes zuerst"
    },
    {
      field: "year",
      order: SortOrder.DESC,
      display: "Neuestes zuerst"
    }
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "100_1_a": {
      "field": "100_1_a.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Komponist/in",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "initiallyOpen": false,
      "autocomplete_size": 3,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT],
    },
    "245_ab": {
      "field": "245_ab.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Titel",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 2,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "initiallyOpen": false,
      "autocomplete_size": 3
    },
    "signatur": {
      "field": "signatur.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Signatur",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 3,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "initiallyOpen": false,
      "autocomplete_size": 3
    },
    "520_a": {
      "field": "520_a.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "musikal. Form",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 4,
      "size": 100,
      "expandAmount": 4,
      "searchWithin": true,
      "initiallyOpen": false
    },

    "300_ab": {
      "field": "300_ab.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Format",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 7,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "initiallyOpen": false
    },

    "019_a": {
      "field": "019_a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "019_a",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 5,
      "size": 100,
      "expandAmount": 4,
      "searchWithin": true,
      "initiallyOpen": false
    },
    "348_a": {
      "field": "348_a.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Publikationsform",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 6,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "initiallyOpen": false
    },

    "500_lib": {
      "field": "500_lib.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Ort Originalquelle",
      "operator": "OR",
      "operators": [
        "OR", "AND"
      ],
      "order": 8,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "initiallyOpen": false,
    },
    "500_date": {
      "field": "500_date.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Datierung / Erscheinungsort",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 9,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "initiallyOpen": false
    },
    "500_category": {
      "field": "500_category.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Subkategorien Allgemein",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 10,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "initiallyOpen": false
    },
    "digi": {
      "field": "digi.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Digitalisiert",
      "operator": "OR",
      "operators": [
        "OR", "NOT"
      ],
      "order": 9,
      "size": 100,
      "expandAmount": 4,
      "searchWithin": true,
      "initiallyOpen": false
    },

    "690_a": {
      "field": "690_a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Schlagwort",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 11,
      "size": 100,
      "expandAmount": 4,
      "searchWithin": true,
      "initiallyOpen": false
    },

    "Geschlecht": {
      "field": "geschlecht.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Komponist/in Geschlecht",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 100,
      "size": 100,
      "expandAmount": 10,
      "help": "env.facetFields.gnd.help",
    },
    "Geburtsort": {
      "field": "geburtsort.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Komponist/in Geburtsort",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 101,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "Sterbeort": {
      "field": "sterbeort.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Komponist/in Sterbeort",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 102,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "Wirkungsort": {
      "field": "wirkungsort.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Komponist/in Wirkungsort",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 103,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "Land": {
      "field": "land.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Komponist/in Herkunft",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 105,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "Beruf": {
      "field": "berufe.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Komponist/in Beruf",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 106,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "Geburtsjahr": {
      "field": "geburtsjahr",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Komponist/in Geburtsjahr",
      "operator": "AND",
      "showAggs": true,
      "order": 107,
      "size": 31,
      "expandAmount": 31,
    } as HistogramFieldModel,
    "Sterbejahr": {
      "field": "sterbejahr",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Komponist/in Sterbejahr",
      "operator": "AND",
      "showAggs": true,
      "order": 108,
      "size": 31,
      "expandAmount": 31,
    } as HistogramFieldModel,
  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {},

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [10, 50, 100, 200],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 50,
    "offset": 0,
    "sortField": "_score",
    "sortOrder": SortOrder.DESC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
      "fct_location": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "fct_location",
      "label": "Geographikum"
      }
      },
  searchResultSettings: {
    showHitNumber: false,
    showHitObjectType: true,
    paginationMethod: "bar",
    showKeepFiltersButton: false,
    pageButtonsInBar: 5, // go to last page and check if there is no overflow!
    // order of view style buttons (left top side on the search result page)
    viewStyleButtons: [
      // "PAGE_SIZE_SELECTOR",
      //"LIST_STYLE_BUTTON",
      //"IIIF_SELECTOR",
      //"IIIF_BUTTON_GROUP"
    ]
  },
  i18n: {
    "de": {
      "top.headerSettings.name": "Musik-Mikrofilmarchiv",
      "top.headerSettings.name.Dev": "Musik-Mikrofilmarchiv (Dev)",
      "top.headerSettings.name.Loc": "Musik-Mikrofilmarchiv (Loc)",
      "top.headerSettings.name.Test": "Musik-Mikrofilmarchiv (Test)",
      "top.headerSettings.betaBarContact.name": "Iris Lindenmann",
      "top.headerSettings.betaBarContact.email": "iris.lindenmann@unibas.ch",
      "env.facetFields.gnd.help": "Die folgenden Facetten sind nur für mit der GND verknüpfte Personen vorhanden."
    },
    "en": {
      "top.headerSettings.name": "portrait collection",
      "top.headerSettings.name.Dev": "portrait collection (Dev)",
      "top.headerSettings.name.Loc": "portrait collection (Loc)",
      "top.headerSettings.name.Test": "portrait collection (Test)",
      "top.headerSettings.betaBarContact.name": "Noah Regenass",
      "top.headerSettings.betaBarContact.email": "noah.regenass@unibas.ch",
    }
  },

};

initRdvLib(environment, [MiradorPlugins.PLUGIN_OCR_HELPER]);
