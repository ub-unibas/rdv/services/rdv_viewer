const Proj = "gbooks_obj-dev";

import {initRdvLib, SettingsModel} from '@rdv-lib';
import {environment as proj} from '@env/gbooks_obj/environment';
import {environment as dev} from '@env_temp/environment.type-dev';
import {addDevNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...dev,
  ...proj,

  headerSettings: addDevNamePostfix(proj.headerSettings),

  proxyUrl : "https://ub-rdv-dev22-proxy.ub.unibas.ch/v1/rdv_query/es_proxy/" +  Proj + "/",
  moreProxyUrl: "https://ub-rdv-dev22-proxy.ub.unibas.ch/v1/rdv_query/further_snippets/" +  Proj + "/",
  inFacetSearchProxyUrl: "https://ub-rdv-dev22-proxy.ub.unibas.ch/v1/rdv_query/facet_search/" +  Proj + "/",
  detailProxyUrl: "https://ub-rdv-dev22-proxy.ub.unibas.ch/v1/rdv_object/object_view/" +  Proj + "/",
  navDetailProxyUrl: "https://ub-rdv-dev22-proxy.ub.unibas.ch/v1/rdv_query/next_objectview/" +  Proj + "/",
  popupQueryProxyUrl: "https://ub-rdv-dev22-proxy.ub.unibas.ch/v1/rdv_query/popup_query/" +  Proj + "/",
  detailSuggestionProxyUrl: "https://ub-rdv-dev22-proxy.ub.unibas.ch/v1/rdv_query/form_query/" +  Proj + "/",
  suggestSearchWordProxyUrl: "https://ub-rdv-dev22-proxy.ub.unibas.ch/v1/rdv_query/autocomplete/" +  Proj + "/",
};

initRdvLib(environment);
