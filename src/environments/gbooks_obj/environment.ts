import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  FacetValueOrder,
  HistogramFieldModel,
  HistogramFieldType,
  initRdvLib,
  Page,
  SettingsModel,
  SortOrder,
  ViewerType,
} from '@rdv-lib';


export const environment: SettingsModel = {
  proxyUrl: undefined,
  narrowEmbeddedIiiFViewer: 'bottom',
  documentViewer: {
    // [ViewerType.UV]: { // not working if aot/prod is built
    'UV': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Detail, order: 20}, {view: Page.Search, order: 15}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/zas_en.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'UVScroll': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}], // default: on all views
      viewerUrl: "assets/uv-3.0.36/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/scroll_view.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "type",
          messageTitleKey: "search-results.view-mode.UVScroll.object_type.message.title",
          messageTextKey: "search-results.view-mode.UVScroll.object_type.message.text",
          values: [
            {label: "Einzelseite", value: {"id": "Einzelseite"}}
          ]
        }
      ],
    },
    // [ViewerType.MIRADOR]: { // not working if aot/prod is built
    'Mirador': {
      enabled: true,
      type: ViewerType.MIRADOR,
      enabledForPage: [{view: Page.Search, order: 10}],
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",
      disabledPlugins: ["text-overlay"],
      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "type",
          messageTitleKey: "search-results.view-mode.UVScroll.object_type.message.title",
          messageTextKey: "search-results.view-mode.UVScroll.object_type.message.text",
          values: [
            {label: "Einzelseite", value: {"id": "Einzelseite"}}
          ]
        }
      ],
    },
    'MiradorScroll': {
      enabled: false,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // mögliche Werte: "share", "download","image-tools","text-overlay"

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_scroll.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'MiradorSinglePage': {
      enabled: true,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",
      disabledPlugins: ["text-overlay"],
      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_single.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    }
  },


  production: false,

  showSimpleSearch: true,
  // viewerExtraWide: true,
  // per Default werden Preview Images angezeigt
  showImagePreview: true,
  scaleDocThumbsByIiif: false,
  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "https://ub-portraets.ub.unibas.ch/",

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: true
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "Relevanz"
    }
  ],

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "type": {
      "field": "type.keyword",
      "facetType": FacetFieldType.CHECKBOX,
      "label": "Typ",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10,
      "initiallyOpen": true,
      "valueOrder": FacetValueOrder.LABEL,
      "help": "env.facetFields.type.help"
    },


    "title": {
      "field": "title.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Titel",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL],
      "searchWithin": true,
    },
    "signatur": {
      "field": "signatur.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Signatur",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL],
      "searchWithin": true,
    },
    "author": {
      "field": "author.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Verfasser*in",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL],
      "searchWithin": true,
    },
    "fachgebiete1987": {
      "field": "fachgebiete.fachgebiete_basel1987.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Fachgebiete Basel 1987 Teil1",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL],
      "searchWithin": true,
    },
    "fachgebiete1993": {
      "field": "fachgebiete.fachgebiete_basel1993.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Fachgebiete Basel 1993 Teil2",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL],
      "searchWithin": true,
    },
    "fachgebiete1994": {
      "field": "fachgebiete.fachgebiete_basel1994.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Fachgebiete Basel 1994",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL],
      "searchWithin": true,
    },
    "fachgebietezuerich": {
      "field": "fachgebiete.fachgebiete_zuerich.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Fachgebiete Zürich",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL],
      "searchWithin": true,
    },
    "fachgebietebern": {
      "field": "fachgebiete.fachgebiete_bern.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Fachgebiete Bern",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL],
      "searchWithin": true,
    },
    "fachgebieteluzern": {
      "field": "fachgebiete.fachgebiete_luzern.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Fachgebiete Luzern",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL],
      "searchWithin": true,
    },
    "marc_lang": {
      "field": "marc_lang.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Sprache MARC",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL],
      "searchWithin": true,
    },
    "obj_id": {
      "field": "obj_id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Google Barcode",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "inst_code": {
      "field": "inst_code.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Institution",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "ocr_lang": {
      "field": "ocr_lang.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "OCR Sprache",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "ocr_lang_split": {
      "field": "ocr_lang_split.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "OCR Sprache aufgetrennt",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "readingOrder": {
      "field": "readingOrder.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Leserichtung",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "amid_singlepage": {
      "field": "amid.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Struktur Tag Einzelseite",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 101,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "amid": {
      "field": "amid_marker.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Struktur Tag",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 101,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "tag": {
      "field": "tag.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Spezialfilter",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 101,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "ocr_analysis": {
      "field": "ocr_analysis.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "OCR Analyse",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 155,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "ocr_vol_analysis": {
      "field": "ocr_vol_analysis.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "OCR Volume",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 155,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "ocr_vol_gtd": {
      "field": "ocr_vol_gtd.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "OCR GTD Volume",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 155,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
    },

  },


  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {},

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [10, 25, 50, 100],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 100,
    "offset": 0,
    "sortField": "_score",
    "sortOrder": SortOrder.DESC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
      "fct_location": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "fct_location",
      "label": "Geographikum"
      }
      },

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },
  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  searchResultSettings: {
    showHitNumber: false,
    showHitObjectType: true,
    paginationMethod: "bar",
    showKeepFiltersButton: false,
    pageButtonsInBar: 5, // go to last page and check if there is no overflow!
    // order of view style buttons (left top side on the search result page)
    viewStyleButtons: [
      // "PAGE_SIZE_SELECTOR",
      "LIST_STYLE_BUTTON",
      //"IIIF_SELECTOR",
      "IIIF_BUTTON_GROUP"
    ]
  },

  i18n: {
    "de": {
      "top.headerSettings.name": "Google Books Datenanalyse",
      "top.headerSettings.name.Dev": "Google Books Datenanalyse (Dev)",
      "top.headerSettings.name.Loc": "Google Books Datenanalyse (Loc)",
      "top.headerSettings.name.Test": "Google Books Datenanalyse (Test)",
      "top.headerSettings.betaBarContact.name": "Martin Reisacher",
      "top.headerSettings.betaBarContact.email": "martin.reisacher@unibas.ch",
      "env.facetFields.type.help": "Objekttyp: Einzelseite oder Buch, hat Auswirkung auf Anzeige der vorhandenen Facetten",
      "env.facetFields.amid2.help": "zusätzliche Struktur Tag Facette, um Werte besser mit boolschen Operator (NOT) zu kombinieren"
    }
  },

};

initRdvLib(environment);
