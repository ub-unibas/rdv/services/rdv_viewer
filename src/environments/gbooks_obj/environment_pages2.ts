import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  FacetValueOrder,
  HistogramFieldModel,
  HistogramFieldType,
  initRdvLib,
  Page,
  SettingsModel,
  SortOrder,
  ViewerType,
} from '@rdv-lib';


export const environment: SettingsModel = {
  proxyUrl: undefined,
  narrowEmbeddedIiiFViewer: 'bottom',
  documentViewer: {
    // [ViewerType.UV]: { // not working if aot/prod is built
    'UV': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Detail, order: 20}, {view: Page.Search, order: 15}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/zas_en.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'UVScroll': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}], // default: on all views
      viewerUrl: "assets/uv-3.0.36/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/scroll_view.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "type",
          messageTitleKey: "search-results.view-mode.UVScroll.object_type.message.title",
          messageTextKey: "search-results.view-mode.UVScroll.object_type.message.text",
          values: [
            {label: "Einzelseite", value: {"id": "Einzelseite"}}
          ]
        }
      ],
    },
    // [ViewerType.MIRADOR]: { // not working if aot/prod is built
    'Mirador': {
      enabled: true,
      type: ViewerType.MIRADOR,
      enabledForPage: [{view: Page.Search, order: 10}],
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",
      disabledPlugins: ["text-overlay"],
      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "type",
          messageTitleKey: "search-results.view-mode.UVScroll.object_type.message.title",
          messageTextKey: "search-results.view-mode.UVScroll.object_type.message.text",
          values: [
            {label: "Einzelseite", value: {"id": "Einzelseite"}}
          ]
        }
      ],
    },
    'MiradorScroll': {
      enabled: false,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // mögliche Werte: "share", "download","image-tools","text-overlay"

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_scroll.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'MiradorSinglePage': {
      enabled: true,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",
      disabledPlugins: ["text-overlay"],
      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_single.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    }
  },


  production: false,

  showSimpleSearch: true,
  // viewerExtraWide: true,
  // per Default werden Preview Images angezeigt
  showImagePreview: true,
  scaleDocThumbsByIiif: false,
  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "https://ub-portraets.ub.unibas.ch/",

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: true
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "Relevanz"
    }
  ],

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "type": {
      "field": "type.keyword",
      "facetType": FacetFieldType.CHECKBOX,
      "label": "Typ",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10,
      "initiallyOpen": true,
      "valueOrder": FacetValueOrder.LABEL,
      "help": "env.facetFields.type.help"
    },


    "title": {
      "field": "title.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Titel",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL],
      "searchWithin": true,
      "facetGroup": "Buch"
    },
    "signatur": {
      "field": "signatur.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Signatur",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL],
      "searchWithin": true,
      "facetGroup": "Buch"
    },
    "author": {
      "field": "author.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Verfasser*in",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL],
      "searchWithin": true,
      "facetGroup": "Buch"
    },
    "fachgebiete": {
      "field": "fachgebiete.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Fachgebiete",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL],
      "searchWithin": true,
      "facetGroup": "Buch"
    },
    "marc_lang": {
      "field": "marc_lang.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Sprache MARC",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL],
      "searchWithin": true,
      "facetGroup": "Buch"
    },
    "obj_id": {
      "field": "obj_id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Google Barcode",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "facetGroup": "Buch"
    },
    "ocr_lang": {
      "field": "ocr_lang.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "OCR Sprache",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "facetGroup": "Buch"
    },
    "readingOrder": {
      "field": "readingOrder.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Leserichtung",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "facetGroup": "Buch"
    },
    "scanningOrder": {
      "field": "scanningOrder.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Scanrichtung",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 5,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "facetGroup": "Buch"
    },
    "coverTag": {
      "field": "coverTag.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Cover Tag?",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 10,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "facetGroup": "Buch"
    },

    "amid": {
      "field": "amid.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Struktur Tag",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 101,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "facetGroup": "Einzelseiten Analyse"
    },
    /*"amid2": {
      "field": "amid.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Struktur Tag 2",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 102,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "facetGroup": "Einzelseiten Analyse",
      "help": "env.facetFields.amid2.help"
    },*/
    "order_label": {
      "field": "order_label.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Label",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 102,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL],
      "facetGroup": "Einzelseiten Analyse"
    },
    "ocr_gtd": {
      "field": "ocr_gtd",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "OCR Qualität Seite",
      "operator": "AND",
      "showAggs": true,
      "order": 103,
      "size": 31,
      "expandAmount": 31,
      "facetGroup": "Einzelseiten Analyse"
    } as HistogramFieldModel,
    /*
    "img_size": {
      "field": "img_size",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Bildgrösse",
      "operator": "AND",
      "showAggs": true,
      "order": 103,
      "size": 31,
      "expandAmount": 31,
      "facetGroup": "Einzelseiten Analyse"
    } as HistogramFieldModel,
    "height": {
      "field": "height",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Höhe",
      "operator": "AND",
      "showAggs": true,
      "order": 103,
      "size": 31,
      "expandAmount": 31,
      "facetGroup": "Einzelseiten Analyse"
    } as HistogramFieldModel,
    "width": {
      "field": "width",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Breite",
      "operator": "AND",
      "showAggs": true,
      "order": 103,
      "size": 31,
      "expandAmount": 31,
      "facetGroup": "Einzelseiten Analyse"
    } as HistogramFieldModel,

    "ocr_size": {
      "field": "ocr_size",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Länge OCR",
      "operator": "AND",
      "showAggs": true,
      "order": 103,
      "size": 31,
      "expandAmount": 31,
      "facetGroup": "Einzelseiten Analyse"
    } as HistogramFieldModel,
    */

    "overall_error": {
      "field": "overall_error_int",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Fehler allgemein",
      "operator": "AND",
      "showAggs": true,
      "order": 151,
      "size": 31,
      "expandAmount": 31,
      "facetGroup": "Google Fehleranalyse"
    } as HistogramFieldModel,
    "tightBoundPages": {
      "field": "tightBoundPages_int",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Eng gebunden",
      "operator": "AND",
      "showAggs": true,
      "order": 151,
      "size": 31,
      "expandAmount": 31,
      "facetGroup": "Google Fehleranalyse"
    } as HistogramFieldModel,
    "missingPages": {
      "field": "missingPages_int",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "fehlende Seiten",
      "operator": "AND",
      "showAggs": true,
      "order": 151,
      "size": 31,
      "expandAmount": 31,
      "facetGroup": "Google Fehleranalyse"
    } as HistogramFieldModel,
    /*"material_error": {
      "field": "material_error_int",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Materialfehler",
      "operator": "AND",
      "showAggs": true,
      "order": 151,
      "size": 31,
      "expandAmount": 31,
      "facetGroup": "Google Fehleranalyse"
    } as HistogramFieldModel,*/
    "badPages": {
      "field": "badPages_int",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "schlechte Seiten",
      "operator": "AND",
      "showAggs": true,
      "order": 151,
      "size": 31,
      "expandAmount": 31,
      "facetGroup": "Google Fehleranalyse"
    } as HistogramFieldModel,
    "rubbish": {
      "field": "rubbish_int",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Abfall",
      "operator": "AND",
      "showAggs": true,
      "order": 151,
      "size": 31,
      "expandAmount": 31,
      "facetGroup": "Google Fehleranalyse"
    } as HistogramFieldModel,
    "ocr_analysis": {
      "field": "ocr_analysis.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "OCR Analyse",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 155,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "ocr_vol_analysis": {
      "field": "ocr_vol_analysis.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "OCR Volume",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 155,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "facetGroup": "Google Fehleranalyse"
    },
    "ocr_vol_gtd": {
      "field": "ocr_vol_gtd.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "OCR GTD Volume",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 155,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "facetGroup": "Google Fehleranalyse"
    },

    "Google_Analyze": {
      "field": "Google_Analyze",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Analyze Datum",
      "operator": "AND",
      "showAggs": true,
      "order": 200,
      "size": 31,
      "expandAmount": 31,
      "facetGroup": "Google Verarbeitungsdatum"
    } as HistogramFieldModel,
    "Google_Convert": {
      "field": "Google_Convert",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Convert Datum",
      "operator": "AND",
      "showAggs": true,
      "order": 200,
      "size": 31,
      "expandAmount": 31,
      "facetGroup": "Google Verarbeitungsdatum"
    } as HistogramFieldModel,
    "Google_Process": {
      "field": "Google_Process",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Process Datum",
      "operator": "AND",
      "showAggs": true,
      "order": 200,
      "size": 31,
      "expandAmount": 31,
      "facetGroup": "Google Verarbeitungsdatum"
    } as HistogramFieldModel,
    "Google_Rubbish": {
      "field": "Google_Rubbish",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Rubbish Datum",
      "operator": "AND",
      "showAggs": true,
      "order": 200,
      "size": 31,
      "expandAmount": 31,
      "facetGroup": "Google Verarbeitungsdatum"
    } as HistogramFieldModel,
    "Google_Scan": {
      "field": "Google_Scan",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Scan Datum",
      "operator": "AND",
      "showAggs": true,
      "order": 200,
      "size": 31,
      "expandAmount": 31,
      "facetGroup": "Google Verarbeitungsdatum"
    } as HistogramFieldModel,
  },


  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {},

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [10, 25, 50, 100],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 100,
    "offset": 0,
    "sortField": "_score",
    "sortOrder": SortOrder.DESC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
      "fct_location": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "fct_location",
      "label": "Geographikum"
      }
      },

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },
  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  searchResultSettings: {
    showHitNumber: false,
    showHitObjectType: true,
    paginationMethod: "bar",
    showKeepFiltersButton: false,
    pageButtonsInBar: 5, // go to last page and check if there is no overflow!
    // order of view style buttons (left top side on the search result page)
    viewStyleButtons: [
      // "PAGE_SIZE_SELECTOR",
      "LIST_STYLE_BUTTON",
      //"IIIF_SELECTOR",
      "IIIF_BUTTON_GROUP"
    ]
  },

  i18n: {
    "de": {
      "top.headerSettings.name": "Google Books Datenanalyse",
      "top.headerSettings.name.Dev": "Google Books Datenanalyse (Dev)",
      "top.headerSettings.name.Loc": "Google Books Datenanalyse (Loc)",
      "top.headerSettings.name.Test": "Google Books Datenanalyse (Test)",
      "top.headerSettings.betaBarContact.name": "Martin Reisacher",
      "top.headerSettings.betaBarContact.email": "martin.reisacher@unibas.ch",
      "env.facetFields.type.help": "Objekttyp: Einzelseite oder Buch, hat Auswirkung auf Anzeige der vorhandenen Facetten",
      "env.facetFields.amid2.help": "zusätzliche Struktur Tag Facette, um Werte besser mit boolschen Operator (NOT) zu kombinieren"
    }
  },

};

initRdvLib(environment);
