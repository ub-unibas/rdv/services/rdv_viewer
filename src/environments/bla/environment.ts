import {
  Backend,
  FacetFieldType, FacetValueOrder,
  HistogramFieldModel,
  HistogramFieldType, initRdvLib,
  SettingsModel,
  SortOrder
} from '@rdv-lib'

export const environment: SettingsModel = {
  production: false,

  showSimpleSearch: true,
  showImagePreview: true,
  scaleDocThumbsByIiif: false,
  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "http://ub-bla.ub.unibas.ch/",
  editable: true,

  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,
  // Header-Anzeige-Einstellungen
  //scaleDocThumbsByIiif: false,
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: true
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "name.label.keyword",
      order: SortOrder.ASC,
      display: "select-sort-fields.name_label_desc"
    },
    {
      field: "signatur.keyword",
      order: SortOrder.ASC,
      display: "select-sort-fields.signatur"
    },
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "author": {
      "field": "author.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "AutorInnen",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1,
      "size": 1000,
      "expandAmount": 1000,
      "searchWithin": true,
      "autocomplete_size": 3,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "Geburtsort": {
      "field": "geburtsort.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Geburtsort",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 2,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "help": "env.facetFields.geburtsort.help",
      "hidden": false
    },
    "Beruf": {
      "field": "berufe.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Beruf",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "hidden": false
    },
    "Sterbeort": {
      "field": "sterbeort.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Sterbeort",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 3,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "hidden": true
    },
    "910_c": {
      "field": "910_c.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Rechte",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 70,
      "size": 100,
      "expandAmount": 5,
    },
    "Wirkungsort": {
      "field": "wirkungsort.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Wirkungsort",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 3,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "hidden": true
    },
    "Geschlecht": {
      "field": "geschlecht.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Geschlecht",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 3,
      "size": 100,
      "expandAmount": 10,
    },

    "wiki_categories": {
      "field": "wiki_categories.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Wikipedia Kategorien",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "size": 100,
      "expandAmount": 30,
      "searchWithin": true,
    },
    "Kategorie": {
      "field": "type.keyword",
      "facetType": FacetFieldType.CHECKBOX,
      "label": "Kategorie",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 0,
      "size": 100,
      "expandAmount": 10,
      "help": "env.facetFields.kategorie.help",
    },
    "Druck-/Verlagsort": {
      "field": "751_a.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Druck-/Verlagsort",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 6,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "Verlag": {
      "field": "264_b.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Verlag",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "help": "env.facetFields.verlag.help"
    },
    "Erscheinungsjahr": {
      "field": "year",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Erscheinungsjahr",
      "operator": "AND",
      "showAggs": true,
      "order": 10,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "Sprache": {
      "field": "lang.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Sprache",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 10,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "Bemerkung": {
      "field": "tag.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Datenanalyse",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 15,
      "size": 100,
      "expandAmount": 20,
      "help": "env.facetFields.bemerkung.help"
    },
    "typo3_len": {
      "field": "typo3_data_length",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Länge Datensatz typo3",
      "operator": "AND",
      "showAggs": true,
      "order": 16,
      "size": 16,
      "expandAmount": 31,
      "help": "env.facetFields.typo3_data_length.help"
    } as HistogramFieldModel,
    "wiki_len": {
      "field": "wiki_length",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Länge Datensatz Wikipedia",
      "operator": "AND",
      "showAggs": true,
      "order": 17,
      "size": 16,
      "expandAmount": 31,
      "help": "env.facetFields.wiki_length.help"
    } as HistogramFieldModel,
    "wiki_headers": {
      "field": "wiki_headers.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Wikipedia Ueberschriften",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 18,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "help": "env.facetFields.wiki_headers.help"
    },
    "typo3_Felder": {
      "field": "typo3_fields.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "typo3 Felder",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 18,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "help": "env.facetFields.typo3_fields.help"
    },
    "Quellen_Wikidata": {
      "field": "wikidata_sources.label.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "in Wikidata verzeichnete Quellen",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 19,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "Quellen": {
      "field": "quellen.label.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "in der GND verzeichnete Quellen",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 19,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "title": {
      "field": "title.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Titel",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 20,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3,
    },
    "bsiz_id": {
      "field": "bsiz_id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "IZ ID",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 21,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3,
    },
    "digitalisiert": {
      "field": "digitalisiert.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Digitalisiert",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 70,
      "size": 100,
      "expandAmount": 5,
    },
    "oai_datestamp": {
      "field": "oai_datestamp",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "OAI Zeitstempel",
      "operator": "AND",
      "showAggs": true,
      "order": 1066,
      "size": 31,
      "expandAmount": 31,
    } as HistogramFieldModel,
  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {},

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [5, 10, 20, 50],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 10,
    "offset": 0,
    "sortField": "name.label.keyword",
    "sortOrder": SortOrder.ASC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {},

  searchResultSettings: {
    showHitNumber: false,
    showHitObjectType: true,
    paginationMethod: "bar",
    showKeepFiltersButton: false,
    pageButtonsInBar: 5, // go to last page and check if there is no overflow!
    // order of view style buttons (left top side on the search result page)
    viewStyleButtons: [
      // "PAGE_SIZE_SELECTOR",
      //"LIST_STYLE_BUTTON",
      //"IIIF_SELECTOR",
      //"IIIF_BUTTON_GROUP"
    ]
  },

  i18n: {
    "de": {
      "env.facetFields.typo3_data_length.help": "ungefähre Zeichenanzahl des Artikels in typo3",
      "env.facetFields.wiki_length.help": "Zeichenanzahl der HTMl-Wikipedia Seite",
      "env.facetFields.wiki_headers.help": "Ueberschriften-Kategorien in der Wikipedia",
      "env.facetFields.kategorie.help": "Auswahl ob Autor oder Werk",
      "env.facetFields.geburtsort.help": "Diverse aus der GND indexierte Daten",
      "env.facetFields.verlag.help": "Diverse aus dem Katalog indexierte Daten",
      "env.facetFields.typo3_fields.help": "Felder aus Typo3 - diese sollten vereinheitlicht werden",
      "env.facetFields.bemerkung.help": "Hinweis, ob Daten in typo3 vorhanden waren, Autor nicht in GND ist, Quellen für GND DAtensätze",
      "select-sort-fields.name_label_desc": "Autor",
      "select-sort-fields.signatur": "Signatur",
      "top.headerSettings.name": "Basler Literarisches Archiv",
      "top.headerSettings.name.Dev": "Basler Literarisches Archiv (Dev)",
      "top.headerSettings.name.Loc": "Basler Literarisches Archiv (Loc)",
      "top.headerSettings.name.Test": "Basler Literarisches Archiv (Test)",
      "top.headerSettings.betaBarContact.name": "Susanne Gubser",
      "top.headerSettings.betaBarContact.email": "susanne.gubser@unibas.ch",
    },
    "en": {
      "select-sort-fields.name_label_desc": "Autor",
      "top.headerSettings.name": "Basler Literarisches Archiv",
      "top.headerSettings.name.Dev": "Basler Literarisches Archiv (Dev)",
      "top.headerSettings.name.Loc": "Basler Literarisches Archiv (Loc)",
      "top.headerSettings.name.Test": "Basler Literarisches Archiv (Test)",
      "top.headerSettings.betaBarContact.name": "Susanne Gubser",
      "top.headerSettings.betaBarContact.email": "susanne.gubser@unibas.ch",
    }
  },

};

initRdvLib(environment);
