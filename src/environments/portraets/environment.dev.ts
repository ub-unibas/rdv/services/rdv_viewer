const Proj = "portraets-dev";

import {FacetFieldType, HistogramFieldModel, HistogramFieldType, initRdvLib, SettingsModel, SortOrder} from '@rdv-lib';
import {environment as proj} from '@env/portraets/environment';
import {environment as dev} from '@env_temp/environment.type-dev';
import {addDevNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...dev,
  ...proj,

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    ...proj.sortFields,
    {
      field: "portrait_person.label.keyword",
      order: SortOrder.ASC,
      display: "Dargestellte Person Alphabetisch"
    },
    {
      field: "geburtsjahr",
      order: SortOrder.ASC,
      display: "Dargestellte Person Geburtsjahr aufsteigend"
    },
    {
      field: "geburtsjahr",
      order: SortOrder.DESC,
      display: "Dargestellte Person Geburtsjahr absteigend"
    }
  ],
  facetFields: {
    ...proj.facetFields,
    "kategorie": {
      "field": "type.keyword",
      "facetType": FacetFieldType.CHECKBOX,
      "label": "Objekttyp",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "expandAmount": 10,
      "size": 100,
      "initiallyOpen": true
    },
    "Geschlecht": {
      "field": "geschlecht.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "facetGroup": "env.facetFields.person_scr",
      "label": "Geschlecht",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 11,
      "size": 100,
      "expandAmount": 10,
      "help": "env.facetFields.fct_place.geschlecht"
    },
    "geburtsjahr": {
      "field": "geburtsjahr",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "facetGroup": "env.facetFields.person_scr",
      "label": "Geburtsjahr",
      "operator": "AND",
      "showAggs": true,
      "order": 11,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "jahrestage": {
      "field": "jahrestage.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "facetGroup": "env.facetFields.person_scr",
      "label": "Jahrestag",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 11,
      "size": 100,
      "expandAmount": 7,
      "searchWithin": true,
      "help": "env.facetFields.fct_place.jahrestage",
    },
    "Berufe": {
      "field": "berufe.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "facetGroup": "env.facetFields.person_scr",
      "label": "Beruf",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 12,
      "size": 100,
      "expandAmount": 7,
      "searchWithin": true,
    },
    "Land": {
      "field": "land.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "facetGroup": "env.facetFields.person_scr",
      "label": "Land",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 13,
      "size": 100,
      "expandAmount": 7,
      "searchWithin": true,
    },
    "wiki_categories": {
      "field": "wiki_categories.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "facetGroup": "env.facetFields.person_scr",
      "label": "Kategorien nach Wikipedia",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 14,
      "size": 100,
      "expandAmount": 7,
      "searchWithin": true,
    },
    "Datenanalyse": {
      "field": "tag.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "facetGroup": "env.facetFields.analysis_intern",
      "label": "Datenanalyse",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1000,
      "expandAmount": 10,
      "size": 100,
    },
    "digitalisiert": {
      "field": "digitalisiert.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "facetGroup": "env.facetFields.analysis_intern",
      "label": "digitalisiert",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1000,
      "expandAmount": 10,
      "size": 100,
    },
    "oai_datestamp": {
      "field": "oai_datestamp",
      "facetType": FacetFieldType.HISTOGRAM,
      "facetGroup": "env.facetFields.analysis_intern",
      "data_type": HistogramFieldType.DATE,
      "label": "OAI Zeitstempel",
      "operator": "AND",
      "showAggs": true,
      "order": 1062,
      "size": 101,
      "expandAmount": 31,
    } as HistogramFieldModel
  },
  headerSettings: addDevNamePostfix(proj.headerSettings),

  proxyUrl : dev.proxyUrl +  Proj + "/",
  moreProxyUrl: dev.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: dev.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: dev.detailProxyUrl +  Proj + "/",
  documentViewerProxyUrl: dev.documentViewerProxyUrl +  Proj + "/",
  navDetailProxyUrl: dev.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: dev.popupQueryProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: dev.suggestSearchWordProxyUrl +  Proj + "/",
};

initRdvLib(environment);
