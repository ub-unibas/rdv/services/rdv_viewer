import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  HistogramFieldModel,
  HistogramFieldType, initRdvLib,
  SettingsModel,
  SortOrder,
  ViewerType
} from '@rdv-lib'

export const environment: SettingsModel = {
  production: false,

  showSimpleSearch: true,
  showImagePreview: false,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "http://ub-fortbild.ub.unibas.ch/",
  editable: false,

  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: false
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "date",
      order: SortOrder.DESC,
      display: "select-sort-fields.date_label_desc"
    },
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "Datum": {
      "field": "date",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Datum",
      "operator": "AND",
      "showAggs": true,
      "order": 1,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "Kategorie": {
      "field": "Kategorie.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Kategorie",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 3,
      "size": 100,
      "expandAmount": 20,
      "searchWithin": false,
    },
    "Veranstalter": {
      "field": "Veranstalter.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Veranstalter",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 5,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": false,
    },
    "Titel": {
      "field": "Bezeichnung.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Titel",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 6,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "Stunden": {
      "field": "Stunden_total",
      "label": "Stunden",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "operator": "AND",
      "showAggs": true,
      "order": 7,
      "size": 100,
      "expandAmount": 20
    } as HistogramFieldModel,
    "Abteilung": {
      "field": "Abteilung.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Abteilung",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 9,
      "size": 100,
      "expandAmount": 20,
      "autocomplete_size": 3
    },
    "Name": {
      "field": "Name.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Name",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 10,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },


  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {},

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [10, 20, 50, 100],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 20,
    "offset": 0,
    "sortField": "date",
    "sortOrder": SortOrder.DESC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {},

  i18n: {
    "de": {
      "select-sort-fields.name_label_desc": "Name",
      "select-sort-fields.date_label_desc": "Datum",
      "top.headerSettings.name": "Fotbildungsdatenbank",
      "top.headerSettings.name.Dev": "Fotbildungsdatenbank (Dev)",
      "top.headerSettings.name.Loc": "Fotbildungsdatenbank (Loc)",
      "top.headerSettings.name.Test": "Fotbildungsdatenbank (Test)",
    },
    "en": {
      "select-sort-fields.name_label_desc": "Name",
      "select-sort-fields.date_label_desc": "Datum",
      "top.headerSettings.name": "Fotbildungsdatenbank",
      "top.headerSettings.name.Dev": "Fotbildungsdatenbank (Dev)",
      "top.headerSettings.name.Loc": "Fotbildungsdatenbank (Loc)",
      "top.headerSettings.name.Test": "Fotbildungsdatenbank (Test)",
    }
  },

};

initRdvLib(environment);
