import {
  FacetFieldType,
  HistogramFieldModel,
  HistogramFieldType, initRdvLib,
  SettingsModel
} from '@rdv-lib'

import {environment as itb} from '@env/itb/environment';
export const environment: SettingsModel = {
  ...itb,
  baseUrl: "https://ub-itb_int.ub.unibas.ch/",
  editable: true,
  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges

  facetFields: {
    ...itb.facetFields,
    "ITB Felder": {
      "field": "Felder.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "ITB Inhalte",
      "operator": "OR",
      "operators": [
        "OR", "AND"
      ],
      "order": 2,
      "expandAmount": 30,
      "size": 100,
      "facetGroup": "Druckerei / Verlag"
    },
    "Bibliothek": {
      "field": "852b_4.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Bibliothek",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 3,
      "expandAmount": 30,
      "size": 100,
      "facetGroup": "Drucke"
    },
    "Status": {
      "field": "page_status.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Publikationsstatus",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 4,
      "expandAmount": 30,
      "size": 100,
      "facetGroup": "Druckerei / Verlag"
    },

    "oai_datestamp": {
      "field": "oai_datestamp",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "OAI Zeitstempel",
      "operator": "AND",
      "showAggs": true,
      "order": 1062,
      "size": 31,
      "expandAmount": 31,
      "facetGroup": "Interne Analyse"
    } as HistogramFieldModel,
    "tag": {
      "field": "tag.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Datenanalyse",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1063,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "facetGroup": "Interne Analyse"
    },
    "Quellen": {
      "field": "quellen.label.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "in der GND verzeichnete Quellen",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10036,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "facetGroup": "Druckerei / Verlag"
    },
    "510_ac": {
      "field": "510_ac.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "VD Nummer (510_ac)",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10036,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "facetGroup": "Drucke"
    },
    "match_qual": {
      "field": "feature_matches",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Match Quality",
      "operator": "AND",
      "showAggs": true,
      "order": 10033,
      "size": 16,
      "expandAmount": 31,
    } as HistogramFieldModel,
  }
};

initRdvLib(environment);
