const Proj = "itb_int-dev";

import {initRdvLib, SettingsModel} from '@rdv-lib';
import {environment as proj} from '@env/itb_int/environment';
import {environment as dev} from '@env_temp/environment.type-dev';
import {addDevNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {

  ...dev,
  ...proj,
  headerSettings: addDevNamePostfix(proj.headerSettings),

  proxyUrl : dev.proxyUrl +  Proj + "/",
  moreProxyUrl: dev.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: dev.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: dev.detailProxyUrl +  Proj + "/",
  documentViewerProxyUrl: dev.documentViewerProxyUrl +  Proj + "/",
  navDetailProxyUrl: dev.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: dev.popupQueryProxyUrl +  Proj + "/",
  detailSuggestionProxyUrl: dev.detailSuggestionProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: dev.suggestSearchWordProxyUrl +  Proj + "/",
  detailEditProxyUrl: dev.detailEditProxyUrl +  Proj + "/",
  detailNewProxyUrl: dev.detailNewProxyUrl +  Proj + "/",
  creatable: [
    {
      label: {
        "de": "neuer Drucker",
        "en": "neuer Drucker",
      },
      value: "Drucker|Verleger"
    },
    {
      label: {
        "de": "neue Druckermarke",
        "en": "neue Druckermarke",
      },
      value: "Druckermarke"
    }
  ],
};

initRdvLib(environment);
