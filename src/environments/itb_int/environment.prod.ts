const Proj = "itb_int-prod";

import {initRdvLib, SettingsModel} from '@rdv-lib';
import {environment as proj} from '@env/itb_int/environment';
import {environment as prod} from '@env_temp/environment.type-prod';

export const environment: SettingsModel = {
  ...prod,
  ...proj,


  proxyUrl : "https://ub-itb-proxy.ub.unibas.ch/v1/rdv_query/es_proxy/" +  Proj + "/",
  moreProxyUrl: "https://ub-itb-proxy.ub.unibas.ch/v1/rdv_query/further_snippets/" +  Proj + "/",
  inFacetSearchProxyUrl: "https://ub-itb-proxy.ub.unibas.ch/v1/rdv_query/facet_search/" +  Proj + "/",
  detailProxyUrl: "https://ub-itb-proxy.ub.unibas.ch/v1/rdv_object/object_view/" +  Proj + "/",
  navDetailProxyUrl: "https://ub-itb-proxy.ub.unibas.ch/v1/rdv_query/next_objectview/" +  Proj + "/",
  popupQueryProxyUrl: "https://ub-itb-proxy.ub.unibas.ch/v1/rdv_query/popup_query/" +  Proj + "/",
  detailSuggestionProxyUrl: "https://ub-itb-proxy.ub.unibas.ch/v1/rdv_query/form_query/" +  Proj + "/",
  suggestSearchWordProxyUrl: "https://ub-itb-proxy.ub.unibas.ch/v1/rdv_query/autocomplete/" +  Proj + "/",
  detailEditProxyUrl: "https://ub-itb-proxy.ub.unibas.ch/v1/rdv_object/object_edit/" +  Proj + "/",
  detailNewProxyUrl: "https://ub-itb-proxy.ub.unibas.ch/v1/rdv_object/object_new/" +  Proj + "/",
  documentViewerProxyUrl: "https://ub-itb-proxy.ub.unibas.ch/v1/rdv_query/iiif_flex_pres/" +  Proj + "/",

  creatable: [
    {
      label: {
        "de": "neuer Drucker",
        "en": "neuer Drucker",
      },
      value: "Drucker|Verleger"
    },
    {
      label: {
        "de": "neue Druckermarke",
        "en": "neue Druckermarke",
      },
      value: "Druckermarke"
    }
  ],

};

initRdvLib(environment);
