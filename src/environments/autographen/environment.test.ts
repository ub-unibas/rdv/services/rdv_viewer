

const Proj = "autographen-test";

import {FacetFieldType, HistogramFieldModel, HistogramFieldType, initRdvLib, SettingsModel} from '@rdv-lib';
import {environment as proj} from '@env/autographen/environment';
import {environment as test} from '@env_temp/environment.type-test';
import {addTestNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...test,
  ...proj,

  headerSettings: addTestNamePostfix(proj.headerSettings),
  facetFields: {
    ...proj.facetFields,
    "Datenanalyse": {
      "field": "tag.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "facetGroup": "env.facetFields.analysis_intern",
      "label": "Datenanalyse",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1000,
      "expandAmount": 10,
      "size": 100,
    },
    "oai_datestamp": {
      "field": "oai_datestamp",
      "facetType": FacetFieldType.HISTOGRAM,
      "facetGroup": "env.facetFields.analysis_intern",
      "data_type": HistogramFieldType.DATE,
      "label": "OAI Zeitstempel",
      "operator": "AND",
      "showAggs": true,
      "order": 1062,
      "size": 101,
      "expandAmount": 31,
    } as HistogramFieldModel
  },
  proxyUrl : test.proxyUrl +  Proj + "/",
  moreProxyUrl: test.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: test.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: test.detailProxyUrl +  Proj + "/",
  documentViewerProxyUrl: test.documentViewerProxyUrl +  Proj + "/",
  navDetailProxyUrl: test.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: test.popupQueryProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: test.suggestSearchWordProxyUrl +  Proj + "/",
};

initRdvLib(environment);
