import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType, FacetValueOrder,
  HistogramFieldModel,
  HistogramFieldType, Page,
  initRdvLib,
  SettingsModel,
  SortOrder,
  ViewerType,
  MiradorPlugins,
  ResultViewStyleButtons
} from '@rdv-lib';

export const environment: SettingsModel = {
  extraCssClass: "env-autographen",

  documentViewer: {
    // [ViewerType.UV]: { // not working if aot/prod is built
    'UV': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Detail, order: 20}, {view: Page.Search, order: 15}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/zas_en.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'UVScroll': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/scroll_view.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "digitalisiert",
          messageTitleKey: "search-results.view-mode.UVScroll.object_type.message.title",
          messageTextKey: "search-results.view-mode.UVScroll.object_type.message.text",
          values: [
            {label: "ja", value: {"id": "ja"}}
          ]
        }
      ],
    },
    // [ViewerType.MIRADOR]: { // not working if aot/prod is built
    'Mirador': {
      enabled: true,
      type: ViewerType.MIRADOR,
      enabledForPage: [{view: Page.Search, order: 10}],
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "digitalisiert",
          messageTitleKey: "search-results.view-mode.UVScroll.object_type.message.title",
          messageTextKey: "search-results.view-mode.UVScroll.object_type.message.text",
          values: [
            {label: "ja", value: {"id": "ja"}}
          ]
        }
      ],
    },
    'MiradorScroll': {
      enabled: false,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // mögliche Werte: "share", "download","image-tools","text-overlay"
      disabledPlugins: ["text-overlay"],

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_scroll.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'MiradorSinglePageNoPreview': {
      enabled: true,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_singlenopreview.json",
      disabledPlugins: ["canvas-link", "text-overlay"],
      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'MiradorSinglePage': {
      enabled: true,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_single.json",
      disabledPlugins: ["canvas-link", "text-overlay"],
      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    }
  },


  production: false,

  showSimpleSearch: true,
  viewerExtraWide: false,
  // per Default werden Preview Images angezeigt
  showImagePreview: true,
  scaleDocThumbsByIiif: false,
  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "https://ub-autographen.ub.unibas.ch/",

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: false,
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  // footerSettings: {
  //   "default": {
  //     // den ganzen Footer ausblenden
  //     disable: false,
  //     // den "Nach Oben"-Link anzeigen
  //     displayToTop: true,
  //     // Host des Projekts im Footer anzeigen ("Universität Basel")
  //     displayHostUrl: true,
  //     // i18n key der URL des Host-Links (bei displayHostUrl: true)
  //     hostUrl: "footer.footerSettings.host.url",
  //     // i18n key des Namens des Host-Links (bei displayHostUrl: true)
  //     hostName: "footer.footerSettings.host.name",
  //     // Portal-URL im Footer anzeigen
  //     displayPortalUrl: true
  //   },
  //   // sprachspezifische Einstellungen (ohne Übersetzungen)
  //   "de": {
  //   },
  //   "en": {
  //   }
  // },

  // Dokument-Detail-Ansicht:
  // Mit 'bottom' oder 'top', wird in der Detail-Ansicht ein IIIF Viewer unterhalb oder oberhalb
  // der Felder eines Dokuments angezeigt. false schaltet die gleichzeitige Ansicht ab.
  narrowEmbeddedIiiFViewer: 'bottom',

  // UV/Mirador verwenden nur maximale Breite der app (false, undefined).
  // Wird diese Option auf true gesetzt, dann wird die volle Browser-Viewport-Breite verwendet.
  // viewerExtraWide: false,

  // die konkreten Werte, werden noch gesetzt
  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,

  externalAboutUrl: {
    "de": "https://geigy-hagenbach.ub.unibas.ch/",
    "en": "https://geigy-hagenbach.ub.unibas.ch/"
  },

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "Relevanz"
    },
    {
      field: "sort_num",
      order: SortOrder.ASC,
      display: "Sammlungsnummer"
    },
    {
      field: "normdate.keyword",
      order: SortOrder.ASC,
      display: "Entstehungsdatum aufsteigend"
    },
    {
      field: "normdate.keyword",
      order: SortOrder.DESC,
      display: "Entstehungsdatum absteigend"
    },
    {
      field: "Schreiber.label.keyword",
      order: SortOrder.ASC,
      display: "Schreiber*in Alphabetisch"
    },
    {
      field: "geburtsjahr",
      order: SortOrder.ASC,
      display: "Schreiber*in Geburtsjahr aufsteigend"
    },
    {
      field: "geburtsjahr",
      order: SortOrder.DESC,
      display: "Schreiber*in Geburtsjahr absteigend"
    }
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "Objekttyp": {
      "field": "type.keyword",
      "facetType": FacetFieldType.CHECKBOX,
      "label": "Objekttyp",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10,
      "initiallyOpen": true
    },
    "Schreiber": {
      "field": "Schreiber.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Name",
      "facetGroup": "env.facetFields.person_scr",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 10,
      "expandAmount": 7,
      "size": 100,
      "searchWithin": true,
      "autocomplete_size": 3,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "Geschlecht": {
      "field": "geschlecht.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "facetGroup": "env.facetFields.person_scr",
      "label": "Geschlecht",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 11,
      "size": 100,
      "expandAmount": 10,
      "help": "env.facetFields.fct_place.geschlecht"
    },
    "geburtsjahr": {
      "field": "geburtsjahr",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "facetGroup": "env.facetFields.person_scr",
      "label": "Geburtsjahr",
      "operator": "AND",
      "showAggs": true,
      "order": 11,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "jahrestage": {
      "field": "jahrestage.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "facetGroup": "env.facetFields.person_scr",
      "label": "Jahrestag",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 11,
      "size": 100,
      "expandAmount": 7,
      "searchWithin": true,
      "help": "env.facetFields.fct_place.jahrestage",
    },
    "Berufe": {
      "field": "berufe.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "facetGroup": "env.facetFields.person_scr",
      "label": "Beruf",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 12,
      "size": 100,
      "expandAmount": 7,
      "searchWithin": true,
    },
    "Land": {
      "field": "land.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "facetGroup": "env.facetFields.person_scr",
      "label": "Land",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 13,
      "size": 100,
      "expandAmount": 7,
      "searchWithin": true,
    },
    "wiki_categories": {
      "field": "wiki_categories.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "facetGroup": "env.facetFields.person_scr",
      "label": "Kategorien nach Wikipedia",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 14,
      "size": 100,
      "expandAmount": 7,
      "searchWithin": true,
    },
    "adressat": {
      "field": "adressat.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Adressat*in",
      "facetGroup": "env.facetFields.object_description",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 20,
      "expandAmount": 7,
      "size": 100,
      "searchWithin": true,
      "autocomplete_size": 3,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "751_a": {
      "field": "751_a.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "facetGroup": "env.facetFields.object_description",
      "label": "Entstehungsort",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 21,
      "expandAmount": 7,
      "size": 100,
      "searchWithin": true,
      "autocomplete_size": 3,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "normdate2": {
      "field": "normdate2",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "facetGroup": "env.facetFields.object_description",
      "label": "Entstehungsdatum",
      "operator": "AND",
      "showAggs": true,
      "order": 22,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "Sprache": {
      "field": "lang.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "facetGroup": "env.facetFields.object_description",
      "label": "Sprache",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 23,
      "expandAmount": 7,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
    },

    "frueheigen": {
      "field": "frueheigen.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "facetGroup": "env.facetFields.object_history",
      "label": "Vorbesitzer*in",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 40,
      "expandAmount": 7,
      "size": 100,
      "searchWithin": true,
      "autocomplete_size": 3,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "verkaeufer": {
      "field": "verkaeufer.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "facetGroup": "env.facetFields.object_history",
      "label": "Verkäufer*in",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 41,
      "expandAmount": 7,
      "size": 100,
      "searchWithin": true,
      "autocomplete_size": 3,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "Erwerbungsjahr": {
      "field": "Jahr",
      "facetType": FacetFieldType.HISTOGRAM,
      "facetGroup": "env.facetFields.object_history",
      "data_type": HistogramFieldType.DATE,
      "label": "Jahr der Erwerbung",
      "operator": "AND",
      "showAggs": true,
      "order": 42,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "Album": {
      "field": "Faszikel.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "facetGroup": "env.facetFields.object_history",
      "label": "Original-Alben von Geigy",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 43,
      "expandAmount": 7,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "hierarchy_filter": {
      "field": "hierarchy_filter_han2.keyword",
      "facetType": FacetFieldType.HIERARCHIC,
      "facetGroup": "env.facetFields.object_history",
      "label": "Struktur in Geigys Katalog",
      "operator": "AND",
      "order": 44,
      "size": 100,
      "expandAmount": 100
    },
    "830_v": {
      "field": "830_v.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "facetGroup": "env.facetFields.object_history",
      "label": "Slg. Nummer Geigy",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 45,
      "searchWithin": true,
      "expandAmount": 7,
      "size": 100,
      "valueOrder": FacetValueOrder.LABEL,
    },
    "digitalisiert_boolean": {
      "field": "digitalisiert_boolean",
      "facetType": FacetFieldType.BOOLEAN,
      "label": "Digitalisiert",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1200,
      "size": 100,
      "expandAmount": 5,
    },
    "transkription_boolean": {
      "field": "transkription_boolean",
      "facetType": FacetFieldType.BOOLEAN,
      "label": "Transkription",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1201,
      "size": 100,
      "expandAmount": 5,
    },
  },


  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {},

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [5, 10, 25, 50],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 10,
    "offset": 0,
    "sortField": "_score",
    "sortOrder": SortOrder.DESC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
    "fct_location": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "fct_location",
      "label": "Geographikum"
    }
  },
  searchResultSettings: {
    showHitNumber: false,
    showHitObjectType: true,
    paginationMethod: "bar",
    showKeepFiltersButton: false,
    pageButtonsInBar: 5, // go to last page and check if there is no overflow!
    // order of view style buttons (left top side on the search result page)
    viewStyleButtons: [
      // "PAGE_SIZE_SELECTOR",
      //"LIST_STYLE_BUTTON",
      //"IIIF_SELECTOR",
      //"IIIF_BUTTON_GROUP"
    ]
  },
  i18n: {
    "de": {
      "top.headerSettings.name": "Autographensammlung",
      "top.headerSettings.name.Dev": "Autographensammlung (Dev)",
      "top.headerSettings.name.Loc": "Autographensammlung (Loc)",
      "top.headerSettings.name.Test": "Autographensammlung (Test)",
      "top.headerSettings.betaBarContact.name": "Historischen Sammlungen",
      "top.headerSettings.betaBarContact.email": "noah.regenass@unibas.ch",
      "env.facetFields.analysis_intern": "Interne Analyse",
      "env.facetFields.object_history": "Geschichte des Objekts",
      "env.facetFields.person_scr": "Schreiber*in",
      "env.facetFields.gnd.help": "Die folgenden Facetten sind nur für mit der GND verknüpfte Personen vorhanden.",
      "env.facetFields.fct_place.jahrestage": "Dabei handelt es sich um Geburts- und Sterbetage der Schreiber*Innen.",
      "env.facetFields.fct_place.geschlecht": "Die folgenden Facetten sind nur soweit in der GND verzeichnet vorhanden."
    },
    "en": {
      "top.headerSettings.name": "autographs collection",
      "top.headerSettings.name.Dev": "autographs collection (Dev)",
      "top.headerSettings.name.Loc": "autographs collection (Loc)",
      "top.headerSettings.name.Test": "autographs collection (Test)",
      "top.headerSettings.betaBarContact.name": "Historischen Sammlungen",
      "top.headerSettings.betaBarContact.email": "noah.regenass@unibas.ch",
    }
  },

};

initRdvLib(environment, [MiradorPlugins.PLUGIN_OCR_HELPER]);
