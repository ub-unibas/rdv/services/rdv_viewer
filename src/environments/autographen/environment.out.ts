const Proj = "autographen-dev";

import { initRdvLib, SettingsModel, ResultViewStyleButtons } from "@rdv-lib";
import { environment as proj } from "@env/autographen/environment";
import { environment as outermedia } from "@env_temp/environment.type-out";

export const environment: SettingsModel = {
  ...outermedia,
  ...proj,

  proxyUrl: outermedia.proxyUrl + Proj + "/",
  moreProxyUrl: outermedia.moreProxyUrl + Proj + "/",
  inFacetSearchProxyUrl: outermedia.inFacetSearchProxyUrl + Proj + "/",
  detailProxyUrl: outermedia.detailProxyUrl + Proj + "/",
  documentViewerProxyUrl: outermedia.documentViewerProxyUrl + Proj + "/",
  navDetailProxyUrl: outermedia.navDetailProxyUrl + Proj + "/",
  popupQueryProxyUrl: outermedia.popupQueryProxyUrl + Proj + "/",
  suggestSearchWordProxyUrl: outermedia.suggestSearchWordProxyUrl + Proj + "/",
  searchResultSettings: {
    showHitNumber: false,
    showHitObjectType: false,
    paginationMethod: "bar",
    pageButtonsInBar: 5, // go to last page and check if there is no overflow!
    // order of view style buttons (left top side on the search result page)
    viewStyleButtons: [
      // "PAGE_SIZE_SELECTOR",
      "LIST_STYLE_BUTTON",
      // "IIIF_SELECTOR",
      "IIIF_BUTTON_GROUP"
    ],
    showKeepFiltersButton: false,
  },
};

initRdvLib(environment);
