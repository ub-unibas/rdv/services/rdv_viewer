const Proj = "autographen-dev";

import {FacetFieldType, HistogramFieldModel, HistogramFieldType, initRdvLib, SettingsModel} from '@rdv-lib';
import {environment as proj} from '@env/autographen/environment';
import {environment as dev} from '@env_temp/environment.type-dev';
import {addDevNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...dev,
  ...proj,

  headerSettings: addDevNamePostfix(proj.headerSettings),
  facetFields: {
    ...proj.facetFields,
    "Datenanalyse": {
      "field": "tag.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "facetGroup": "env.facetFields.analysis_intern",
      "label": "Datenanalyse",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1000,
      "expandAmount": 10,
      "size": 100,
    },
    "oai_datestamp": {
      "field": "oai_datestamp",
      "facetType": FacetFieldType.HISTOGRAM,
      "facetGroup": "env.facetFields.analysis_intern",
      "data_type": HistogramFieldType.DATE,
      "label": "OAI Zeitstempel",
      "operator": "AND",
      "showAggs": true,
      "order": 1062,
      "size": 101,
      "expandAmount": 31,
    } as HistogramFieldModel
  },
  proxyUrl : dev.proxyUrl +  Proj + "/",
  moreProxyUrl: dev.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: dev.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: dev.detailProxyUrl +  Proj + "/",
  documentViewerProxyUrl: dev.documentViewerProxyUrl +  Proj + "/",
  navDetailProxyUrl: dev.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: dev.popupQueryProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: dev.suggestSearchWordProxyUrl +  Proj + "/",
};

initRdvLib(environment);
