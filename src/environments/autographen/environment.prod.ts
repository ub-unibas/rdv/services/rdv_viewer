const Proj = "autographen-prod";

import {initRdvLib, SettingsModel} from '@rdv-lib';
import {environment as proj} from '@env/autographen/environment';
import {environment as prod} from '@env_temp/environment.type-prod';

export const environment: SettingsModel = {
  ...prod,
  ...proj,

  proxyUrl : "https://ub-autographen-proxy.ub.unibas.ch/v1/rdv_query/es_proxy/" +  Proj + "/",
  moreProxyUrl: "https://ub-autographen-proxy.ub.unibas.ch/v1/rdv_query/further_snippets/" +  Proj + "/",
  inFacetSearchProxyUrl: "https://ub-autographen-proxy.ub.unibas.ch/v1/rdv_query/facet_search/" +  Proj + "/",
  detailProxyUrl: "https://ub-autographen-proxy.ub.unibas.ch/v1/rdv_object/object_view/" +  Proj + "/",
  documentViewerProxyUrl: "https://ub-autographen-proxy.ub.unibas.ch/v1/rdv_query/iiif_flex_pres/" +  Proj + "/",
  navDetailProxyUrl: "https://ub-autographen-proxy.ub.unibas.ch/v1/rdv_query/next_objectview/" +  Proj + "/",
  popupQueryProxyUrl: "https://ub-autographen-proxy.ub.unibas.ch/v1/rdv_query/popup_query/" +  Proj + "/",
  detailSuggestionProxyUrl: "https://ub-autographen-proxy.ub.unibas.ch/v1/rdv_query/form_query/" +  Proj + "/",
  suggestSearchWordProxyUrl: "https://ub-autographen-proxy.ub.unibas.ch/v1/rdv_query/autocomplete/" +  Proj + "/",
};

initRdvLib(environment);
