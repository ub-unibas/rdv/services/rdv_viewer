import {environment as prod_k8} from "@env_temp/environment.type-prod_k8s_dd";

const Proj = "swa_pdfs-prod_k8s";

import {initRdvLib, SettingsModel} from '@rdv-lib';
import {environment as proj} from '@env/swa_pdfs/environment';
import {addTestNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {

  ...prod_k8,
  ...proj,
  headerSettings: addTestNamePostfix(proj.headerSettings),

  proxyUrl : prod_k8.proxyUrl +  Proj + "/",
  moreProxyUrl: prod_k8.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: prod_k8.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: prod_k8.detailProxyUrl +  Proj + "/",
  navDetailProxyUrl: prod_k8.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: prod_k8.popupQueryProxyUrl +  Proj + "/",
  detailSuggestionProxyUrl: prod_k8.detailSuggestionProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: prod_k8.suggestSearchWordProxyUrl +  Proj + "/",
  detailEditProxyUrl: "http://127.0.0.1:5000/v2/rdv_object/object_edit/" +  Proj + "/",
  detailNewProxyUrl: "http://127.0.0.1:5000/v2/rdv_object/object_new/" +  Proj + "/",
  creatable: [
    {
      label: {
        "de": "neues PDF",
        "en": "scanned press clipping"
      },
      value: "swadok_pdfs"
    }
  ],
};
initRdvLib(environment);
