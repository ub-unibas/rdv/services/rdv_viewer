const Proj = "swa_pdfs-dev";
import {initRdvLib, SettingsModel} from '@rdv-lib';
import {environment as proj} from '@env/swa_pdfs/environment';
import {environment as dev} from '@env_temp/environment.type-dev';
import {addDevNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {

  ...dev,
  ...proj,
  headerSettings: addDevNamePostfix(proj.headerSettings),
  documentViewerProxyUrl: undefined,
  editable: true,

  proxyUrl : "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_query/es_proxy/" +  Proj + "/",
  moreProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_query/further_snippets/" +  Proj + "/",
  inFacetSearchProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_query/facet_search/" +  Proj + "/",
  detailProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_object/object_view/" +  Proj + "/",
  navDetailProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_query/next_objectview/" +  Proj + "/",
  popupQueryProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_query/popup_query/" +  Proj + "/",
  detailSuggestionProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_query/form_query/" +  Proj + "/",
  suggestSearchWordProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_query/autocomplete/" +  Proj + "/",
  detailEditProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_object/object_edit/" +  Proj + "/",
  detailNewProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_object/object_new/" +  Proj + "/",
  creatable: [
    {
      label: {
        "de": "neues PDF",
        "en": "scanned press clipping"
      },
      value: "swadok_pdfs"
    }
  ],
};
initRdvLib(environment);
