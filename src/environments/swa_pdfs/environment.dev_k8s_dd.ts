import {environment as dev_k8s} from "@env_temp/environment.type-dev_k8s_dd";

const Proj = "swa_pdfs-dev_k8s";

import {initRdvLib, SettingsModel} from '@rdv-lib';
import {environment as proj} from '@env/swa_pdfs/environment';
import {addTestNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {

  ...dev_k8s,
  ...proj,
  headerSettings: addTestNamePostfix(proj.headerSettings),

  proxyUrl : dev_k8s.proxyUrl +  Proj + "/",
  moreProxyUrl: dev_k8s.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: dev_k8s.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: dev_k8s.detailProxyUrl +  Proj + "/",
  navDetailProxyUrl: dev_k8s.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: dev_k8s.popupQueryProxyUrl +  Proj + "/",
  detailSuggestionProxyUrl: dev_k8s.detailSuggestionProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: dev_k8s.suggestSearchWordProxyUrl +  Proj + "/",
  detailEditProxyUrl: "http://127.0.0.1:5000/v2/rdv_object/object_edit/" +  Proj + "/",
  detailNewProxyUrl: "http://127.0.0.1:5000/v2/rdv_object/object_new/" +  Proj + "/",
  creatable: [
    {
      label: {
        "de": "neues PDF",
        "en": "scanned press clipping"
      },
      value: "swadok_pdfs"
    }
  ],
};
initRdvLib(environment);
