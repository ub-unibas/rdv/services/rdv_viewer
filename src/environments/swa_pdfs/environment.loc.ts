import {environment as test} from "@env_temp/environment.type-test";

const Proj = "swa_pdfs-loc";
import {initRdvLib, SettingsModel} from '@rdv-lib';
import {environment as proj} from "@env/swa_pdfs/environment";
import {environment as loc} from "@env_temp/environment.type-loc";
import {addLocNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...proj,
  ...loc,
  headerSettings: addLocNamePostfix(proj.headerSettings),
  documentViewerProxyUrl: undefined,

  proxyUrl : loc.proxyUrl +  Proj + "/",
  moreProxyUrl: loc.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: loc.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: loc.detailProxyUrl +  Proj + "/",
  navDetailProxyUrl: loc.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: loc.popupQueryProxyUrl +  Proj + "/",
  detailSuggestionProxyUrl: loc.detailSuggestionProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: loc.suggestSearchWordProxyUrl +  Proj + "/",
  detailEditProxyUrl: "http://127.0.0.1:5000/v2/rdv_object/object_edit/" +  Proj + "/",
  detailNewProxyUrl: "http://127.0.0.1:5000/v2/rdv_object/object_new/" +  Proj + "/",
  creatable: [
    {
      label: {
        "de": "neues PDF",
        "en": "scanned press clipping"
      },
      value: "swadok_pdfs"
    }
  ],
};
initRdvLib(environment);
