const Proj = "swa_pdfs-test";
import {initRdvLib, SettingsModel} from '@rdv-lib';
import {environment as proj} from '@env/swa_pdfs/environment';
import {environment as test} from '@env_temp/environment.type-test';
import {addTestNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...proj,
  ...test,
  headerSettings: addTestNamePostfix(proj.headerSettings),
  documentViewerProxyUrl: undefined,

  proxyUrl : "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_query/es_proxy/" +  Proj + "/",
  moreProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_query/further_snippets/" +  Proj + "/",
  inFacetSearchProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_query/facet_search/" +  Proj + "/",
  detailProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_object/object_view/" +  Proj + "/",
  navDetailProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_query/next_objectview/" +  Proj + "/",
  popupQueryProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_query/popup_query/" +  Proj + "/",
  detailSuggestionProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_query/form_query/" +  Proj + "/",
  suggestSearchWordProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_query/autocomplete/" +  Proj + "/",
  detailEditProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_object/object_edit/" +  Proj + "/",
  detailNewProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_object/object_new/" +  Proj + "/",
  creatable: [
    {
      label: {
        "de": "neues PDF",
        "en": "scanned press clipping"
      },
      value: "swadok_pdfs"
    }
  ],
  i18n: {
    "de": {
      "top.headerSettings.name": "SWA Kleinschriften",
      "top.headerSettings.name.Dev": "SWA Kleinschriften (Dev)",
      "top.headerSettings.name.Loc": "SWA Kleinschriften (Loc)",
      "top.headerSettings.name.Test": "SWA Kleinschriften (Test)",
      "top.headerSettings.betaBarContact.name": "UB Wirtschaft - SWA",
      "top.headerSettings.betaBarContact.email": "info-ubw-swa@unibas.ch",
    },
    "en": {
      "top.headerSettings.name": "SWA Kleinschriften",
      "top.headerSettings.name.Dev": "SWA Kleinschriften (Dev)",
      "top.headerSettings.name.Loc": "SWA Kleinschriften (Loc)",
      "top.headerSettings.name.Test": "SWA Kleinschriften (Test)",
      "top.headerSettings.betaBarContact.name": "UB Wirtschaft - SWA",
      "top.headerSettings.betaBarContact.email": "info-ubw-swa@unibas.ch",
    }
  },
};
initRdvLib(environment);
