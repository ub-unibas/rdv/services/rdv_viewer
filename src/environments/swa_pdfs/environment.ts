import {
  FacetFieldType,
  HistogramFieldModel,
  HistogramFieldType,
  initRdvLib,
  SettingsModel,
  SortOrder
} from "@rdv-lib";

import {environment as swa} from '@env/swa/environment';

export const environment: SettingsModel = {
  ...swa,
  editable: true,
  showImagePreview: true,

  externalAboutUrl: {
    "de": "https://ub-swasearch.ub.unibas.ch/"
  },
  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: true,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: false
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },
  facetFields: {
    ...swa.facetFields,
    "title2": {
      "field": "title_autocomplete.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "IZ ID Datensatz",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "Type": {
      "field": "type_autocomplete.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Typ",
      "operator": "OR",
      "operators": ["OR", "AND", "NOT"],
      "order": 1,
      "size": 100,
      "expandAmount": 10
    },
    "Lokalcode": {
      "field": "990_f.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "lokaler Code",
      "operator": "OR",
      "operators": ["OR", "AND", "NOT"],
      "order": 1,
      "size": 100,
      "expandAmount": 10
    },
    "delete": {
      "field": "delete",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Gelöscht",
      "operator": "OR",
      "operators": [
        "AND", "OR", "NOT"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10,
    },
    "sys_created": {
      "field": "sys_created",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Datum Upload",
      "operator": "AND",
      "showAggs": true,
      "order": 10,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "sys_updated": {
      "field": "sys_updated",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Aktualisiert",
      "operator": "AND",
      "showAggs": true,
      "order": 10,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "tag": {
      "field": "tag.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Datenanalyse",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 50,
      "size": 100,
      "expandAmount": 20,
    },
    "filename": {
      "field": "pdf_filename.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Dateiname",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 21,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "bsiz_id": {
      "field": "bsiz_id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "IZ ID",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 21,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "page_sysid": {
      "field": "page_sysid.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Systemnummer DSV01",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 22,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    }
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "date",
      order: SortOrder.ASC,
      display: "Erscheinungsdatum (älteste)"
    },
    {
      field: "date",
      order: SortOrder.DESC,
      display: "Erscheinungsdatum (neueste)"
    },
    {
      field: "sys_created",
      order: SortOrder.ASC,
      display: "Upload (älteste)"
    },
    {
      field: "sys_created",
      order: SortOrder.DESC,
      display: "Upload (neueste)"
    },
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "select-sort-fields.score_desc"
    }
  ],

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [20, 50, 100, 200],

  searchResultSettings: {
    showHitNumber: true,
    showHitObjectType: true,
    paginationMethod: "bar",
    showKeepFiltersButton: true,
    pageButtonsInBar: 5, // go to last page and check if there is no overflow!
    // order of view style buttons (left top side on the search result page)
    viewStyleButtons: [
      "PAGE_SIZE_SELECTOR",
      //"LIST_STYLE_BUTTON",
      //"IIIF_SELECTOR",
      //"IIIF_BUTTON_GROUP"
    ]
  },

  i18n: {
    "de": {
      "detailed.search_result": "Zurück",
      "detailed.edit_save": "Upload / Speichern",
      "detailed.new": "Upload neues PDF / Korrektur Metadaten",
      "simple-search.new_doc.label": "Neuer Upload",
      "header.about": "SWA Search",
      "env.facetFields.fct_pubyear": "Erscheinungsjahr",
      "env.facetFields.fct_access": "Zugänglichkeit",
      "env.facetFields.fct_objecttype": "Objekttyp",
      "env.facetFields.fct_author": "Verfasser/in",
      "env.facetFields.descr_han_hierarchy_han_filter": "Bestandsgliederung",
      "env.facetFields.descr_han_hierarchy_per_filter": "Bestandsgliederung Nachlässe",
      "env.facetFields.descr_han_hierarchy_verband_filter": "Bestandsgliederung Verbände",
      "top.headerSettings.name": "SWA PDF Upload Tool",
      "top.headerSettings.name.Dev": "SWA PDF Upload Tool (Dev)",
      "top.headerSettings.name.Loc": "SWA PDF Upload Tool (Loc)",
      "top.headerSettings.name.Test": "SWA PDF Upload Tool (Test)",
      "top.headerSettings.betaBarContact.name": "UB Wirtschaft - SWA",
      "top.headerSettings.betaBarContact.email": "info-ubw-swa@unibas.ch",
    },
  }
}

initRdvLib(environment);
