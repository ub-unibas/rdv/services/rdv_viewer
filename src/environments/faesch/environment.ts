import {
  ExtraInfoFieldType,
  FacetFieldType, FacetValueOrder,
  HistogramFieldModel,
  HistogramFieldType,
  initRdvLib,
  SettingsModel,
  SortOrder
} from '@rdv-lib';

import {environment as autographen} from '@env/autographen/environment';

export const environment: SettingsModel = {
  ...autographen,

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: true
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "Relevanz"
    },
    {
      field: "year",
      order: SortOrder.ASC,
      display: "Erscheinungsjahr"
    }
  ],

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    ...autographen.facetFields,
    "Objekttyp": {
      "field": "type.keyword",
      "facetType": FacetFieldType.CHECKBOX,
      "label": "Objekt / AutorIn",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10,
      "initiallyOpen": true
    },
    "hierarchy_filter": {
      "field": "hierarchy_filter_han2.keyword",
      "facetType": FacetFieldType.HIERARCHIC,
      "label": "Archiv-Tektonik",
      "operator": "AND",
      "order": 2,
      "size": 100,
      "expandAmount": 100
    },
    "adress_empf": {
      "field": "adress_empf.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "VerfasserIn",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 3,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "autocomplete_size": 3,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "DruckerIn": {
      "field": "Drucker.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "DruckerIn",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 3,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "Druck-/Verlagsort": {
      "field": "751_a.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Druck-/Verlagsort",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 3,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },

    "year": {
      "field": "year",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Erscheinungsjahr",
      "operator": "AND",
      "showAggs": true,
      "order": 4,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "Sprache": {
      "field": "lang.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Sprache",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 5,
      "expandAmount": 10,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "Gattung": {
      "field": "655_a_7.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Gattung",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 6,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
    },

    "340_a": {
      "field": "340_a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Material",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 7,
      "expandAmount": 20,
      "size": 100,
      "valueOrder": FacetValueOrder.COUNT,
    },

    "990_c": {
      "field": "990_c.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "lokaler Code",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 8,
      "expandAmount": 20,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "Signatur": {
      "field": "split_signatur.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Signatur / Signaturgruppen",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 10,
      "expandAmount": 20,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "Umfang": {
      "field": "300_a_norm.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Datenmuster Umfang",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 11,
      "expandAmount": 20,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT]
    },
    "Masse": {
      "field": "300_c_norm.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Datenmuster Maße",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 12,
      "expandAmount": 20,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT]
    },

    "Anmerkung": {
      "field": "500_a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Anmerkung",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 13,
      "expandAmount": 20,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "Bib Nachweis": {
      "field": "510_a_norm.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Bibliographischer Nachweis",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 14,
      "expandAmount": 20,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "Literatur": {
      "field": "581_a_norm.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Literatur",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 14,
      "expandAmount": 20,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "Exemplar": {
      "field": "590561563_a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Anmerkungen zum Exemplar",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 14,
      "expandAmount": 20,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "title": {
      "field": "title.label.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Titel",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 15,
      "expandAmount": 20,
      "size": 100,
      "searchWithin": true,
      "autocomplete_size": 3,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "Geschlecht": {
      "field": "geschlecht.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "GND Geschlecht",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 24,
      "size": 100,
      "expandAmount": 10,
    },
    "Land": {
      "field": "land.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "GND Land",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 28,
      "size": 100,
      "expandAmount": 30,
    },
    "Berufe": {
      "field": "berufe.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "GND Beruf",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 24,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "Geburtsort": {
      "field": "geburtsort.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "GND Geburtsort",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 27,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "Wirkungsort": {
      "field": "wirkungsort.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "GND Wirkungsort",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 27,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "Sterbeort": {
      "field": "sterbeort.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "GND Sterbeort",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 27,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "wiki_categories": {
      "field": "wiki_categories.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Wikipedia Kategorien",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 32,
      "size": 100,
      "expandAmount": 30,
      "searchWithin": true,
    },
    "wiki_orig_categories": {
      "field": "wiki_orig_categories.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Wikipedia vollst. Kategorien",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 32,
      "size": 100,
      "expandAmount": 30,
      "searchWithin": true,
    },


    "Personeneintrag (alle Rollen)": {
      "field": "700_abcd.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Personeneintrag (alle Rollen)",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 50,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "Koerperschaftseintrag (alle Rollen)": {
      "field": "710_a.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Koerperschaftseintrag (alle Rollen)",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 51,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "rel_persons2": {
      "field": "rel_persons2",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Personeneintrag Rolle",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 53,
      "expandAmount": 100,
      "size": 100,
    },
    "rel_persons2 Auswahl": {
      "field": "rel_persons2.scr.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Personeneintrag Auswahl",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 54,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
    },
    "digitalisiert": {
      "field": "digitalisiert.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Digitalisiert",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 70,
      "size": 100,
      "expandAmount": 5,
    },
    "910_c": {
      "field": "910_c.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Rechte",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 70,
      "size": 100,
      "expandAmount": 5,
    },
    "Datenanalyse": {
      "field": "tag.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Datenanalyse",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 100,
      "expandAmount": 10,
      "size": 100,
    },
    "Datenanalyse MarcFelder": {
      "field": "record_fields.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Datenanalyse MarcFelder",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 100,
      "expandAmount": 50,
      "size": 300,
      "searchWithin": true,
    },
    "Datenanalyse MarcSubFelder": {
      "field": "record_fields_subcodes.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Datenanalyse MarcSubFelder",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 100,
      "expandAmount": 50,
      "size": 1000,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "record_fields_count": {
      "field": "record_fields_count",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Datenanalyse Anzahl MarcFelder",
      "operator": "AND",
      "showAggs": true,
      "order": 1062,
      "size": 31,
      "expandAmount": 31,
    } as HistogramFieldModel,
    "record_length": {
      "field": "record_length",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Datenanalyse Länge MarcXML",
      "operator": "AND",
      "showAggs": true,
      "order": 1062,
      "size": 31,
      "expandAmount": 31,
    } as HistogramFieldModel,
    "oai_datestamp": {
      "field": "oai_datestamp",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "OAI Zeitstempel",
      "operator": "AND",
      "showAggs": true,
      "order": 1066,
      "size": 31,
      "expandAmount": 31,
    } as HistogramFieldModel,
  },


  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {},

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [5, 10, 25, 50],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 10,
    "offset": 0,
    "sortField": "_score",
    "sortOrder": SortOrder.DESC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
      "fct_location": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "fct_location",
      "label": "Geographikum"
      }
      },
  searchResultSettings: {
    showHitNumber: false,
    showHitObjectType: true,
    paginationMethod: "bar",
    showKeepFiltersButton: false,
    pageButtonsInBar: 5, // go to last page and check if there is no overflow!
    // order of view style buttons (left top side on the search result page)
    viewStyleButtons: [
      // "PAGE_SIZE_SELECTOR",
      //"LIST_STYLE_BUTTON",
      //"IIIF_SELECTOR",
      //"IIIF_BUTTON_GROUP"
    ]
  },
  i18n: {
    "de": {
      "top.headerSettings.name": "Museum Faesch",
      "top.headerSettings.name.Dev": "Museum Faesch (Dev)",
      "top.headerSettings.name.Loc": "Museum Faesch (Loc)",
      "top.headerSettings.name.Test": "Museum Faesch (Test)",
      "top.headerSettings.betaBarContact.name": "Martin Reisacher",
      "top.headerSettings.betaBarContact.email": "martin.reisacher@unibas.ch",
      "env.facetFields.gnd.help": "Die folgenden Facetten sind nur für mit der GND verknüpfte Personen vorhanden."
    },
    "en": {
      "top.headerSettings.name": "museum faesch",
      "top.headerSettings.name.Dev": "museum faesch (Dev)",
      "top.headerSettings.name.Loc": "museum faesch (Loc)",
      "top.headerSettings.name.Test": "museum faesch (Test)",
      "top.headerSettings.betaBarContact.name": "Martin Reisacher",
      "top.headerSettings.betaBarContact.email": "martin.reisacher@unibas.ch",
    }
  },

};

initRdvLib(environment);
