const Proj = "afrikaportal-compose";

import {initRdvLib, SettingsModel} from '@rdv-lib'
import {environment as proj} from "@env/afrikaportal/environment";
import {environment as compose} from "@env_temp/environment.type-compose";
import {addComposeNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...compose,
  ...proj,

  headerSettings: addComposeNamePostfix(proj.headerSettings),

  proxyUrl : compose.proxyUrl +  Proj + "/",
  moreProxyUrl: compose.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: compose.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: compose.detailProxyUrl +  Proj + "/",
  documentViewerProxyUrl: compose.documentViewerProxyUrl +  Proj + "/",
  navDetailProxyUrl: compose.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: compose.popupQueryProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: compose.suggestSearchWordProxyUrl +  Proj + "/",
};

initRdvLib(environment);
