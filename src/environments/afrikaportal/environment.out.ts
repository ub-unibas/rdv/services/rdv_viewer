const Proj = "afrikaportal-test";

import { initRdvLib } from "@rdv-lib";
import { environment as proj } from "@env/afrikaportal/environment";
import { environment as outermedia } from "@env_temp/environment.type-out";
import { addLocNamePostfix } from "@env_temp/util";
import { initMapReducer, MapSettingsModel } from "@rdv-lib";

const omProxy = "ub-rdv-outermedia-proxy.ub.unibas.ch";
const afrikaPortalProxy =
  "test.rdv-query-builder.ub-digitale-dienste.k8s-001.unibas.ch";
// workaround, because outermedia specific proxy isn't definied yet
const updProxy = (s: string): string => s.replace(omProxy, afrikaPortalProxy);

export const environment: MapSettingsModel = {
  ...outermedia,
  ...proj,

  headerSettings: addLocNamePostfix(proj.headerSettings),
  countriesProxyUrl: outermedia.proxyUrl + Proj + "/",

  proxyUrl: updProxy(outermedia.proxyUrl) + Proj + "/",
  moreProxyUrl: updProxy(outermedia.moreProxyUrl) + Proj + "/",
  inFacetSearchProxyUrl:
    updProxy(outermedia.inFacetSearchProxyUrl) + Proj + "/",
  detailProxyUrl: updProxy(outermedia.detailProxyUrl) + Proj + "/",
  documentViewerProxyUrl:
    updProxy(outermedia.documentViewerProxyUrl) + Proj + "/",
  navDetailProxyUrl: updProxy(outermedia.navDetailProxyUrl) + Proj + "/",
  popupQueryProxyUrl: updProxy(outermedia.popupQueryProxyUrl) + Proj + "/",
  suggestSearchWordProxyUrl:
    updProxy(outermedia.suggestSearchWordProxyUrl) + Proj + "/",
  searchResultSettings: {
    showHitNumber: false,
    showHitObjectType: false,
    paginationMethod: "bar",
    pageButtonsInBar: 5, // go to last page and check if there is no overflow!
    // order of view style buttons (left top side on the search result page)
    viewStyleButtons: [
      // "PAGE_SIZE_SELECTOR",
      "LIST_STYLE_BUTTON",
      // "IIIF_SELECTOR",
      "IIIF_BUTTON_GROUP",
    ],
    showKeepFiltersButton: false,
  },
};
initRdvLib(environment);
const mapConfig = environment.mapConfig.config;
initMapReducer(mapConfig.zoom, mapConfig.lat, mapConfig.lng);
