const Proj = "afrikaportal-dev_k8s";

import {initRdvLib, SettingsModel} from '@rdv-lib';
import {environment as proj} from '@env/afrikaportal/environment';
import {environment as dev} from '@env_temp/environment.type-dev_k8s';
import {addTestNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {

  ...dev,
  ...proj,
  headerSettings: addTestNamePostfix(proj.headerSettings),

  proxyUrl : dev.proxyUrl +  Proj + "/",
  moreProxyUrl: dev.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: dev.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: dev.detailProxyUrl +  Proj + "/",
  documentViewerProxyUrl: dev.documentViewerProxyUrl +  Proj + "/",
  navDetailProxyUrl: dev.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: dev.popupQueryProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: dev.suggestSearchWordProxyUrl +  Proj + "/",
};

initRdvLib(environment);
