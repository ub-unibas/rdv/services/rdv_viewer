import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  HistogramFieldModel,
  HistogramFieldType,
  initRdvLib,
  MiradorPlugins,
  Page,
  SettingsModel,
  SortOrder,
  ViewerType
} from '@rdv-lib'

export const environment: SettingsModel = {
  production: false,

  documentViewer: {
    // [ViewerType.MIRADOR]: { // not working if aot/prod is built
    'Mirador': {
      enabled: true,
      type: ViewerType.MIRADOR,
      enabledForPage: [{view: Page.Search, order: 10}],
      viewerUrl: "/assets/mirador/init.html",
      disabledPlugins: [],
      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_pres.json",
      // Optional kann pro Breakpoint eine andere Konfiguration geladen werden.
      // Prinzipiell werden die Konfigurationen verwendet, wo die aktuelle Browserbreite unterhalb des maxWidth-Wertes liegt.
      // Von allen in Frage kommenden Definitionen wird diejenige mit der kleinsten Differenz von
      // "maxWidth - <aktuelle Breite>" verwendet.
      // Anm.: Die Reihenfolge der Definitionen ist egal.
      //viewerConfigMobilUrl: [
      // {maxWidth: 700, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"},
      //{maxWidth: 500, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"}
      //],
      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'Mirador-3.0.0-rc.5': {
      enabled: false,
      type: ViewerType.MIRADOR,
      enabledForPage: [{view: Page.Search, order: 10}],
      viewerUrl: "/assets/mirador-3.0.0-rc.5/init.html",
      disabledPlugins: ["text-overlay"],
      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador.json",
      // Optional kann pro Breakpoint eine andere Konfiguration geladen werden.
      // Prinzipiell werden die Konfigurationen verwendet, wo die aktuelle Browserbreite unterhalb des maxWidth-Wertes liegt.
      // Von allen in Frage kommenden Definitionen wird diejenige mit der kleinsten Differenz von
      // "maxWidth - <aktuelle Breite>" verwendet.
      // Anm.: Die Reihenfolge der Definitionen ist egal.
      //viewerConfigMobilUrl: [
      // {maxWidth: 700, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"},
      //{maxWidth: 500, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"}
      //],
      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    // [ViewerType.UV]: { // not working if aot/prod is built
    'UV': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}, {view: Page.Detail, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/zas_en.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'UV-3.0.36': {
      enabled: false,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 25}, {view: Page.Detail, order: 25}], // default: on all views
      viewerUrl: "assets/uv-3.0.36/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/zas_en.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [

      ],
    },
    'UVScroll': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/scroll_view.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'UV-3.0.36-Scroll': {
      enabled: false,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 25}], // default: on all views
      viewerUrl: "assets/uv-3.0.36/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/scroll_view.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'MiradorScroll': {
      enabled: false,
      enabledForPage: [{view: Page.Search, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // mögliche Werte: "share", "download","image-tools","text-overlay"
      disabledPlugins: ["text-overlay"],

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_scroll.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'Mirador-3.0.0-rc.5-Scroll': {
      enabled: false,
      enabledForPage: [{view: Page.Search, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador-3.0.0-rc.5/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // mögliche Werte: "share", "download","image-tools","text-overlay"
      disabledPlugins: ["text-overlay"],

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_scroll.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'MiradorSinglePage': {
      enabled: true,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_single.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'Mirador-3.0.0-rc.5-SinglePage': {
      enabled: false,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador-3.0.0-rc.5/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_single.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    }
  },

  showSimpleSearch: true,
  viewerExtraWide: false,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "http://ub-newspapers.ub.unibas.ch/",

  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: true
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },


  // Dokument-Detail-Ansicht:
  // Mit 'bottom' oder 'top', wird in der Detail-Ansicht ein IIIF Viewer unterhalb oder oberhalb
  // der Felder eines Dokuments angezeigt. false schaltet die gleichzeitige Ansicht ab.
  narrowEmbeddedIiiFViewer: 'bottom',

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "select-sort-fields.score_desc"
    },
    {
      field: "date",
      order: SortOrder.ASC,
      display: "select-sort-fields.date_asc"
    },
    {
      field: "date",
      order: SortOrder.DESC,
      display: "select-sort-fields.date_desc"
    },
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "Quelldatum": {
      "field": "date",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "env.facetFields.Quelldatum",
      "operator": "AND",
      "showAggs": true,
      "order": 0,
      "size": 31,
      "expandAmount": 31,

    } as HistogramFieldModel,
    "newspaper": {
      "field": "newspaper.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "env.facetFields.Quellname",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 2,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true
    },
    "issue_type": {
      "field": "pages.issue_type.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Ausgabetyp",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 3,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "issue_number": {
      "field": "issue_number.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Ausgaben-Nummer",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 4,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "goobi_name": {
      "field": "goobi_name.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Goobi-Name",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 5,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "page": {
      "field": "page",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Seitenanzahl",
      "operator": "AND",
      "showAggs": true,
      "order": 11,
      "size": 100,
      "expandAmount": 10
    } as HistogramFieldModel,
  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {
    "Quelldatum": {
      "field": "Quelldatum.keyword",
      "facetType": FacetFieldType.RANGE,
      "label": "Quelldatum",
      "from": "1960-01-01",
      "to": "2200-01-01",
      "min": "1960-01-01",
      "max": "2200-01-01",
      "showMissingValues": true,
      "order": 1,
      "expandAmount": 5
    }
  },

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [5, 10, 20, 50],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 10,
    "offset": 0,
    "sortField": "_score",
    "sortOrder": SortOrder.DESC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [
    {
      "css": "col-sm-4 col-lg-4 text-start",
      "extraInfo": true,
      "field": "title",
      "label": "Dossiertitel",
      "landingpage": true,
      "sort": "title.keyword"
    },
    {
      "css": "col-sm-4 col-lg-4 text-start",
      "field": "Quellname",
      "label": "Zeitung",
      "sort": "Quellname.keyword"
    },
    {
      "css": "col-sm-4 col-lg-4 text-start",
      "field": "Quelldatum",
      "label": "Quelldatum",
      "sort": "Quelldatum.keyword"
    }
  ],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
    "descr_fuv": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_fuv",
      "label": "Firmen und Verb\u00e4nde"
    },
    "descr_person": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_person",
      "label": "Personen"
    },
    "descr_sach": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_sach",
      "label": "Sachdeskriptor"
    },
    "object_type": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "object_type",
      "label": "Objekttyp"
    }
  },

  searchResultSettings: {
    showHitNumber: false,
    showHitObjectType: true,
    paginationMethod: "bar",
    showKeepFiltersButton: false,
    pageButtonsInBar: 5, // go to last page and check if there is no overflow!
    // order of view style buttons (left top side on the search result page)
    viewStyleButtons: [
      // "PAGE_SIZE_SELECTOR",
      //"LIST_STYLE_BUTTON",
      //"IIIF_SELECTOR",
      //"IIIF_BUTTON_GROUP"
    ]
  },

  i18n: {
    "de": {
      "top.headerSettings.name": "AVIS-Blatt",
      "top.headerSettings.name.Dev": "AVIS-Blatt (Dev)",
      "top.headerSettings.name.Loc": "AVIS-Blatt (Loc)",
      "top.headerSettings.name.Test": "AVIS-Blatt(Test)",
    },
    "en": {
      "top.headerSettings.name": "AVIS-Blatt",
      "top.headerSettings.name.Dev": "AVIS-Blatt (Dev)",
      "top.headerSettings.name.Loc": "AVIS-Blatt (Loc)",
      "top.headerSettings.name.Test": "AVIS-Blatt (Test)",
    }
  },

};

initRdvLib(environment, [MiradorPlugins.PLUGIN_OCR_HELPER]);
