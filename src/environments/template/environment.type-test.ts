import {BaseSettings} from "@rdv-lib";
import {viewer} from '@env_temp/viewer';
import {richtextEditor} from "@env_temp/richtexteditor";

export const environment: BaseSettings = {
  ...viewer,
  ...richtextEditor,
  production: true,

  //Wo liegt Proxy-Skript, das mit Elasticsearch spricht?
  proxyUrl: "https://ub-rdv-test-proxy.ub.unibas.ch/v2/rdv_query/es_proxy/",
  moreProxyUrl: "https://ub-rdv-test-proxy.ub.unibas.ch/v2/rdv_query/further_snippets/",
  inFacetSearchProxyUrl: "https://ub-rdv-test-proxy.ub.unibas.ch/v2/rdv_query/facet_search/",
  popupQueryProxyUrl: "https://ub-rdv-test-proxy.ub.unibas.ch/v2/rdv_query/popup_query/",
  documentViewerProxyUrl: "https://ub-rdv-test-proxy.ub.unibas.ch/v2/rdv_query/iiif_flex_pres/",
  detailProxyUrl: "https://ub-rdv-test-proxy.ub.unibas.ch/v2/rdv_object/object_view/",
  navDetailProxyUrl: "https://ub-rdv-test-proxy.ub.unibas.ch/v2/rdv_query/next_objectview/",
  detailSuggestionProxyUrl: "https://ub-rdv-test-proxy.ub.unibas.ch/v2/rdv_query/form_query/",
  suggestSearchWordProxyUrl: "https://ub-rdv-test-proxy.ub.unibas.ch/v2/rdv_query/autocomplete/",
  detailEditProxyUrl: "https://ub-rdv-test-proxy.ub.unibas.ch/v2/rdv_object/object_edit/",
  detailNewProxyUrl: "https://ub-rdv-test-proxy.ub.unibas.ch/v2/rdv_object/object_new/"
};
