import {BaseSettings} from "@rdv-lib";
import {viewer} from '@env_temp/viewer';
import {richtextEditor} from "@env_temp/richtexteditor";

export const environment: BaseSettings = {
  ...viewer,
  ...richtextEditor,
  production: true,

  //Wo liegt Proxy-Skript, das mit Elasticsearch spricht?
  proxyUrl : "https://rdv-querybuilder-swa.ub-dd-prod.k8s-001.unibas.ch/v2/rdv_query/es_proxy/" ,
  moreProxyUrl: "https://rdv-querybuilder-swa.ub-dd-prod.k8s-001.unibas.ch/v2/rdv_query/further_snippets/",
  inFacetSearchProxyUrl: "https://rdv-querybuilder-swa.ub-dd-prod.k8s-001.unibas.ch/v2/rdv_query/facet_search/",
  detailProxyUrl: "https://rdv-querybuilder-swa.ub-dd-prod.k8s-001.unibas.ch/v2/rdv_object/object_view/",
  documentViewerProxyUrl: "https://rdv-querybuilder-swa.ub-dd-prod.k8s-001.unibas.ch/v2/rdv_query/iiif_flex_pres/",
  navDetailProxyUrl: "https://rdv-querybuilder-swa.ub-dd-prod.k8s-001.unibas.ch/v2/rdv_query/next_objectview/",
  popupQueryProxyUrl: "https://rdv-querybuilder-swa.ub-dd-prod.k8s-001.unibas.ch/v2/rdv_query/popup_query/",
  detailSuggestionProxyUrl: "https://rdv-querybuilder-swa.ub-dd-prod.k8s-001.unibas.ch/v2/rdv_query/form_query/",
  suggestSearchWordProxyUrl: "https://rdv-querybuilder-swa.ub-dd-prod.k8s-001.unibas.ch/v2/rdv_query/autocomplete/"
};
