import {viewer} from '@env_temp/viewer';
import {richtextEditor} from "@env_temp/richtexteditor";
import { BaseSettings } from '@rdv-lib';

export const environment: BaseSettings = {
  ...viewer,
  ...richtextEditor,
  production: false,
  //Wo liegt Proxy-Skript, das mit Elasticsearch spricht?
  proxyUrl: "https://ub-rdv-outermedia-proxy.ub.unibas.ch/v2/rdv_query/es_proxy/",
  moreProxyUrl: "https://ub-rdv-outermedia-proxy.ub.unibas.ch/v1/rdv_query/further_snippets/",
  inFacetSearchProxyUrl: "https://ub-rdv-outermedia-proxy.ub.unibas.ch/v1/rdv_query/facet_search/",
  popupQueryProxyUrl: "https://ub-rdv-outermedia-proxy.ub.unibas.ch/v1/rdv_query/popup_query/",
  documentViewerProxyUrl: "https://ub-rdv-outermedia-proxy.ub.unibas.ch/v1/rdv_query/iiif_flex_pres/",
  detailProxyUrl: "https://ub-rdv-outermedia-proxy.ub.unibas.ch/v1/rdv_object/object_view/",
  detailEditProxyUrl: "https://ub-rdv-outermedia-proxy.ub.unibas.ch/v1/rdv_object/object_edit/",
  navDetailProxyUrl: "https://ub-rdv-outermedia-proxy.ub.unibas.ch/v1/rdv_query/next_objectview/",
  detailSuggestionProxyUrl: "https://ub-rdv-outermedia-proxy.ub.unibas.ch/v1/rdv_query/form_query/",
  suggestSearchWordProxyUrl: "https://ub-rdv-outermedia-proxy.ub.unibas.ch/v1/rdv_query/autocomplete/",
  detailNewProxyUrl: "https://ub-rdv-outermedia-proxy.ub.unibas.ch/v1/rdv_object/object_new/"
};
