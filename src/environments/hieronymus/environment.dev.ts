const Proj = "hieronymus-dev";

import {FacetFieldType, FacetValueOrder, initRdvLib, SettingsModel} from '@rdv-lib'
import {environment as proj} from '@env/hieronymus/environment';
import {environment as dev} from '@env_temp/environment.type-dev';
import {addDevNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...dev,
  ...proj,
  headerSettings: addDevNamePostfix(proj.headerSettings),


  facetFields: {
    ...proj.facetFields,
    "obj_type": {
      "field": "type.keyword",
      "facetType": FacetFieldType.CHECKBOX,
      "label": "Kategorie",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "expandAmount": 10,
      "size": 100,
      "initiallyOpen": true,
      "valueOrder": FacetValueOrder.LABEL
    },

    "hierarchy_filter": {
      "field": "hierarchy_filter.keyword",
      "facetType": FacetFieldType.HIERARCHIC,
      "label": "Themenkapitel",
      "operator": "AND",
      "order": 10,
      "size": 100,
      "expandAmount": 100,
      "facetGroup": "Katalog Griechischer Geist",
      "initiallyOpen": true
    },
    "GG-ID": {
      "field": "gg_id.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Katalognummer",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 11,
      "size": 500,
      "expandAmount": 500,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT],
      "facetGroup": "Katalog Griechischer Geist"
    },

    "GG-Rel-ID": {
      "field": "related_gg.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "referenzierte Katalognummer",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 12,
      "size": 500,
      "expandAmount": 500,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT],
      "facetGroup": "Katalog Griechischer Geist"
    },
    "GG-Seiten": {
      "field": "img_type.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Abbildungen der Drucke",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 13,
      "size": 100,
      "expandAmount": 10,
      "facetGroup": "Katalog Griechischer Geist"
    },
    "digitalisiert": {
      "field": "digitalisiert.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Digitalisiert",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 64,
      "size": 100,
      "expandAmount": 15,
      "facetGroup": "Register Drucke"
    },
    "inst_digi": {
      "field": "inst_digi.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Digitalisiert von",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 64,
      "size": 100,
      "expandAmount": 100,
      "facetGroup": "Register Drucke"
    },
    "local_code": {
      "field": "local_code.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Statistik",
      "operator": "OR",
      "operators": [
        "AND", "OR", "NOT"
      ],
      "order": 65,
      "expandAmount": 10,
      "size": 100,
      "help": "env.facetFields.local_code",
      "facetGroup": "Indexsuche"
    }
  },

  proxyUrl : dev.proxyUrl +  Proj + "/",
  moreProxyUrl: dev.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: dev.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: dev.detailProxyUrl +  Proj + "/",
  documentViewerProxyUrl: dev.documentViewerProxyUrl +  Proj + "/",
  navDetailProxyUrl: dev.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: dev.popupQueryProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: dev.suggestSearchWordProxyUrl +  Proj + "/",
  detailSuggestionProxyUrl: dev.detailSuggestionProxyUrl +  Proj + "/",
};
initRdvLib(environment);
