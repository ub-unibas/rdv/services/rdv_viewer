/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {
  initRdvLib,
  MiradorPlugins,
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  FacetValueOrder,
  HistogramFieldModel,
  HistogramFieldType,
  Page,
  SettingsModel,
  SortOrder,
  ViewerType,
} from "@rdv-lib";

export const environment: SettingsModel = {
  extraCssClass: "env-autographen",
  // Defaultsprache für fehlende Übersetzungen in anderen Sprachen
  defaultLanguage: 'de',

  // optional pro Sprache environment-spezifische Übersetzungen
  // i18n: {
  //   "de": {
  //     "beta-header.contact": "XXX Kontaktieren Sie bei Fragen: ",
  //   },
  //   "en": {
  //     "beta-header.contact": "YYY Contact: ",
  //   }
  // },

  production: false,

  showSimpleSearch: true,
  // per Default werden Preview Images angezeigt
  // showImagePreview: false,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "https://ub-gg.ub.unibas.ch/",
  narrowEmbeddedIiiFViewer: 'bottom',
  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: false
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  // footerSettings: {
  //   "default": {
  //     // den ganzen Footer ausblenden
  //     disable: false,
  //     // den "Nach Oben"-Link anzeigen
  //     displayToTop: true,
  //     // Host des Projekts im Footer anzeigen ("Universität Basel")
  //     displayHostUrl: true,
  //     // i18n key der URL des Host-Links (bei displayHostUrl: true)
  //     hostUrl: "footer.footerSettings.host.url",
  //     // i18n key des Namens des Host-Links (bei displayHostUrl: true)
  //     hostName: "footer.footerSettings.host.name",
  //     // Portal-URL im Footer anzeigen
  //     displayPortalUrl: true
  //   },
  //   // sprachspezifische Einstellungen (ohne Übersetzungen)
  //   "de": {
  //   },
  //   "en": {
  //   }
  // },

  // sollten mehrere documentViewer enabled sein, wir dem User in der UI eine
  // Auswahlmöglichkeit gegeben
  documentViewer: {
    // [ViewerType.UV]: { // not working if aot/prod is built
    'UV': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 25}, {view: Page.Detail, order: 25}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/zas_en.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "obj_type",
          messageTitleKey: "search-results.view-mode.UVScroll.object_type.message.title",
          messageTextKey: "search-results.view-mode.UVScroll.object_type.message.text",
          values: [
            {label: "Illustration", value: {"id": "Illustration"}}
          ]
        }
      ],
    },
    'UVScroll': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/scroll_view.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "obj_type",
          messageTitleKey: "search-results.view-mode.UVScroll.object_type.message.title",
          messageTextKey: "search-results.view-mode.UVScroll.object_type.message.text",
          values: [
            {label: "Illustration", value: {"id": "Illustration"}}
          ]
        }
      ],
    },
    // [ViewerType.MIRADOR]: { // not working if aot/prod is built
    'Mirador': {
      enabled: true,
      type: ViewerType.MIRADOR,
      enabledForPage: [{view: Page.Search, order: 10}],
      viewerUrl: "/assets/mirador/init.html",
      disabledPlugins: ["text-overlay"],
      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador.json",
      // Optional kann pro Breakpoint eine andere Konfiguration geladen werden.
      // Prinzipiell werden die Konfigurationen verwendet, wo die aktuelle Browserbreite unterhalb des maxWidth-Wertes liegt.
      // Von allen in Frage kommenden Definitionen wird diejenige mit der kleinsten Differenz von
      // "maxWidth - <aktuelle Breite>" verwendet.
      // Anm.: Die Reihenfolge der Definitionen ist egal.
      //viewerConfigMobilUrl: [
      // {maxWidth: 700, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"},
      //{maxWidth: 500, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"}
      //],
      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "obj_type",
          messageTitleKey: "search-results.view-mode.UVScroll.object_type.message.title",
          messageTextKey: "search-results.view-mode.UVScroll.object_type.message.text",
          values: [
            {label: "Illustration", value: {"id": "Illustration"}}
          ]
        }
      ],
    },
    'MiradorScroll': {
      enabled: false,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // mögliche Werte: "share", "download","image-tools","text-overlay"
      disabledPlugins: ["text-overlay"],

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_scroll.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'MiradorSinglePage': {
      enabled: true,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_single.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    }
  },

  // UV/Mirador verwenden nur maximale Breite der app (false, undefined).
  // Wird diese Option auf true gesetzt, dann wird die volle Browser-Viewport-Breite verwendet.
  viewerExtraWide: false,

  // die konkreten Werte, werden noch gesetzt
  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,
  editable: false,

  externalAboutUrl: "https://ub.unibas.ch/de/bibliothekskataloge/",

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "select-sort-fields.score_obj"
    },
    {
      field: "year",
      order: SortOrder.ASC,
      display: "select-sort-fields.date_asc"
    },
    {
      field: "year",
      order: SortOrder.DESC,
      display: "select-sort-fields.date_desc"
    },
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {

    "author": {
      "field": "author.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Verfasser*in",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 40,
      "expandAmount": 500,
      "size": 500,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]

    },
    "title": {
      "field": "title_label.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Titel",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 41,
      "expandAmount": 500,
      "size": 500,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT

    },
    "year": {
      "field": "year",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Druckjahr",
      "operator": "AND",
      "showAggs": true,
      "order": 42,
      "size": 31,
      "expandAmount": 31

    } as HistogramFieldModel,

    "verlagsort": {
      "field": "751_a.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Druck-/Verlagsort",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 43,
      "expandAmount": 500,
      "size": 500,
      "searchWithin": true

    },

    "Sprachcode": {
      "field": "lang.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Sprache",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 45,
      "expandAmount": 50,
      "size": 100

    },
    "Drucker": {
      "field": "Drucker.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Druckerei / Verlag",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 46,
      "expandAmount": 1500,
      "size": 1500,
      "searchWithin": true,
      "autocomplete_size": 10,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]

    },
    "frueheigen": {
      "field": "frueheigen.id.keyword",
      "facetType": FacetFieldType.SIMPLE
,
      "label": "Vorbesitzer*in",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 47,
      "expandAmount": 500,
      "size": 500,
      "searchWithin": true,
      "autocomplete_size": 3,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "rel_persons": {
      "field": "rel_persons.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "weitere Personen / Körperschaften",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 52,
      "expandAmount": 1500,
      "size": 1500,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]

    },
    "Erstdrucke": {
      "field": "Erstdrucke.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Erstdrucke",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 110,
      "size": 100,
      "expandAmount": 5

    },
    "Widmungsexemplare": {
      "field": "Widmungsexemplare.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Widmungsexemplare",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 111,
      "size": 100,
      "expandAmount": 5

    },
    "Adressaten": {
      "field": "Adressaten.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Widmungsadressaten",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 112,
      "size": 500,
      "expandAmount": 500,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]

    },
  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {
    "Quelldatum": {
      "field": "Quelldatum.keyword",
      "facetType": FacetFieldType.RANGE,
      "label": "Quelldatum",
      "from": "1960-01-01",
      "to": "2200-01-01",
      "min": "1960-01-01",
      "max": "2200-01-01",
      "showMissingValues": true,
      "order": 1,
      "expandAmount": 5
    }
  },

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [10, 50, 100, 200],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 10,
    "offset": 0,
    "sortField": "_score",
    "sortOrder": SortOrder.DESC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_score",
      "sortOrder": SortOrder.DESC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [
    {
      "css": "col-sm-4 col-lg-4 text-start",
      "extraInfo": true,
      "field": "title",
      "label": "Dossiertitel",
      "landingpage": true,
      "sort": "title.keyword"
    },
    {
      "css": "col-sm-4 col-lg-4 text-start",
      "field": "Quellname",
      "label": "Zeitung",
      "sort": "Quellname.keyword"
    },
    {
      "css": "col-sm-4 col-lg-4 text-start",
      "field": "Quelldatum",
      "label": "Quelldatum",
      "sort": "Quelldatum.keyword"
    }
  ],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
    "descr_fuv": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_fuv",
      "label": "Firmen und Verb\u00e4nde"
    },
    "descr_person": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_person",
      "label": "Personen"
    },
    "descr_sach": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_sach",
      "label": "Sachdeskriptor"
    },
    "object_type": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "object_type",
      "label": "Relevanz"
    }
  },

  searchResultSettings: {
    showHitNumber: false,
    showHitObjectType: true,
    paginationMethod: "bar",
    showKeepFiltersButton: true,
    pageButtonsInBar: 5, // go to last page and check if there is no overflow!
    // order of view style buttons (left top side on the search result page)
    viewStyleButtons: [
      // "PAGE_SIZE_SELECTOR",
      //"IIIF_SELECTOR",
    ]
  },

  i18n: {
    "de": {
      "simple-search.title": "Stichwortsuche",
      "header.about": "Über den Griechischen Geist aus Basler Pressen",
      "top.headerSettings.name": "Griechischer Geist aus Basler Pressen",
      "top.headerSettings.name.Dev": "Griechischer Geist aus Basler Pressen (Dev)",
      "top.headerSettings.name.Loc": "Griechischer Geist aus Basler Pressen (Loc)",
      "top.headerSettings.name.Test": "Griechischer Geist aus Basler Pressen (Test)",
      "top.headerSettings.betaBarContact.name": "Christoph Schneider",
      "top.headerSettings.betaBarContact.email": "christoph.schneider@unibas.ch",
      "search-results.view-mode.UVScroll.object_type.message.title": "Hinweis",
      "search-results.view-mode.UVScroll.object_type.message.text": "In dieser Ansicht können nur die Illustrationen angzeigt werden.",
      "select-sort-fields.score_obj": "Relevanz (bei ausgeführter Stichwortsuche)",
      "date.label.decade": "{{startYear}} - {{endYear}}",
    }
  },

};

initRdvLib(environment, [MiradorPlugins.PLUGIN_OCR_HELPER]);
