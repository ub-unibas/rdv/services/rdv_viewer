const Proj = "hieronymus-loc";

import {initRdvLib, SettingsModel} from '@rdv-lib'
import {environment as proj} from "@env/hieronymus/environment";
import {environment as loc} from "@env_temp/environment.type-loc";
import {addLocNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...loc,
  ...proj,
  headerSettings: addLocNamePostfix(proj.headerSettings),

  proxyUrl : loc.proxyUrl +  Proj + "/",
  moreProxyUrl: loc.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: loc.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: loc.detailProxyUrl +  Proj + "/",
  documentViewerProxyUrl: loc.documentViewerProxyUrl +  Proj + "/",
  navDetailProxyUrl: loc.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: loc.popupQueryProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: loc.suggestSearchWordProxyUrl +  Proj + "/",
  detailSuggestionProxyUrl: loc.detailSuggestionProxyUrl +  Proj + "/",
  //detailEditProxyUrl: "https://ub-rdv-loc-proxy.ub.unibas.ch/v1/rdv_object/object_edit/" +  Proj + "/",
};

initRdvLib(environment);
