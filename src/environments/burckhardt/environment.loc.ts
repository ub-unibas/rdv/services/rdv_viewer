import {environment as prod} from "@env_temp/environment.type-prod";

const Proj = "burckhardt-loc";


import {environment as proj} from "@env/burckhardt/environment";
import {environment as loc} from "@env_temp/environment.type-loc";
import {addLocNamePostfix} from "@env_temp/util";
import {initRdvLib, SettingsModel} from "@rdv-lib";

export const environment: SettingsModel = {
  ...loc,
  ...proj,

  headerSettings: addLocNamePostfix(proj.headerSettings),

  proxyUrl : loc.proxyUrl +  Proj + "/",
  moreProxyUrl: loc.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: loc.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: loc.detailProxyUrl +  Proj + "/",
  documentViewerProxyUrl: loc.documentViewerProxyUrl +  Proj + "/",
  navDetailProxyUrl: loc.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: loc.popupQueryProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: loc.suggestSearchWordProxyUrl +  Proj + "/",
};
initRdvLib(environment);
