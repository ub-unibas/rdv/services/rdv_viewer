
const Proj = "tinti-dev";

import {initRdvLib, SettingsModel} from 'rdv-lib'
import {environment as proj} from '@env/tinti/environment';
import {environment as dev} from '@env_temp/environment.type-dev';
import {addDevNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...dev,
  ...proj,

  headerSettings: addDevNamePostfix(proj.headerSettings),

  proxyUrl : dev.proxyUrl +  Proj + "/",
  moreProxyUrl: dev.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: dev.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: dev.detailProxyUrl +  Proj + "/",
  documentViewerProxyUrl: dev.documentViewerProxyUrl +  Proj + "/",
  navDetailProxyUrl: dev.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: dev.popupQueryProxyUrl +  Proj + "/",
  detailSuggestionProxyUrl: dev.detailSuggestionProxyUrl +  Proj + "/",
  detailEditProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_object/object_edit/" +  Proj + "/",
};
initRdvLib(environment);
