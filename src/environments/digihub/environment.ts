/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  FacetValueOrder,
  HistogramFieldModel,
  HistogramFieldType,
  initRdvLib,
  MiradorPlugins,
  Page,
  SettingsModel,
  SortOrder,
  ViewerType,
} from "@rdv-lib";

export const environment: SettingsModel = {
  documentViewer: {
    // [ViewerType.MIRADOR]: { // not working if aot/prod is built
    'Mirador': {
      enabled: true,
      type: ViewerType.MIRADOR,
      enabledForPage: [{view: Page.Search, order: 10}],
      viewerUrl: "https://ub-mirador.ub.unibas.ch/",
      disabledPlugins: [],
      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      // viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_pres.json",
      // Optional kann pro Breakpoint eine andere Konfiguration geladen werden.
      // Prinzipiell werden die Konfigurationen verwendet, wo die aktuelle Browserbreite unterhalb des maxWidth-Wertes liegt.
      // Von allen in Frage kommenden Definitionen wird diejenige mit der kleinsten Differenz von
      // "maxWidth - <aktuelle Breite>" verwendet.
      // Anm.: Die Reihenfolge der Definitionen ist egal.
      //viewerConfigMobilUrl: [
      // {maxWidth: 700, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"},
      //{maxWidth: 500, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"}
      //],
      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'MiradorSinglePage': {
      enabled: true,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "https://ub-mirador.ub.unibas.ch/",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_single.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [],
    }
    },

  production: false,
  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "http://ub-digibas.ub.unibas.ch/",

  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,

  // Dokument-Detail-Ansicht:
  // Mit 'bottom' oder 'top', wird in der Detail-Ansicht ein IIIF Viewer unterhalb oder oberhalb
  // der Felder eines Dokuments angezeigt. false schaltet die gleichzeitige Ansicht ab.
  narrowEmbeddedIiiFViewer: 'bottom',

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: false
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },


  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "all_text": "Freitext",
      "fct_location.search": "Geographikum",
      "fct_mediatype.search": "Datentr\u00e4ger/Inhaltstyp",
      "fct_person_organisation.search": "Person/Institution",
      "fct_topic.search": "Schlagwort",
      "field_title": "Titel/Benennung"
    },
    "preselect": [
      "all_text"
    ]
  },

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {
  },

//Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
//order gilt fuer Facetten und Ranges
  facetFields: {
    "dataset": {
      "field": "dataset.keyword",
      "facetType": FacetFieldType.CHECKBOX,
      "label": "Dataset",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "valueOrder": FacetValueOrder.LABEL,
      "order": 0,
      "expandAmount": 10,
      "size": 100,
      "autocomplete_size": 3
    },
    "Tag": {
      "field": "tag.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Tag",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "facetGroup": "Metadaten"
    },
    "GenreForm": {
      "field": "genreform.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Genre/Form",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "facetGroup": "Metadaten"
    },
    "Erscheinungsjahr": {
      "field": "year",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Erscheinungsjahr",
      "operator": "AND",
      "showAggs": true,
      "order": 2,
      "size": 31,
      "expandAmount": 31,
      "facetGroup": "Metadaten"
    } as HistogramFieldModel,

    "hierarchy_filter": {
      "field": "hierarchy_filter_han2.keyword",
      "facetType": FacetFieldType.HIERARCHIC,
      "label": "Tektonik",
      "operator": "AND",
      "order": 4,
      "size": 100,
      "expandAmount": 100
    },
    "Ebene": {
      "field": "level.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Ebene",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 10,
      "size": 100,
      "expandAmount": 10,
      "facetGroup": "Metadaten"
    },
    "author": {
      "field": "author.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Autor",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 20,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "autocomplete_size": 3,
      "facetGroup": "Metadaten"
    },
    "Sprache": {
      "field": "lang.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Sprache",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 22,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true,
      "facetGroup": "Metadaten"
    },
    "bsiz_id": {
      "field": "bsiz_id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "IZID",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 90,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "autocomplete_size": 3,
      "facetGroup": "IDs"
    },
    "old_sysid": {
      "field": "old_sysid.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "alte Systemnummer",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 91,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "autocomplete_size": 3,
      "facetGroup": "IDs"
    },
    "signatur": {
      "field": "signatur.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Signatur",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 90,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true,
      "facetGroup": "IDs"
    },
    "oai_sets": {
      "field": "oai_sets.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "OAI Set",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "valueOrder": FacetValueOrder.LABEL,
      "order": 95,
      "expandAmount": 20,
      "size": 100,
      "autocomplete_size": 3,
      "facetGroup": "OAI"
    },
    "oai_datestamp": {
      "field": "oai_datestamp",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "OAI Zeitstempel",
      "operator": "AND",
      "showAggs": true,
      "order": 96,
      "size": 31,
      "expandAmount": 31,
      "help": "env.facetFields.oai_datestamp.help",
      "facetGroup": "OAI"
    } as HistogramFieldModel,

    "vlm_id": {
      "field": "vlm_id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "ID",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "valueOrder": FacetValueOrder.COUNT,
      "order": 1000,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "facetGroup": "VLM"
    },
    "vlm_status": {
      "field": "vlm_status",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Status",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "valueOrder": FacetValueOrder.LABEL,
      "order": 1000,
      "expandAmount": 10,
      "size": 100,
      "facetGroup": "VLM"
    },

    "vlm_set": {
      "field": "vlm_set.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Set",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "valueOrder": FacetValueOrder.LABEL,
      "order": 1000,
      "expandAmount": 10,
      "size": 100,

      "facetGroup": "VLM"
    },

    "goobi_id": {
      "field": "goobi_id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "ID",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "valueOrder": FacetValueOrder.COUNT,
      "order": 1000,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "facetGroup": "Goobi"
    },
    "goobi_place": {
      "field": "goobi_place.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Titel",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "valueOrder": FacetValueOrder.COUNT,
      "order": 1000,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "facetGroup": "Goobi"
    },
    "goobi_vorgaenge": {
      "field": "goobi_vorgaenge",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Vorgänge",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "valueOrder": FacetValueOrder.LABEL,
      "order": 1000,
      "expandAmount": 10,
      "size": 100,

      "facetGroup": "Goobi"
    },
    "goobi_project": {
      "field": "goobi_project.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Projekt",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "valueOrder": FacetValueOrder.LABEL,
      "order": 1000,
      "expandAmount": 10,
      "size": 100,

      "facetGroup": "Goobi"
    },
    "goobi_parent": {
      "field": "goobi_parent.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Goobi Split-Titel",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "valueOrder": FacetValueOrder.COUNT,
      "order": 1000,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "facetGroup": "Goobi"
    },
    "goobi_schritt": {
      "field": "Bearbeitungsschritt.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Bearbeitungsschritt",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT],

      "order": 1000,
      "expandAmount": 10,
      "size": 100,

      "facetGroup": "Goobi"
    },
    "goobi_status": {
      "field": "Bearbeitungsstatus",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Bearbeitungsstatus",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "valueOrder": FacetValueOrder.LABEL,
      "order": 1000,
      "expandAmount": 10,
      "size": 100,

      "facetGroup": "Goobi"
    },
    "embargo_digispace": {
      "field": "digispace_embargo.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Embargo",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "valueOrder": FacetValueOrder.LABEL,
      "order": 1000,
      "expandAmount": 10,
      "size": 100,

      "facetGroup": "Digispace"
    },
    "project_digispace": {
      "field": "digispace_project.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Projekt",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "valueOrder": FacetValueOrder.LABEL,
      "order": 1000,
      "expandAmount": 10,
      "size": 100,

      "facetGroup": "Digispace"
    },
    "subprojects_digispace": {
      "field": "digispace_subprojects.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "SubProjekt",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "valueOrder": FacetValueOrder.LABEL,
      "order": 1000,
      "expandAmount": 10,
      "size": 100,

      "facetGroup": "Digispace"
    },
    "error": {
      "field": "digispace_errors.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Fehler",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "valueOrder": FacetValueOrder.LABEL,
      "order": 1000,
      "expandAmount": 10,
      "size": 100,

      "facetGroup": "Digispace"
    },
  },

// @Depreacted: only used in "old" search form, for new search add to "facetFields"
//Infos zu Ranges (z.B. Label)
//order gilt fuer Facetten und Ranges
  rangeFields: {
  },

//Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [5, 10, 20, 50],

  queryParams: {
    "rows": 10,
    "offset": 0,
    "sortField": "_score",
    "sortOrder": SortOrder.DESC
  },

  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "select-sort-fields.score_desc"
    },
    {
      field: "fct_date",
      order: SortOrder.ASC,
      display: "select-sort-fields.date_asc"
    },
    {
      field: "fct_date",
      order: SortOrder.DESC,
      display: "select-sort-fields.date_desc"
    },
  ],

//Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_score",
      "sortOrder": SortOrder.DESC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

//Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [
  ],

//Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
    "fct_location": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "fct_location",
      "label": "Geographikum"
    },
    "fct_mediatype": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "fct_mediatype",
      "label": "Datentr\u00e4ger/Inhaltstyp"
    },
    "fct_topic": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "fct_topic",
      "label": "Schlagwort"
    },
    "field_date_input_values": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "field_date_input_values",
      "label": "Zeit (Ausgangswert)"
    }
  },

  searchResultSettings: {
    showHitNumber: false,
    showHitObjectType: true,
    paginationMethod: "bar",
    showKeepFiltersButton: false,
    pageButtonsInBar: 5, // go to last page and check if there is no overflow!
    // order of view style buttons (left top side on the search result page)
    viewStyleButtons: [
      // "PAGE_SIZE_SELECTOR",
      //"LIST_STYLE_BUTTON",
      //"IIIF_SELECTOR",
      //"IIIF_BUTTON_GROUP"
    ]
  },

  i18n: {
    "de": {
      "top.headerSettings.name": "DigiBasel",
      "top.headerSettings.name.Dev": "DigiBasel (Dev)",
      "top.headerSettings.name.Loc": "DigiBasel (Loc)",
      "top.headerSettings.name.Test": "DigiBasel (Test)",
    },
    "en": {
      "top.headerSettings.name": "DigiBasel",
      "top.headerSettings.name.Dev": "DigiBasel (Dev)",
      "top.headerSettings.name.Loc": "DigiBasel (Loc)",
      "top.headerSettings.name.Test": "DigiBasel (Test)",
    }
  },
};

initRdvLib(environment, [MiradorPlugins.PLUGIN_OCR_HELPER]);
