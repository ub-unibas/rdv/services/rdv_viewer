
const Proj = "swa-prod";
import {initRdvLib, SettingsModel} from '@rdv-lib';
import {environment as proj} from '@env/swa/environment';
import {environment as prod} from '@env_temp/environment.type-prod';

export const environment: SettingsModel = {

  ...prod,
  ...proj,

  documentViewerProxyUrl: undefined,

  proxyUrl : "https://ub-swapdf-proxy.ub.unibas.ch/v2/rdv_query/es_proxy/" +  Proj + "/",
  moreProxyUrl: "https://ub-swapdf-proxy.ub.unibas.ch/v2/rdv_query/further_snippets/" +  Proj + "/",
  inFacetSearchProxyUrl: "https://ub-swapdf-proxy.ub.unibas.ch/v2/rdv_query/facet_search/" +  Proj + "/",
  detailProxyUrl: "https://ub-swapdf-proxy.ub.unibas.ch/v2/rdv_object/object_view/" +  Proj + "/",
  navDetailProxyUrl: "https://ub-swapdf-proxy.ub.unibas.ch/v2/rdv_query/next_objectview/" +  Proj + "/",
  popupQueryProxyUrl: "https://ub-swapdf-proxy.ub.unibas.ch/v2/rdv_query/popup_query/" +  Proj + "/",
  detailSuggestionProxyUrl: "https://ub-swapdf-proxy.ub.unibas.ch/v2/rdv_query/form_query/" +  Proj + "/",
  suggestSearchWordProxyUrl: "https://ub-swapdf-proxy.ub.unibas.ch/v2/rdv_query/autocomplete/" +  Proj + "/",

};
initRdvLib(environment);
