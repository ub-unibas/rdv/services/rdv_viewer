const Proj = "swa-dev";
import {FacetFieldType, FacetValueOrder, HistogramFieldModel, HistogramFieldType, initRdvLib, SettingsModel} from '@rdv-lib';
import {environment as proj} from '@env/swa/environment';
import {environment as dev} from '@env_temp/environment.type-dev';
import {addDevNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {

  ...dev,
  ...proj,

  headerSettings: addDevNamePostfix(proj.headerSettings),


  proxyUrl : "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_query/es_proxy/" +  Proj + "/",
  moreProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_query/further_snippets/" +  Proj + "/",
  inFacetSearchProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_query/facet_search/" +  Proj + "/",
  detailProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_object/object_view/" +  Proj + "/",
  navDetailProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_query/next_objectview/" +  Proj + "/",
  popupQueryProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_query/popup_query/" +  Proj + "/",
  detailSuggestionProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_query/form_query/" +  Proj + "/",
  suggestSearchWordProxyUrl: "https://ub-swapdftest-proxy.ub.unibas.ch/v2/rdv_query/autocomplete/" +  Proj + "/",


  facetFields: {
    ...proj.facetFields,
    "oai_datestamp": {
      "field": "oai_datestamp",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "OAI Zeitstempel",
      "operator": "AND",
      "showAggs": true,
      "order": 1062,
      "size": 31,
      "expandAmount": 31,
    } as HistogramFieldModel,
    "title": {
      "field": "title.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "title",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 101,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT],
      "autocomplete_size": 3
    }
  }
};
initRdvLib(environment);
