const Proj = "swa-dev_k8s";

import {initRdvLib, SettingsModel} from '@rdv-lib';
import {environment as proj} from '@env/swa/environment';
import {environment as dev_k8s} from "@env_temp/environment.type-dev_k8s_dd";
import {addTestNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {

  ...dev_k8s,
  ...proj,
  headerSettings: addTestNamePostfix(proj.headerSettings),

  proxyUrl : dev_k8s.proxyUrl +  Proj + "/",
  moreProxyUrl: dev_k8s.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: dev_k8s.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: dev_k8s.detailProxyUrl +  Proj + "/",
  documentViewerProxyUrl: dev_k8s.documentViewerProxyUrl +  Proj + "/",
  navDetailProxyUrl: dev_k8s.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: dev_k8s.popupQueryProxyUrl +  Proj + "/",
  detailSuggestionProxyUrl: dev_k8s.detailSuggestionProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: undefined,
};

initRdvLib(environment);
