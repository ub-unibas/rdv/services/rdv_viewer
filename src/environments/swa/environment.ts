import {
  Backend,
  FacetFieldType,
  HistogramFieldModel,
  HistogramFieldType,
  initRdvLib,
  SettingsModel,
  SortOrder,
  ViewerType,
  Page,
  FacetValueOrder,
  MiradorPlugins
} from "@rdv-lib";

export const environment: SettingsModel = {
  production: false,

  showSimpleSearch: true,
  showImagePreview: true,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "http://ub-swasearch.ub.unibas.ch/",

  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,

  documentViewer: {
    // [ViewerType.MIRADOR]: { // not working if aot/prod is built
    'Mirador': {
      enabled: true,
      type: ViewerType.MIRADOR,
      enabledForPage: [{view: Page.Search, order: 10}],
      viewerUrl: "/assets/mirador/init.html",
      disabledPlugins: ["text-overlay"],
      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador.json",
      // Optional kann pro Breakpoint eine andere Konfiguration geladen werden.
      // Prinzipiell werden die Konfigurationen verwendet, wo die aktuelle Browserbreite unterhalb des maxWidth-Wertes liegt.
      // Von allen in Frage kommenden Definitionen wird diejenige mit der kleinsten Differenz von
      // "maxWidth - <aktuelle Breite>" verwendet.
      // Anm.: Die Reihenfolge der Definitionen ist egal.
      //viewerConfigMobilUrl: [
      // {maxWidth: 700, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"},
      //{maxWidth: 500, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"}
      //],
      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "digitalisiert",
          messageTitleKey: "search-results.view-mode.UV.object_type.message.title",
          messageTextKey: "Nur digitalisierte Objekte können in dieser Ansicht angezeigt werden.",
          values: [
            {label: "einzelseiten", value: {"id": "einzelseiten"}},
          ]
        }
      ],
    },
    'UV': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}, {view: Page.Detail, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/zas_en.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "digitalisiert",
          messageTitleKey: "search-results.view-mode.UV.object_type.message.title",
          messageTextKey: "Nur digitalisierte Objekte können in dieser Ansicht angezeigt werden.",
          values: [
            {label: "einzelseiten", value: {"id": "einzelseiten"}},
          ]
        }
      ],
    },
    'UVScroll': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/scroll_view.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "digitalisiert",
          messageTitleKey: "search-results.view-mode.UV.object_type.message.title",
          messageTextKey: "Nur digitalisierte Objekte können in dieser Ansicht angezeigt werden.",
          values: [
            {label: "einzelseiten", value: {"id": "einzelseiten"}},
          ]
        }
      ],
    },
    'MiradorSinglePage': {
      enabled: true,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_single.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "digitalisiert",
          messageTitleKey: "search-results.view-mode.UV.object_type.message.title",
          messageTextKey: "Nur digitalisierte Objekte können in dieser Ansicht angezeigt werden.",
          values: [
            {label: "einzelseiten", value: {"id": "einzelseiten"}},
          ]
        }
      ],
    },
  },
  narrowEmbeddedIiiFViewer: 'bottom',
  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: true,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: false
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  externalAboutUrl: {
    "de": "https://ub.unibas.ch/de/historische-bestaende/wirtschaftsdokumentation/",
    "en": "https://ub.unibas.ch/en/collections/historical-holdings/economic-documentation/"
  },
  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "select-sort-fields.score_desc"
    },
    {
      field: "year",
      order: SortOrder.ASC,
      display: "select-sort-fields.date_asc"
    },
    {
      field: "year",
      order: SortOrder.DESC,
      display: "select-sort-fields.date_desc"
    }
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "year": {
      "field": "year",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "env.facetFields.fct_pubyear",
      "operator": "AND",
      "showAggs": true,
      "order": 99,
      "size": 31,
      "expandAmount": 31,
    } as HistogramFieldModel,
    "swa_objecttype_id": {
      "field": "swa_objecttype_id.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "env.facetFields.fct_objecttype",
      "operator": "OR",
      "valueOrder": FacetValueOrder.LABEL,
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 100,
      "size": 100,
      "expandAmount": 20,
      "translatesValues": true
    },
    "descr_fuv": {
      "field": "descr_fuv.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "env.facetFields.descr_fuv",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 101,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT]
    },
    "descr_person": {
      "field": "descr_person.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "env.facetFields.descr_person",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 102,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT]
    },
    "stw_ids": {
      "field": "stw_ids.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "env.facetFields.descr_sach_prefLabel",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 103,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT]
    },


    "hierarchy_filter": {
      "field": "hierarchy_filter.keyword",
      "facetType": FacetFieldType.HIERARCHIC,
      "label": "env.facetFields.descr_sach_hierarchy_filter",
      "operator": "AND",
      "order": 104,
      "size": 100,
      "expandAmount": 100
    },
    "descr_place": {
      "field": "descr_place.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "env.facetFields.fct_place",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 105,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "help": "env.facetFields.fct_place.help",
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT]
    },
    "swa_access_id": {
      "field": "swa_access_id.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "env.facetFields.fct_access_id",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 107,
      "size": 100,
      "expandAmount": 10,
      "translatesValues": true
    },
    "Signatur": {
      "field": "signatur.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "env.facetFields.Signatur",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 108,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT]
    },
    "authors": {
      "field": "author.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "env.facetFields.fct_author",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 108,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT]
    },
    "swa_local": {
      "field": "swa_local.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "SWA Localcode",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 104,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "hidden": true
    },
    "swa_digi": {
      "field": "swa_digi.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "SWA Digital",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 105,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "hidden": true
    },
    "genreform": {
      "field": "genreform.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "655a",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 105,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "hidden": true
    },
    /*"hierarchy_filter_han": {
      "field": "hierarchy_filter_han2.keyword",
      "facetType": FacetFieldType.HIERARCHIC,
      "label": "env.facetFields.descr_han_hierarchy_han_filter",
      "operator": "AND",
      "order": 1060,
      "size": 100,
      "expandAmount": 100,
      "help": "env.facetFields.hierarchy_filter.help",
    },*/
    "Erschliessungsebene": {
      "field": "level.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "env.facetFields.level",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1061,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "help": "env.facetFields.hierarchy_filter.help",
    },
    "LokalerCode": {
      "field": "990f.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "LokalerCode",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 1061,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    }
  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {},

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [10, 20, 50, 100],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 20,
    "offset": 0,
    "sortField": "_score",
    "sortOrder": SortOrder.DESC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {},

  searchResultSettings: {
    showHitNumber: true,
    showHitObjectType: true,
    paginationMethod: "bar",
    showKeepFiltersButton: true,
    pageButtonsInBar: 5, // go to last page and check if there is no overflow!
    // order of view style buttons (left top side on the search result page)
    viewStyleButtons: [
      "PAGE_SIZE_SELECTOR",
      //"LIST_STYLE_BUTTON",
      //"IIIF_SELECTOR",
      //"IIIF_BUTTON_GROUP"
    ]
  },

  i18n: {
    "de": {
      "env.facetFields.fct_pubyear": "Erscheinungsjahr",
      "env.facetFields.Signatur": "Signatur",
      "env.facetFields.fct_access_id": "Zugänglichkeit",
      "env.facetFields.level": "Verzeichnungsstufe",
      "env.facetFields.fct_objecttype": "Objekttyp",
      "env.facetFields.fct_author": "Verfasser/in",
      "env.facetFields.descr_han_hierarchy_han_filter": "Hierarchie / Kontext",
      "env.facetFields.descr_han_hierarchy_per_filter": "Bestandsgliederung Nachlässe",
      "env.facetFields.descr_han_hierarchy_verband_filter": "Bestandsgliederung Verbände",
      "top.headerSettings.name": "SWA Search",
      "top.headerSettings.name.Dev": "SWA Search (Dev)",
      "top.headerSettings.name.Loc": "SWA Search (Loc)",
      "top.headerSettings.name.Test": "SWA Search (Test)",
      "top.headerSettings.betaBarContact.name": "UB Wirtschaft - SWA",
      "top.headerSettings.betaBarContact.email": "info-ubw-swa@unibas.ch",
      "env.facetFields.hierarchy_filter.help": "Hierarchisch strukturierte Archivbestände (Wirtschaftsarchive/Privatarchive)"
    },
    "en": {
      "env.facetFields.fct_pubyear": "Year of publication",
      "env.facetFields.Signatur": "Callnumber",
      "env.facetFields.fct_access": "Accessibility",
      "env.facetFields.level": "Level",
      "env.facetFields.fct_objecttype": "Object type",
      "env.facetFields.fct_author": "Author",
      "env.facetFields.descr_han_hierarchy_han_filter": "Hierarchy / Context",
      "top.headerSettings.name": "SWA Search",
      "top.headerSettings.name.Dev": "SWA Search (Dev)",
      "top.headerSettings.name.Loc": "SWA Search (Loc)",
      "top.headerSettings.name.Test": "SWA Search (Test)",
      "top.headerSettings.betaBarContact.name": "UB Wirtschaft - SWA",
      "top.headerSettings.betaBarContact.email": "info-ubw-swa@unibas.ch",
      "env.facetFields.hierarchy_filter.help": "Hierarchically structured archival holdings (economic archives/private archives)",

      "env.facetFields.label.swa_objecttype_id.Archivmaterial": "Archive material",
      "env.facetFields.label.swa_objecttype_id.Archivmaterial: Firmenarchiv": "Archive material from companies and organizations",
      "env.facetFields.label.swa_objecttype_id.Archivmaterial: jeder Bestand": "Archive material: all fonds",
      "env.facetFields.label.swa_objecttype_id.Archivmaterial: Personennachlass": "Archive material from people",
      "env.facetFields.label.swa_objecttype_id.Archivmaterial: Verbandsarchiv": "Archive material from associations",
      "env.facetFields.label.swa_objecttype_id.Dokumentensammlung Zeitungsausschnitte elektronisch": "Document collections: electronic newspaper cuttings",
      "env.facetFields.label.swa_objecttype_id.Dokumentensammlungen Firmen und Organisationen": "Document collections: companies and organizations",
      "env.facetFields.label.swa_objecttype_id.Dokumentensammlungen in Schachteln": "Document collections in boxes",
      "env.facetFields.label.swa_objecttype_id.Dokumentensammlungen Personen": "Document collections: people",
      "env.facetFields.label.swa_objecttype_id.Dokumentensammlungen Themen": "Document collections: subjects",
      "env.facetFields.label.swa_objecttype_id.E-Geschäftsberichte": "Annual reports (electronic)",
      "env.facetFields.label.swa_objecttype_id.E-Monografien": "Grey literature (electronic)",
      "env.facetFields.label.swa_objecttype_id.E-Zeitschriften": "E-Journals",
      "env.facetFields.label.swa_objecttype_id.Kleinschriften": "Grey literature",
      "env.facetFields.label.swa_objecttype_id.Zeitschriften / Reihen / Geschäftsberichte": "Journals / Series / Annual reports",

      "env.facetFields.label.swa_access_id.Online frei zugänglich": "Online: free access",
      "env.facetFields.label.swa_access_id.Print: bestell- und ausleihbar": "Print for loan",
      "env.facetFields.label.swa_access_id.Print: Zugang im Sonderlesesaal": "Print: access in special reading room",
      "env.facetFields.label.swa_access_id.Online: zugänglich mit Login": "Online: access with login",
      "env.facetFields.label.swa_access_id.online frei zugänglich - retrodigitalisiert in e-rara/e-manuscripta": "Online: free access - digitised in e-rara/e-manuscripta",

      "env.facetFields.label.fct_level.Dossier / Teildossier / Dokument": "File / Sub-file / Document",
      "env.facetFields.label.fct_level.Serie / Teilserie": "Serie / Sub-serie",
      "env.facetFields.label.fct_level.Bestand / Teilbestand": "Fonds / Sub-fonds",
    }
  },

};

initRdvLib(environment, [MiradorPlugins.PLUGIN_OCR_HELPER]);
