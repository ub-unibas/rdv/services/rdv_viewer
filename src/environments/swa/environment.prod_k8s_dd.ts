const Proj = "swa-prod_k8s";

import {initRdvLib, SettingsModel} from '@rdv-lib';
import {environment as proj} from '@env/swa/environment';
import {environment as prod_k8} from "@env_temp/environment.type-prod_k8s_dd";
import {addTestNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {

  ...prod_k8,
  ...proj,
  headerSettings: addTestNamePostfix(proj.headerSettings),

  proxyUrl : prod_k8.proxyUrl +  Proj + "/",
  moreProxyUrl: prod_k8.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: prod_k8.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: prod_k8.detailProxyUrl +  Proj + "/",
  documentViewerProxyUrl: prod_k8.documentViewerProxyUrl +  Proj + "/",
  navDetailProxyUrl: prod_k8.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: prod_k8.popupQueryProxyUrl +  Proj + "/",
  detailSuggestionProxyUrl: prod_k8.detailSuggestionProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: undefined,
};

initRdvLib(environment);
