/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";
import { AbstractAppComponent } from "@rdv-lib";
import { environment } from "../../environments/environment";

/**
 * Main entry point to the application.
 */
@Component({
  selector: "app-root",
  template: `
    <app-beta-header
      *ngIf="(headerSettings$ | async)?.showBetaBar"
      contactName="top.headerSettings.betaBarContact.name"
      contactEmail="top.headerSettings.betaBarContact.email"
    ></app-beta-header>
    <app-header></app-header>
    <router-outlet class="g-0"></router-outlet>
    <app-footer></app-footer>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent extends AbstractAppComponent implements OnInit {
  ngOnInit(): void {
    if (environment.extraCssClass) {
      document.body.classList.add(environment.extraCssClass);
    }
  }
}
