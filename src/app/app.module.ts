/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import { Location, registerLocaleData } from "@angular/common";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import {
  Route,
  RouterModule,
  Routes,
  UrlMatchResult,
  UrlSegment,
  UrlSegmentGroup,
  UrlSerializer,
} from "@angular/router";

import localeDe from "@angular/common/locales/de";
import localeEn from "@angular/common/locales/en";
import localeDeExtra from "@angular/common/locales/extra/de";
import localeEnExtra from "@angular/common/locales/extra/en";
import localeFrExtra from "@angular/common/locales/extra/fr";
import localeItExtra from "@angular/common/locales/extra/it";
import localeFr from "@angular/common/locales/fr";
import localeIt from "@angular/common/locales/it";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { environment } from "@env/environment";
import {
  LocalizeParser,
  LocalizeRouterModule,
  LocalizeRouterService,
  LocalizeRouterSettings,
  ManualParserLoader,
} from "@gilsdav/ngx-translate-router";
import { NgSelectModule } from "@ng-select/ng-select";
import { EffectsModule } from "@ngrx/effects";
import { StoreModule } from "@ngrx/store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import {
  TranslateCompiler,
  TranslateLoader,
  TranslateModule,
  TranslateService,
} from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import {
  AboutComponent,
  CoreModule,
  DetailedComponent,
  EnvTranslateCompiler,
  LOCALIZED_ROUTER_PREFIX,
  NewSearchComponent,
  PopupLandingComponent,
  PreviousRouteServiceProvider,
  RemoteFilterConfigsEffects,
  SearchComponent,
  SearchModule,
  URLSearchParamsUrlSerializer,
  backendSearchServiceProvider,
  coreReducers,
  headerSettingsModelByLanguage,
  metaReducers,
  MapComponent,
  MapModule,
} from "@rdv-lib";
import { TinyMceModule, TinymceComponent } from "@tinymce";
import { ToastrModule } from "ngx-toastr";
import { AppComponent } from "./components/app.component";

registerLocaleData(localeFr, "fr-FR", localeFrExtra);
registerLocaleData(localeDe, "de-DE", localeDeExtra);
registerLocaleData(localeEn, "en-GB", localeEnExtra);
registerLocaleData(localeIt, "it-IT", localeItExtra);

// needs custom url matcher, to catch not localized urls e.g. "/popup?..."
export function popupUrlMatcher(
  segments: UrlSegment[],
  group: UrlSegmentGroup,
  route: Route
): UrlMatchResult {
  let res: UrlMatchResult = null;
  if (segments && segments.length === 1 && segments[0].path === "popup") {
    res = { consumed: segments };
  }
  return res;
}

const HTML_EDITOR = TinymceComponent;
// const HTML_EDITOR = SimpleHtmlEditorComponent;

export const routes: Routes = [
  { path: "about", component: AboutComponent },
  {
    path: "detail/:id",
    component: DetailedComponent,
    data: {
      htmlEditor: HTML_EDITOR,
      editorSettings: environment.richtextEditor,
    },
  },
  {
    path: "newdoc/:type",
    component: DetailedComponent,
    data: {
      htmlEditor: HTML_EDITOR,
      editorSettings: environment.richtextEditor,
    },
  },
  {
    path: "map",
    component: MapComponent,
  },
  { path: "", pathMatch: "full", component: NewSearchComponent },
  { path: "old-search", component: SearchComponent },
  { path: "popup", component: PopupLandingComponent },
  {
    matcher: popupUrlMatcher,
    component: PopupLandingComponent,
    data: { skipRouteLocalization: true },
  },
  { path: "**", component: NewSearchComponent },
];

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
const localRoutes = ["de"];
const getLocale = (language: string) =>
  headerSettingsModelByLanguage(language, environment.headerSettings);
if (getLocale("en")?.showEnglishLanguageSwitch !== false) {
  localRoutes.push("en");
}
if (getLocale("fr")?.showFrenchLanguageSwitch) {
  localRoutes.push("fr");
}
if (getLocale("it")?.showItalianLanguageSwitch) {
  localRoutes.push("it");
}

/**
 * Root module
 */
@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
      useDefaultLang: true,
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
      compiler: {
        provide: TranslateCompiler,
        useClass: EnvTranslateCompiler,
      },
    }),
    RouterModule.forRoot(routes, { relativeLinkResolution: "legacy" }),
    SearchModule,
    TinyMceModule,
    CoreModule,
    MapModule,
    LocalizeRouterModule.forRoot(routes, {
      parser: {
        provide: LocalizeParser,
        useFactory: (translate, location, settings) =>
          new ManualParserLoader(
            translate,
            location,
            settings,
            localRoutes,
            LOCALIZED_ROUTER_PREFIX
          ),
        deps: [TranslateService, Location, LocalizeRouterSettings],
      },
    }),
    StoreModule.forRoot(coreReducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false,
      },
    }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot([RemoteFilterConfigsEffects]),
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [AppComponent],
  providers: [
    backendSearchServiceProvider,
    PreviousRouteServiceProvider,
    LocalizeRouterService,
    {
      provide: UrlSerializer,
      useClass: URLSearchParamsUrlSerializer,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
