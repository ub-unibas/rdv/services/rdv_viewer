#!/bin/sh

cd rdv-ws
yarn install
# probably not needed as universal viewer is available
# yarn run build-mirador
yarn run build-prod
cd ..
yarn upgrade rdv-lib
