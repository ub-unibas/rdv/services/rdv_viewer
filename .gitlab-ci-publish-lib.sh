#!/bin/sh

cd rdv-ws/projects/rdv-lib

dst=../../dist/rdv-lib

oldVersion=`grep version package.json |grep -o -E '[0-9]+\.[0-9]+.[0-9]+'`
git log --pretty=format:'%cs %an: %s' --reverse "lib-v${oldVersion}...HEAD" > /tmp/Changelog.txt

# increase version
newVersion=`npm version patch`
git commit -m"Bump rdv-lib version to lib-${newVersion}" package.json
git tag "lib-${newVersion}"
git push
git push --tags

# build
yarn run build-prod

mv /tmp/Changelog.txt $dst/

npm publish $dst/
