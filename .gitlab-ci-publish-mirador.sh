#!/bin/sh

cd rdv-ws/projects/mirador-with-plugins

dst=../../dist/mirador-with-plugins/

oldVersion=`grep version package.json |grep -o -E '[0-9]+\.[0-9]+.[0-9]+'`
git log --pretty=format:'%cs %an: %s' --reverse "mira-v${oldVersion}...HEAD" > /tmp/Changelog.txt

# increase version
newVersion=`npm version patch`
git commit -m"Bump mirador-with-plugins version to ${newVersion}" package.json
git tag "mira-${newVersion}"
git push
git push --tags

cd ../..

/bin/sh build-mirador-with-plugins.sh

cd projects/mirador-with-plugins

mv /tmp/Changelog.txt $dst/

npm publish $dst
