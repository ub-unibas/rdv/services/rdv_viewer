#!/bin/sh
rm -rf ../../dist/mirador-with-plugins
cd projects/mirador-with-plugins
yarn install
npm run start

# open in your browser: http://localhost:4200/
# or with disabled plugin "text-overlay": http://localhost:4200/?dp=text-overlay