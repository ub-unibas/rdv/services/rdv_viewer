#!/bin/sh

dst=../../dist/mirador-with-plugins

rm -rf $dst
cd projects/mirador-with-plugins
yarn install
npm run build
rm -f build/asset-manifest.json
mv build/index.html build/init.html
mkdir -p ../../dist/
rm -rf $dst
mv build $dst
cp package.json $dst/
cp Readme.txt  $dst/