import MiradorSharePlugin from "mirador-share-plugin";
import MiradorDownloadPlugin from "mirador-dl-plugin";
import mirador from "mirador";
import { miradorImageToolsPlugin } from "mirador-image-tools";
import defaultSettings from "./default_settings";
import MiradorTextOverlay from "mirador-textoverlay/es";
import canvasLinkPlugin from "mirador-canvaslink/es";
import imageCropperPlugin from "mirador-imagecropper/es";
import miradorOcrHelper from "@4eyes/mirador-ocr-helper/es";

import "@material-ui/core/styles/colorManipulator";
import "@material-ui/icons";

var usp = new URLSearchParams(window.location.search);
var manifestUrl = usp.get("manifest");
var searched = usp.get("h");
var locale = usp.get("locale");
var configUrl = usp.get("config");
var disabledPlugins = usp.get("dp");
var canvasIds = usp.get("canvasIds");

if(canvasIds && manifestUrl) {
  var searchSep = "?";
  if (manifestUrl.indexOf("?") >= 0) {
    searchSep = "&";
  }
  manifestUrl += searchSep + "canvasIds=" + encodeURIComponent(canvasIds);
}

const PLUGIN_SHARE = "share";
const PLUGIN_DOWNLOAD = "download";
const PLUGIN_IMAGE_TOOLS = "image-tools";
const PLUGIN_TEXT_OVERLAY = "text-overlay";
const PLUGIN_CANVAS_LINK = "canvas-link";
const PLUGIN_IMAGE_CROPPER = "image-cropper";
const PLUGIN_OCR_HELPER = "ocr-helper";

if (disabledPlugins) {
  disabledPlugins = disabledPlugins.split(",");
} else {
  disabledPlugins = [];
}

let viewerConfig = {
  plugins: [],
};

// NOTE use either PLUGIN_OCR_HELPER or PLUGIN_TEXT_OVERLAY, but never both!
if (
  disabledPlugins.indexOf(PLUGIN_TEXT_OVERLAY) < 0 &&
  disabledPlugins.indexOf(PLUGIN_OCR_HELPER) < 0
) {
  console.error(
    "Will disable " +
      PLUGIN_OCR_HELPER +
      ", because " +
      PLUGIN_TEXT_OVERLAY +
      " is also enabled."
  );
  disabledPlugins.push(PLUGIN_OCR_HELPER);
}

if (disabledPlugins.indexOf(PLUGIN_SHARE) < 0) {
  viewerConfig.plugins = viewerConfig.plugins.concat(MiradorSharePlugin);
}
if (disabledPlugins.indexOf(PLUGIN_DOWNLOAD) < 0) {
  viewerConfig.plugins = viewerConfig.plugins.concat(MiradorDownloadPlugin);
}
if (disabledPlugins.indexOf(PLUGIN_IMAGE_TOOLS) < 0) {
  viewerConfig.plugins.push(miradorImageToolsPlugin);
}
if (disabledPlugins.indexOf(PLUGIN_TEXT_OVERLAY) < 0) {
  viewerConfig.plugins = viewerConfig.plugins.concat(MiradorTextOverlay);
}
if (disabledPlugins.indexOf(PLUGIN_CANVAS_LINK) < 0) {
  viewerConfig.plugins.push(canvasLinkPlugin);
}
if (disabledPlugins.indexOf(PLUGIN_IMAGE_CROPPER) < 0) {
  viewerConfig.plugins.push(imageCropperPlugin);
}
if (disabledPlugins.indexOf(PLUGIN_OCR_HELPER) < 0) {
  viewerConfig.plugins.push(miradorOcrHelper);
}

function get(pathAndQuery, reqInit) {
  reqInit = reqInit || {
    method: "GET",
    mode: "cors",
    cache: "no-cache",
    headers: {
      "Content-Type": "application/json",
    },
  };
  if (reqInit.mode !== "cors" && !reqInit.credentials) {
    reqInit.credentials = "include"; // adds cookies!
  }

  return fetch(pathAndQuery, reqInit)
    .then(async function (response) {
      var status = response.status;
      var statusCodeCateory = Math.floor(status / 100);
      if (statusCodeCateory !== 2 && statusCodeCateory !== 3) {
        var payload = { error: "unknown" };
        try {
          payload = await response.json();
        } catch (e) {
          // NOP
        }
        return Promise.reject(
          new Error(
            "Bad response " +
              status +
              " for " +
              pathAndQuery +
              ": " +
              response.statusText,
            payload
          )
        );
      }
      return response.json();
    })
    .catch(function (ex) {
      console.error("** ERROR ** request failed", ex, "details:", ex.payload);
      return Promise.reject(ex); // DON'T FORGET THIS LINE, OTHERWISE FOLLOWING then() WILL BE TRIGGERED!
    });
}

function initMirador(config) {
  config.id = "mirador";
  config.language = locale;
  config.catalog = [
    {
      manifestId: manifestUrl,
      provider: "Universität Basel",
    },
  ];
  if (!config.windows) {
    config.windows = [];
  }

  let win;
  if (!config.windows || config.windows.length !== 1) {
    win = {};
    if (!config.windows) {
      config.windows = [];
    }
    config.windows.push(win);
  } else {
    win = config.windows[0];
  }
  win.loadedManifest = manifestUrl;
  if (win.imageToolsEnabled === undefined) {
    win.imageToolsEnabled = true;
  }
  if (win.imageToolsOpen === undefined) {
    win.imageToolsOpen = false;
  }
  if (!win.textOverlay) {
    win.textOverlay = {
      enabled: true,
      selectable: true,
      visible: false,
    };
  }
  if (disabledPlugins.indexOf(PLUGIN_IMAGE_TOOLS) >= 0) {
    win.imageToolsEnabled = false;
  }
  if (
    disabledPlugins.indexOf(PLUGIN_TEXT_OVERLAY) >= 0 &&
    disabledPlugins.indexOf(PLUGIN_OCR_HELPER) >= 0
  ) {
    win.textOverlay.enabled = false;
  }
  if (disabledPlugins.indexOf(PLUGIN_CANVAS_LINK) < 0) {
    win.canvasLink = {
      // the identifier of the manifest as the first argument and
      // an array of the currently displayed canvas objects as the second argument
      getCanvasLink: (manifestId, canvasObjects) => {
        const cos = canvasObjects ? canvasObjects : [];
        const coIds = cos.map((co) => (co.id ? co.id : "-1"));
        const url = window.location.origin + window.location.pathname;
        return `${url}?manifest=${encodeURIComponent(
          manifestId
        )}&canvasIds=${encodeURIComponent(coIds.join(","))}`;
      },
    };
  }

  if (!win.thumbnailNavigationPosition) {
    win.thumbnailNavigationPosition = "far-bottom";
  }
  if (win.maximized === undefined) {
    win.maximized = true;
  }
  win.defaultSearchQuery = searched;

  mirador.viewer(config, viewerConfig);
}

if (!manifestUrl) {
  // fails with PLUGIN_OCR_HELPER
  // manifestUrl = "https://iiif.europeana.eu/presentation/9200396/BibliographicResource_3000118436165/manifest";
  // works with PLUGIN_OCR_HELPER
  manifestUrl =
    "https://api.chgov.bar.admin.ch/manifests/32322791/32322791.json";
}

if (configUrl) {
  get(configUrl).then(function (config) {
    initMirador(config);
  });
} else {
  initMirador(defaultSettings);
}
