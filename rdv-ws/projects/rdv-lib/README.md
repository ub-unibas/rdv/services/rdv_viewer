# RdvLib

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.4.

## Code scaffolding

Run `ng generate component component-name --project rdv-lib` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project rdv-lib`.
> Note: Don't forget to add `--project rdv-lib` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `npm run build-dev` or `npm run build-prod` to build the project. The build artifacts will be stored in the `dist/` directory.

Use `build-dev` for development, because only here web component is working in IDEs.

If a prod build of a dependent project shall be created, use `build-prod`, followed by
`yarn upgrade rdv-lib`. Afterwards you can build the depdendent project as known.

## Publishing

After building your library with `npm run build`, go to the dist folder `cd dist/rdv-lib`.
Afterwards run in depending projects `yarn upgrade rdv-lib` and rebuild them too.

## Running unit tests

Run `ng test rdv-lib` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
