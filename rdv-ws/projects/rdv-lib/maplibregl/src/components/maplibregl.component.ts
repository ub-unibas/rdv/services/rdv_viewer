import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  ViewChild,
} from '@angular/core';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { MapConfig } from '@ub-unibas/rdv_viewer-lib/tool';
import type { Point } from 'geojson';
import GeoHash from 'latlon-geohash';
import * as maplibregl from 'maplibre-gl';
import { Observable, Subscription, debounce, fromEvent, timer } from 'rxjs';

/**
 * All information to re-render popup on language change.
 */
interface PopupInfo {
  popup: maplibregl.Popup;
  // mag: number;
  // causedTsunami: boolean;
  type: string;
}

export interface MapLimits {
  zoom: number;
  topLeft: maplibregl.LngLat;
  bottomRight: maplibregl.LngLat;
}

const maxZoom = 14;

@Component({
  selector: 'app-maplibre-gl',
  template: `<div #maplibregl class="maplibregl"></div>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MapLibreGlComponent implements AfterViewInit, OnDestroy {
  @Input() mapConfig: MapConfig;
  @Output() mapChanged: EventEmitter<MapLimits>;

  @ViewChild('maplibregl') maplibreglRef: ElementRef<HTMLElement>;
  protected map: maplibregl.Map;
  protected languageChangeSubscription: Subscription;
  protected resizeSubscription: Subscription;
  protected openPopups: PopupInfo[];
  protected resizeObservable$: Observable<Event>;
  protected lastInnerWindowWidth: number;
  protected lastInnerWindowHeight: number;

  constructor(protected localize: LocalizeRouterService, protected translate: TranslateService) {
    this.openPopups = [];
    this.lastInnerWindowHeight = window.innerHeight;
    this.lastInnerWindowWidth = window.innerWidth;
    this.mapChanged = new EventEmitter<MapLimits>();
  }

  ngAfterViewInit(): void {
    this.resizeObservable$ = fromEvent(window, 'resize').pipe(debounce(() => timer(1000)));
    this.resizeSubscription = this.resizeObservable$.subscribe((e) => {
      const newWindowInnerWidth = window.innerWidth;
      const newWindowInnerHeight = window.innerHeight;
      if (newWindowInnerHeight > this.lastInnerWindowHeight || newWindowInnerWidth > this.lastInnerWindowWidth) {
        // this.loadNewData(); // TODO enable? resize fires moveend too!
      }
      this.lastInnerWindowWidth = newWindowInnerWidth;
      this.lastInnerWindowHeight = newWindowInnerHeight;
    });
    this.languageChangeSubscription = this.translate.onLangChange.subscribe((params: LangChangeEvent) => this.languageChanged(params.lang));
    const mapOptions = this.mapConfig.config;
    this.initMapLibreGl(mapOptions.zoom, mapOptions.lng, mapOptions.lat);
  }

  protected addMapLayers() {
    this.map.addSource('earthquakes', {
      type: 'geojson',
      clusterMaxZoom: maxZoom + 1, // Max zoom to cluster points on
      clusterRadius: 50, // Radius of each cluster when clustering points (defaults to 50)
      data: {
        type: 'FeatureCollection',
        features: [],
      },
    });

    this.map.addLayer({
      id: 'clusters',
      type: 'circle',
      source: 'earthquakes',
      filter: ['has', 'point_count'],
      paint: {
        // Use step expressions (https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
        'circle-color': ['step', ['get', 'point_count'], '#51bbd6', 100, '#f1f075', 750, '#f28cb1'],
        'circle-radius': ['step', ['get', 'point_count'], 20, 100, 30, 750, 40],
      },
    });

    this.map.addLayer({
      id: 'cluster-count',
      type: 'symbol',
      source: 'earthquakes',
      filter: ['has', 'point_count'],
      layout: {
        'text-field': ['get', 'point_count_abbreviated'],
        'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
        'text-size': 12,
        'icon-allow-overlap': true,
      },
    });

    this.map.addLayer({
      id: 'unclustered-point',
      type: 'symbol',
      source: 'earthquakes',
      filter: ['!', ['has', 'point_count']],
      // layout: { 'icon-image': ['case', ['<', ['get', 'mag'], 3], 'weak', 'strong'], 'icon-size': 0.1 },
      layout: { 'icon-image': ['get', 'type'], 'icon-size': 0.1, 'icon-allow-overlap': true },
    });

    // inspect a cluster on click
    this.map.on('click', 'clusters', (e) => {
      const features = this.map.queryRenderedFeatures(e.point, {
        layers: ['clusters'],
      });
      const geoHash = features[0].properties.geoHash;
      console.log('Show geohash:', geoHash);
      const geoHashRect = GeoHash.neighbours(geoHash);
      const sw = GeoHash.decode(geoHashRect.sw);
      const ne = GeoHash.decode(geoHashRect.ne);

      this.map.fitBounds(new maplibregl.LngLatBounds(sw, ne));
    });

    this.map.on('click', 'unclustered-point', (e) => {
      const coordinates = this.featureCoo(e.features).slice() as [number, number];
      const type = e.features[0].properties.type;
      const geoHash = e.features[0].properties.geoHash;
      const lat = e.features[0].properties.lat;
      const lng = e.features[0].properties.lng;

      // Ensure that if the map is zoomed out such that
      // multiple copies of the feature are visible, the
      // popup appears over the copy being pointed to.
      while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
        coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
      }

      const popup = new maplibregl.Popup()
        .setLngLat(coordinates)
        .setHTML(this.translate.instant('mapbox.Component.Popup_html', { type, geoHash, lat, lng }))
        .addTo(this.map);
      this.openPopups.push({ popup, type });
      popup.on('close', () => {
        for (let i = 0; i < this.openPopups.length; i++) {
          if (this.openPopups[i].popup === popup) {
            this.openPopups.splice(i, 1);
            break;
          }
        }
      });
    });

    this.map.on('mouseenter', 'clusters', (e) => {
      this.map.getCanvas().style.cursor = 'pointer';
      console.log('GEOHASH', e.features[0].properties.geoHash);
    });
    this.map.on('mouseleave', 'clusters', () => {
      this.map.getCanvas().style.cursor = '';
    });

    this.map.on('mouseenter', 'unclustered-point', () => {
      this.map.getCanvas().style.cursor = 'pointer';
    });
    this.map.on('mouseleave', 'unclustered-point', () => {
      this.map.getCanvas().style.cursor = '';
    });
  }

  protected initMapLibreGl(zoom: number, lng: number, lat: number) {
    const mapConfig = this.mapConfig;
    const mapOptions = mapConfig.config;

    const localeKeys = [
      'AttributionControl.ToggleAttribution',
      'AttributionControl.MapFeedback',
      'FullscreenControl.Enter',
      'FullscreenControl.Exit',
      'GeolocateControl.FindMyLocation',
      'GeolocateControl.LocationNotAvailable',
      'LogoControl.Title',
      'Map.Title',
      'NavigationControl.ResetBearing',
      'NavigationControl.ZoomIn',
      'NavigationControl.ZoomOut',
      'ScrollZoomBlocker.CtrlMessage',
      'ScrollZoomBlocker.CmdMessage',
      'TouchPanBlocker.Message',
    ];
    const locale = {};
    for (const k of localeKeys) {
      locale[k] = this.translate.instant('mapbox.' + k);
    }

    const mapLibreGlOptions: maplibregl.MapOptions = {
      container: this.maplibreglRef.nativeElement,
      style: mapOptions.style,
      zoom: zoom,
      center: [lng, lat],
      locale: locale,
      minZoom: 2.5,
      maxZoom: maxZoom,
    };
    this.map = new maplibregl.Map(mapLibreGlOptions);
    this.map.addControl(new maplibregl.NavigationControl());
    this.map.on('zoomend', (e) => {
      const currentZoom = this.map.getZoom();
      console.log('ZOOM', currentZoom);
      // clusters may change ...
      this.signalMapLimitChange();
    });
    this.map.on('mouseup', (e) => {
      const bbox = this.map.getBounds();
      console.log('AT', e.lngLat, 'center=', this.map.getCenter(), JSON.stringify(bbox));
    });
    this.map.on('moveend', () => {
      // get new data for the new visible part of the map ...
      this.signalMapLimitChange();
    });

    // load data ...
    this.map.on('load', () => {
      this.updateMapLanguage();
      // no svgs! png or jpeg only!
      const images = [
        { url: '/assets/img/weak.png', id: 'weak' },
        { url: '/assets/img/strong.png', id: 'strong' },
        { url: '/assets/img/red-cross.png', id: 'flag' },
      ];
      Promise.all(
        images.map(
          (img) =>
            new Promise<void>((resolve, _reject) => {
              this.map.loadImage(img.url, (error, res) => {
                if (error === null) {
                  this.map.addImage(img.id, res);
                } else {
                  console.error("Can't load image " + img.id + ' ' + img.url + ': ' + error);
                }
                resolve();
              });
            })
        )
      ).then(() => this.addMapLayers());
    });
  }

  protected equalLatLng(v1: maplibregl.LngLat | undefined, v2: maplibregl.LngLat | undefined): boolean {
    if (v1 !== undefined && v2 !== undefined) {
      const equal = v1.lat === v2.lat && v1.lng === v2.lng;
      return equal;
    }
    return false;
  }

  public currentRectangle(): { topLeft: maplibregl.LngLat; bottomRight: maplibregl.LngLat } {
    const bbox = this.map.getBounds();
    const topLeft = bbox.getNorthWest();
    const bottomRight = bbox.getSouthEast();
    return { topLeft, bottomRight };
  }

  protected signalMapLimitChange() {
    const bbox = this.map.getBounds();
    const topLeft = bbox.getNorthWest();
    const bottomRight = bbox.getSouthEast();
    this.mapChanged.emit({ topLeft, bottomRight, zoom: this.map.getZoom() });
  }

  public setGeoPoints(geoPoints: GeoJSON.FeatureCollection<GeoJSON.Geometry>) {
    if (!this.getSource()) {
      setTimeout(() => this.setGeoPoints(geoPoints), 250);
    } else {
      this.getSource().setData(geoPoints);
    }
  }

  getSource(): maplibregl.GeoJSONSource {
    return this.map.getSource('earthquakes') as maplibregl.GeoJSONSource;
  }

  zoomTo(zoom: number, options?: maplibregl.AnimationOptions, eventData?: any) {
    this.map.zoomTo(zoom, options, eventData);
  }

  getMap(): maplibregl.Map {
    return this.map;
  }

  getZoom(): number {
    return this.map.getZoom();
  }

  flyTo(options: maplibregl.FlyToOptions, eventData?: any) {
    return this.map.flyTo(options, eventData);
  }

  protected featureCoo(features: maplibregl.GeoJSONFeature[]): [number, number] {
    return (features[0].geometry as Point).coordinates as [number, number];
  }

  protected updateMapLanguage() {
    const l = this.translate.currentLang;
    this.map.getStyle().layers.forEach((layer) => {
      if (layer.id.endsWith('-label')) {
        this.map.setLayoutProperty(layer.id, 'text-field', ['coalesce', ['get', `name_${l}`], ['get', 'name']]);
      }
    });
    // this.openPopups.forEach((p) => p.remove());
    // this.openPopups = [];
    this.openPopups.forEach((p) =>
      p.popup.setHTML(
        this.translate.instant('mapbox.Component.Popup_html', {
          type: p.type,
        })
      )
    );
  }

  protected languageChanged(language) {
    this.updateMapLanguage();
  }

  ngOnDestroy(): void {
    if (this.languageChangeSubscription) {
      this.languageChangeSubscription.unsubscribe();
    }
    if (this.resizeSubscription) {
      this.resizeSubscription.unsubscribe();
    }
    this.removeMapLibreGl();
  }

  protected removeMapLibreGl() {
    this.openPopups = [];
    // TODO equivalent in  maplibre-gl?
    // mapboxgl.clearStorage();
    if (this.map) {
      this.map.remove();
    }
  }
}
