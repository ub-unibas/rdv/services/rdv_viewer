import { Action } from '@ngrx/store';
import { GeoLocation } from '../components/Map';

/* eslint-disable @typescript-eslint/no-shadow */
export enum MapActionTypes {
  SetZoom = '[Map] Set current zoom',
  SetCenter = '[Map] Set center of map',
}
/* eslint-enable @typescript-eslint/no-shadow */

export class SetZoom implements Action {
  /**
   * @ignore
   */
  readonly type = MapActionTypes.SetZoom;

  /**
   * Contains the zoom
   *
   * @param payload
   */
  constructor(public payload: number) {}
}

export class SetCenter implements Action {
  /**
   * @ignore
   */
  readonly type = MapActionTypes.SetCenter;

  /**
   * Contains the center
   *
   * @param payload
   */
  constructor(public payload: GeoLocation) {}
}

export type MapActions = SetZoom | SetCenter;
