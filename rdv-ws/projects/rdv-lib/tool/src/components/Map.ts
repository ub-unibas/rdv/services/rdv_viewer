import GeoHash from 'latlon-geohash';
// THIS IMPORT IS NEEDED EVEN IF IDES THINK SOMETHING ELSE!
import type { GeoJSON } from 'geojson';

export enum MapProvider {
  MAPBOX = 'mapbox',
  MAPLIBRE_GL = 'maplibre-gl',
}

export interface ZoomToPrecision {
  zoomStart: number;
  zoomEnd: number;
  precision: number;
}

export interface MapConfig {
  /**
   * Provider specific config
   */
  config: any;
  /**
   * Which provider?
   */
  provider: MapProvider;
  /**
   * Credentials to use map API
   */
  credentials?: any; // depends on provider
  /**
   * coordinate facet key in environment.facetFields
   */
  coordinateFacetKey: string;
  /**
   * year facet key in environment.facetFields
   */
  yearFacetKey: string;
  /**
   * default cluster precision
   */
  zoomPrecisionMapping: ZoomToPrecision[];
  /**
   * A list of 'CENTROID', 'CENTER', 'NE' in any order, but with at least one element.
   */
  positionHint: RenderHint[];
  /**
   * Projection - value depends on provider
   */
  projection?: any;
}

export function precisionForZoom(zoom: number | undefined, map: ZoomToPrecision[] | undefined): number {
  zoom = zoom ?? 7;
  map = map ?? [];
  for (const m of map) {
    if (zoom >= m.zoomStart && zoom < m.zoomEnd) {
      return m.precision;
    }
  }
  if (zoom >= 14) {
    return map[map.length - 1].precision;
  }
  return 1;
}

export interface GeoLocation {
  lat: number;
  lon: number;
}

export interface Centroid {
  location: GeoLocation;
  count: number;
}

export interface Centroid {
  centroid: Centroid;
}

export interface GeoBucket extends Centroid {
  count: number;
  label: string; // == geo hash
}

export interface DocTranslation {
  object_type?: string;
  line1?: string;
}

export interface GeoPoint extends Centroid {
  id: string;
  geo_key: string;
  object_type: string;
  title: string;
  i18n: { [key: string]: DocTranslation };
}

function log(...args: any[]) {
  if ((window as any).DEBUG_COORDS === true) {
    console.log(...args);
  }
}

function checkLatLon(
  renderHint: RenderHint,
  lat: number,
  lon: number,
  minLon: number,
  maxLon: number,
  minLat: number,
  maxLat: number,
  geoHash: string,
  geoHashSW: any,
  geoHashNE: any
): boolean {
  const latInside = lat >= geoHashSW.lat && lat <= geoHashNE.lat;
  if (!latInside) {
    log('-- LAT OUTSIDE GEOHASH:', renderHint, geoHash, lat, geoHashSW.lat, '..', geoHashNE.lat);
  }
  const latInsideBox = lat >= minLat && lat <= maxLat;
  if (!latInsideBox) {
    log('-- LAT OUTSIDE BOX:', renderHint, geoHash, lat, minLat, '..', maxLat);
  }
  const lonInside = lon >= geoHashSW.lon && lon <= geoHashNE.lon;
  if (!lonInside) {
    log('-- LON OUTSIDE GEOHASH', renderHint, geoHash, lon, geoHashSW.lon, '..', geoHashNE.lon);
  }
  const lonInsideBox = lon >= minLon && lon <= maxLon;
  if (!lonInsideBox) {
    log('-- LON OUTSIDE BOX:', renderHint, geoHash, lon, minLon, '..', maxLon);
  }
  return latInside && latInsideBox && lonInside && lonInsideBox;
}

export enum RenderHint {
  CENTROID = 'CENTROID',
  CENTER = 'CENTER',
  NE = 'NE',
}

export interface LatLng {
  lat: number;
  lng: number;
}

export interface LatLon {
  lat: number;
  lon: number;
}

export type MapPoint = LatLng | LatLon;

export function validGeoHashBounds(geoHash: string): { ne: { lat: number; lon: number }; sw: { lat: number; lon: number } } {
  return GeoHash.bounds(geoHash);
}

export function cooOfPoint(
  geoHash: string,
  point: Centroid,
  docCount: number,
  minLon: number,
  maxLon: number,
  minLat: number,
  maxLat: number,
  renderHint: RenderHint[]
): GeoLocation | undefined {
  const { ne, sw } = validGeoHashBounds(geoHash);
  const latOrdered = sw.lat <= ne.lat;
  const lngOrdered = sw.lon <= ne.lon;
  if (!latOrdered || !lngOrdered) {
    console.warn('ATTENTION unexpected lat/lon relation of geohash bounds!');
  }

  let lat: number | undefined;
  let lon: number | undefined;
  for (const rh of renderHint) {
    lat = undefined;
    lon = undefined;
    switch (rh) {
      case 'CENTER':
        lat = sw.lat + (ne.lat - sw.lat) / 2;
        lon = sw.lon + (ne.lon - sw.lon) / 2;
        break;
      case 'CENTROID':
        // if some docs of the geohash have multiple coordinates ... can't use centroid
        // centroid's count is then higher than the doc count
        if (point.centroid.location && docCount === point.centroid.count) {
          lat = point.centroid.location.lat;
          lon = point.centroid.location.lon;
        } else {
          log('-- CLUSTER/POINT WITH MULTI LOCATION DOC', geoHash, "(can't use centroid)");
        }
        break;
      case 'NE':
        lat = ne.lat;
        lon = ne.lon;
        break;
    }
    if (lat !== undefined && lon !== undefined && checkLatLon(rh, lat, lon, minLon, maxLon, minLat, maxLat, geoHash, sw, ne)) {
      log('** USING', rh, geoHash, lat, lon);
      return { lat, lon };
    }
  }

  log('Ignoring', geoHash);
  return undefined;
}

// render single geo points
export function geoSnippetsToGeoJson(
  buckets: GeoPoint[],
  minLon: number,
  maxLon: number,
  minLat: number,
  maxLat: number,
  renderHint: RenderHint[]
): GeoJSON.FeatureCollection<GeoJSON.Geometry> {
  const features: any[] = [];

  (buckets ?? []).forEach((b) => {
    const count = 1;
    const latLon = cooOfPoint(b.geo_key, b, count, minLon, maxLon, minLat, maxLat, renderHint);
    if (latLon) {
      const properties: { [key: string]: any } = {
        id: b.id,
        lng: latLon.lon,
        lat: latLon.lat,
        type: 'point',
        geoHash: b.geo_key,
        snippet: b,
      };
      features.push({
        type: 'Feature',
        properties,
        geometry: { type: 'Point', coordinates: [properties.lng, properties.lat] },
      });
    }
  });

  return {
    type: 'FeatureCollection',
    features,
  };
}

// render clusters
export function geoFacetToGeoJson(
  points: GeoBucket[],
  minLon: number,
  maxLon: number,
  minLat: number,
  maxLat: number,
  renderHint: RenderHint[]
): GeoJSON.FeatureCollection<GeoJSON.Geometry> {
  const features: any[] = [];

  (points ?? []).forEach((b, index) => {
    const count = b.count;
    const geoHash = b.label; // really!
    const latLon = cooOfPoint(geoHash, b, count, minLon, maxLon, minLat, maxLat, renderHint);
    if (latLon) {
      const properties: { [key: string]: any } = {
        id: geoHash,
        lng: latLon.lon,
        lat: latLon.lat,
        geoHash: geoHash,
        cluster: true,
        cluster_id: index,
        point_count: count,
        point_count_abbreviated: count,
      };

      features.push({
        type: 'Feature',
        properties,
        geometry: { type: 'Point', coordinates: [properties.lng, properties.lat] },
      });
    }
  });

  return {
    type: 'FeatureCollection',
    features,
  };
}
