import { EventEmitter } from '@angular/core';

export interface HtmlEditor {
  html: string;
  settings: any;
  changedHtml: EventEmitter<string>;
}
