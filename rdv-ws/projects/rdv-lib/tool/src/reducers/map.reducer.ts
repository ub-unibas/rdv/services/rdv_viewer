import * as fromActions from '../actions/map.actions';
import { GeoLocation } from '../components/Map';

export interface State {
  zoom: number;
  center: GeoLocation;
}

export let initialState: State;

export function initMapReducer(zoom: number, latCenter: number, lngCenter: number): void {
  initialState = { zoom, center: { lat: latCenter, lon: lngCenter } };
}

/**
 * @ignore
 */
export function reducer(state = initialState, action: fromActions.MapActions): State {
  switch (action.type) {
    case fromActions.MapActionTypes.SetZoom:
      return {
        ...state,
        zoom: action.payload,
      };

    case fromActions.MapActionTypes.SetCenter:
      return {
        ...state,
        center: action.payload,
      };

    default:
      return state;
  }
}
