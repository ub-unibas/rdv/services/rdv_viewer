import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromReducer from './map.reducer';

export interface MapConfigState {
  map: fromReducer.State;
}

export interface MapState {
  map: MapConfigState;
}

export const reducers: ActionReducerMap<MapConfigState> = {
  map: fromReducer.reducer,
};

const _getMapState = createFeatureSelector<MapConfigState>('map');

const _getMapConfig = createSelector(_getMapState, (ms) => ms.map);

export const getMapZoom = createSelector(_getMapConfig, (mc) => mc.zoom);

export const getMapCenter = createSelector(_getMapConfig, (mc) => mc.center);
