import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  ViewChild,
} from '@angular/core';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { LatLng, MapConfig } from '@ub-unibas/rdv_viewer-lib/tool';
// THE IMPORT OF GeoJSON IS NEEDED EVEN IF IDES THINK SOMETHING ELSE!
import type { Point, GeoJSON } from 'geojson';
import * as mapboxgl from 'mapbox-gl';
import { Observable, Subscription, debounce, fromEvent, timer } from 'rxjs';

/**
 * All information to re-render popup on language change.
 */
interface PopupInfo {
  popup: mapboxgl.Popup;
  // mag: number;
  // causedTsunami: boolean;
  type: string;
}

export interface MapboxImages {
  url: string;
  id: string;
}

export interface MapLimits {
  zoom: number;
  topLeft: mapboxgl.LngLat;
  bottomRight: mapboxgl.LngLat;
  center: mapboxgl.LngLat;
}

export interface MapboxEventHandler<T extends keyof mapboxgl.MapLayerEventType> {
  type: T;
  layer: string | ReadonlyArray<string>;
  listener: (map: mapboxgl.Map, ev: mapboxgl.MapLayerEventType[T] & mapboxgl.EventData) => void;
}

const maxZoom = 14;

@Component({
  selector: 'app-mapbox',
  template: ` <div #mapbox class="mapbox"></div> `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MapBoxComponent implements AfterViewInit, OnDestroy {
  @Input() mapConfig: MapConfig;
  @Input() images: MapboxImages[];
  @Input() layers: mapboxgl.AnyLayer[];
  @Input() eventHandlers: MapboxEventHandler<any>[];
  @Output() mapChanged: EventEmitter<MapLimits>;

  @ViewChild('mapbox') mapboxRef: ElementRef<HTMLElement>;
  protected map: mapboxgl.Map;
  protected languageChangeSubscription: Subscription;
  protected resizeSubscription: Subscription;
  protected openPopups: PopupInfo[];
  protected resizeObservable$: Observable<Event>;
  protected lastInnerWindowWidth: number;
  protected lastInnerWindowHeight: number;

  constructor(protected localize: LocalizeRouterService, protected translate: TranslateService) {
    this.openPopups = [];
    this.lastInnerWindowHeight = window.innerHeight;
    this.lastInnerWindowWidth = window.innerWidth;
    this.mapChanged = new EventEmitter<MapLimits>();
  }

  ngAfterViewInit(): void {
    this.resizeObservable$ = fromEvent(window, 'resize').pipe(debounce(() => timer(1000)));
    this.resizeSubscription = this.resizeObservable$.subscribe((e) => {
      const newWindowInnerWidth = window.innerWidth;
      const newWindowInnerHeight = window.innerHeight;
      if (newWindowInnerHeight > this.lastInnerWindowHeight || newWindowInnerWidth > this.lastInnerWindowWidth) {
        // this.loadNewData(); // TODO enable? resize fires moveend too!
      }
      this.lastInnerWindowWidth = newWindowInnerWidth;
      this.lastInnerWindowHeight = newWindowInnerHeight;
    });
    this.languageChangeSubscription = this.translate.onLangChange.subscribe((params: LangChangeEvent) => this.languageChanged(params.lang));
    const mapOptions = this.mapConfig.config;
    this.initMapbox(mapOptions.zoom, mapOptions.lng, mapOptions.lat);
  }

  protected addMapLayers() {
    this.map.addSource('earthquakes', {
      type: 'geojson',
      // data: 'https://docs.mapbox.com/mapbox-gl-js/assets/earthquakes.geojson',
      cluster: false,
      clusterMaxZoom: maxZoom + 1, // Max zoom to cluster points on
      clusterRadius: 50, // Radius of each cluster when clustering points (defaults to 50)
      data: {
        type: 'FeatureCollection',
        features: [],
      },
    });

    this.layers.forEach((l) => this.map.addLayer(l));
    this.eventHandlers.forEach(({ type, layer, listener }) => this.map.on(type, layer, (e) => listener(this.map, e) ));
  }

  protected initMapbox(zoom: number, lng: number, lat: number) {
    const mapConfig = this.mapConfig;
    const mapOptions = mapConfig.config;

    const localeKeys = [
      'AttributionControl.ToggleAttribution',
      'AttributionControl.MapFeedback',
      'FullscreenControl.Enter',
      'FullscreenControl.Exit',
      'GeolocateControl.FindMyLocation',
      'GeolocateControl.LocationNotAvailable',
      'LogoControl.Title',
      'Map.Title',
      'NavigationControl.ResetBearing',
      'NavigationControl.ZoomIn',
      'NavigationControl.ZoomOut',
      'ScrollZoomBlocker.CtrlMessage',
      'ScrollZoomBlocker.CmdMessage',
      'TouchPanBlocker.Message',
    ];
    const locale = {};
    for (const k of localeKeys) {
      locale[k] = this.translate.instant('mapbox.' + k);
    }
    const mapboxOptions: mapboxgl.MapboxOptions = {
      accessToken: mapConfig.credentials,
      container: this.mapboxRef.nativeElement,
      style: mapOptions.style,
      zoom: zoom,
      center: [lng, lat],
      locale: locale,
      minZoom: 2.5,
      maxZoom: maxZoom,
      projection: mapConfig.projection,
    };
    this.map = new mapboxgl.Map(mapboxOptions);
    this.map.addControl(new mapboxgl.NavigationControl());
    this.map.addControl(new mapboxgl.ScaleControl({ maxWidth: 100 }), 'bottom-right');
    // zoom causes move too, so do it only once
    // this.map.on('zoomend', (e) => {
    //   const currentZoom = this.map.getZoom();
    //   console.log('ZOOM', currentZoom);
    //   // clusters may change ...
    //   this.signalMapLimitChange();
    // });
    this.map.on('mouseup', (e) => {
      const bbox = this.currentRectangle();
      if ((window as any).DEBUG_COORDS) {
        console.log('AT', e.lngLat, 'center=', this.map.getCenter(), JSON.stringify(bbox));
      }
    });
    this.map.on('moveend', () => {
      const currentZoom = this.map.getZoom();
      if ((window as any).DEBUG_COORDS) {
        console.log('ZOOM', currentZoom);
      }
      // get new data for the new visible part of the map ...
      this.signalMapLimitChange();
    });

    // load data ...
    this.map.on('load', () => {
      // mapbox 1.13.3 doesn't support mapboxOptions.language (>=2.0.0)
      // this approach fixes the language switch issue in 2.15.0 too.
      this.updateMapLanguage();
      // no svgs! png or jpeg only!
      Promise.all(
        this.images.map(
          (img) =>
            new Promise<void>((resolve, _reject) => {
              this.map.loadImage(img.url, (error, res) => {
                if (error === null) {
                  this.map.addImage(img.id, res);
                } else {
                  console.error("Can't load image " + img.id + ' ' + img.url + ': ' + error);
                }
                resolve();
              });
            })
        )
      ).then(() => this.addMapLayers());
    });
  }

  protected equalLatLng(v1: mapboxgl.LngLat | undefined, v2: mapboxgl.LngLat | undefined): boolean {
    if (v1 !== undefined && v2 !== undefined) {
      const equal = v1.lat === v2.lat && v1.lng === v2.lng;
      return equal;
    }
    return false;
  }

  protected clipToRange(p: mapboxgl.LngLat): LatLng {
    let lat = p.lat;
    let lng = p.lng;

    if (lng < -180) {
      lng = -180;
    } else if (lng > 180) {
      lng = 180;
    }

    if (lat < -90) {
      lat = -90;
    } else if (lat > 90) {
      lat = 90;
    }
    return { lat, lng };
  }

  public currentRectangle(): { topLeft: LatLng; bottomRight: LatLng } {
    const bbox = this.map.getBounds();
    const topLeft = bbox.getNorthWest();
    const bottomRight = bbox.getSouthEast();
    return { topLeft: this.clipToRange(topLeft), bottomRight: this.clipToRange(bottomRight) };
  }

  protected signalMapLimitChange() {
    const bbox = this.map.getBounds();
    const topLeft = bbox.getNorthWest();
    const bottomRight = bbox.getSouthEast();
    const center = this.map.getCenter();
    this.mapChanged.emit({ topLeft, bottomRight, center, zoom: this.map.getZoom() });
  }

  public setGeoPoints(geoPoints: GeoJSON.FeatureCollection<GeoJSON.Geometry>) {
    if (!this.getSource()) {
      setTimeout(() => this.setGeoPoints(geoPoints), 250);
    } else {
      this.getSource().setData(geoPoints);
    }
  }

  getSource(): mapboxgl.GeoJSONSource {
    return this.map.getSource('earthquakes') as mapboxgl.GeoJSONSource;
  }

  zoomTo(zoom: number, options?: mapboxgl.AnimationOptions, eventData?: mapboxgl.EventData) {
    this.map.zoomTo(zoom, options, eventData);
  }

  getMap(): mapboxgl.Map {
    return this.map;
  }

  getZoom(): number {
    return this.map.getZoom();
  }

  flyTo(options: mapboxgl.FlyToOptions, eventData?: mapboxgl.EventData) {
    return this.map.flyTo(options, eventData);
  }

  protected updateMapLanguage() {
    const l = this.translate.currentLang;
    // Not working in mapbox 2.15.0 if language is set in the mapbox init options!
    this.map.getStyle().layers.forEach((layer) => {
      if (layer.id.endsWith('-label')) {
        this.map.setLayoutProperty(layer.id, 'text-field', ['coalesce', ['get', `name_${l}`], ['get', 'name']]);
      }
    });
  }

  protected languageChanged(language) {
    this.updateMapLanguage();
  }

  ngOnDestroy(): void {
    if (this.languageChangeSubscription) {
      this.languageChangeSubscription.unsubscribe();
    }
    if (this.resizeSubscription) {
      this.resizeSubscription.unsubscribe();
    }
    this.removeMapbox();
  }

  protected removeMapbox() {
    mapboxgl.clearStorage();
    if (this.map) {
      this.map.remove();
    }
  }
}
