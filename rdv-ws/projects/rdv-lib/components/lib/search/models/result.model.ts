/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

export interface DocTranslation {
  object_type?: string;
  line1?: string;
}

export interface Doc {
  /**
   * Unique id of result
   */
  id: string;
  title?: string;
  object_type?: string;
  search_after_values: string;
  preview_image: string; // url
  line1?: string;
  fulltext_snippet: string; // html
  fulltext?: string;
  link?: { label?: string; url: string }[];
  i18n: { [key: string]: DocTranslation };
  // runtime
  hitNr?: number;
}

/**
 * Currently displayed result
 */
export type Result = Doc;

export interface GeoResult {
  id: string;
  title?: string;
  object_type?: string;
  search_after_values: string;
  preview_image: string | null;
  i18n: { [key: string]: DocTranslation };
  centroid?: {
    location: { lat: number; lon: number };
    count: number;
  };
  geo_key: string;
}
