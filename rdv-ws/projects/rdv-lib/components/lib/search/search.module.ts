/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchComponent } from './components/search.component';
// import {ChartsModule} from 'ng2-charts';
// import {NgxChartsModule} from '@swimlane/ngx-charts';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { LocalizeRouterModule } from '@gilsdav/ngx-translate-router';
import { NgSelectModule } from '@ng-select/ng-select';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { TranslateModule } from '@ngx-translate/core';
import { ClipboardModule } from 'ngx-clipboard';
import { ToastrModule } from 'ngx-toastr';
import { PipesModule } from '../shared/pipes/index';
import { BasketRequestInterceptor } from '../shared/services/basket-request-interceptor.service';
import { DetailedRequestInterceptor } from '../shared/services/detailed-request-interceptor.service';
import { SearchRequestInterceptor } from '../shared/services/search-request-interceptor.service';
import { AutocompleteComponent } from './components/autocomplete.component';
import { BasketIconComponent } from './components/basket-icon.component';
import { BasketListComponent } from './components/basket-list.component';
import { BasketResultsListComponent } from './components/basket-results-list.component';
import { CheckboxFacetComponent } from './components/checkbox-facet.component';
import { CollapsibleFacetsComponent } from './components/collapsible-facets.component';
import { CollapsibleComponent } from './components/collapsible.component';
import { CopyLinkComponent } from './components/copy-link.component';
import { DateHierarchicFacetComponent } from './components/date-hierarchic-facet.component';
import { DateHistogramFacetComponent } from './components/date-histogram-facet.component';
import { DetailedFieldComponent } from './components/detailed-field.component';
import { DetailedComponent } from './components/detailed.component';
import { DocViewModeComponent } from './components/doc-view-mode.component';
import { EditFieldComponent } from './components/edit-field.component';
import { ExpandableComponent } from './components/expandable.component';
import { ExportResultsListComponent } from './components/export-results-list.component';
import { FacetControlsComponent } from './components/facet-controls.component';
import { FacetExpandableComponent } from './components/facet-expandable.component';
import { FacetOrderSelectorComponent } from './components/facet-order-selector.component';
import { FacetsComponent } from './components/facets.component';
import { FieldsComponent } from './components/fields.component';
import { FiltersComponent } from './components/filters.component';
import { GeoHierarchicFacetComponent } from './components/geo-hierarchic-facet.component';
import { GeoHistogramFacetComponent } from './components/geo-histogram-facet.component';
import { HierachicFacetComponent } from './components/hierachic-facet.component';
import { InFacetSearchComponent } from './components/in-facet-search.component';
import { IntHierarchicFacetComponent } from './components/int-hierarchic-facet.component';
import { IntHistogramFacetComponent } from './components/int-histogram-facet.component';
import { ManageSavedQueriesComponent } from './components/manage-saved-queries.component';
import { ManageSearchComponent } from './components/manage-search.component';
import { MinMaxDateInputComponent } from './components/min-max-date-input.component';
import { MinMaxGeoInputComponent } from './components/min-max-geo-input.component';
import { MinMaxIntInputComponent } from './components/min-max-int-input.component';
import { MiradorComponent } from './components/mirador.component';
import { NewSearchComponent } from './components/new-search.component';
import { OperatorSelectorComponent } from './components/operator-selector.component';
import { ParamsSetComponent } from './components/params-set.component';
import { PopupLandingComponent } from './components/popup-landing.component';
import { RangesComponent } from './components/ranges.component';
import { ResultHeaderComponent } from './components/result-header.component';
import { ResultListsComponent } from './components/result-lists.component';
import { ResultPagingComponent } from './components/result-paging.component';
import { ResultRowComponent } from './components/result-row.component';
import { RowsPerPageComponent } from './components/rows-per-page.component';
import { SaveQueryComponent } from './components/save-query.component';
import { SearchHitValueComponent } from './components/search-hit-value.component';
import { SearchHitComponent } from './components/search-hit.component';
import { SearchParamsComponent } from './components/search-params.component';
import { SearchResultsListComponent } from './components/search-results-list.component';
import { SearchResultsComponent } from './components/search-results.component';
import { SearchResultsPagingComponent } from './components/search-results.paging.component';
import { SelectedFacetsComponent } from './components/selected-facets.component';
import { SimpleCollapsibleFacetComponent } from './components/simple-collapsible-facet.component';
import { SimpleFacetComponent } from './components/simple-facet.component';
import { SimpleSearchAutocompleteComponent } from './components/simple-search-autocomplete.component';
import { SimpleSearchComponent } from './components/simple-search.component';
import { SubcatFacetComponent } from './components/subcat-facet.component';
import { TopComponent } from './components/top.components';
import { UniversalViewerComponent } from './components/universal-viewer.component';
import { UvManifestLinkComponent } from './components/uv-manifest-link.component';
import { ValidatableListComponent } from './components/validatable-list.component';
import { VisualSearchComponent } from './components/visual-search.component';
import { FormEffects } from './effects/form.effects';
import { QueryEffects } from './effects/query.effects';
import * as fromSearch from './reducers/index';
import { BrokenImageLinkDirective } from './shared/directives/broken-image-link.directive';
import { DisplayLinkDirective } from './shared/directives/display-link.directive';
import { PopperDirective } from './shared/directives/popper.directive';
import { SafePipe } from './shared/directives/safe-src.directive';
import { DynamicModule } from 'ng-dynamic-component';
import { SimpleHtmlEditorComponent } from './components/simple-html-editor.component';
// import { ToolModule } from '../../../tool/public-api';
// import { LocalizeRouterModule } from '@gilsdav/ngx-translate-router';
// import { RouterModule } from '@angular/router';

/**
 * Provides components for search and display of search results
 */

const SEARCH_COMPONENTS = [
  SearchComponent,
  NewSearchComponent,
  PopupLandingComponent,
  TopComponent,
  SimpleSearchComponent,
  SearchResultsComponent,
  SearchHitComponent,
  SearchHitValueComponent,
  CollapsibleComponent,
  ExpandableComponent,
  SimpleFacetComponent,
  CheckboxFacetComponent,
  FacetExpandableComponent,
  OperatorSelectorComponent,
  FacetControlsComponent,
  FacetOrderSelectorComponent,
  SimpleCollapsibleFacetComponent,
  IntHistogramFacetComponent,
  IntHierarchicFacetComponent,
  DateHistogramFacetComponent,
  DateHierarchicFacetComponent,
  GeoHistogramFacetComponent,
  GeoHierarchicFacetComponent,
  MinMaxIntInputComponent,
  MinMaxDateInputComponent,
  MinMaxGeoInputComponent,
  HierachicFacetComponent,
  CollapsibleFacetsComponent,
  SelectedFacetsComponent,
  InFacetSearchComponent,
  ManageSearchComponent,
  SearchParamsComponent,
  ParamsSetComponent,
  CopyLinkComponent,
  SaveQueryComponent,
  ManageSavedQueriesComponent,
  BasketListComponent,
  FieldsComponent,
  FiltersComponent,
  VisualSearchComponent,
  FacetsComponent,
  SubcatFacetComponent,
  RangesComponent,
  ResultListsComponent,
  BasketResultsListComponent,
  SearchResultsListComponent,
  BasketIconComponent,
  ExportResultsListComponent,
  DisplayLinkDirective,
  BrokenImageLinkDirective,
  SafePipe,
  ResultRowComponent,
  ResultPagingComponent,
  SearchResultsPagingComponent,
  ResultHeaderComponent,
  RowsPerPageComponent,
  DetailedComponent,
  UniversalViewerComponent,
  UvManifestLinkComponent,
  MiradorComponent,
  DetailedFieldComponent,
  EditFieldComponent,
  PopperDirective,
  DocViewModeComponent,
  AutocompleteComponent,
  ValidatableListComponent,
  SimpleSearchAutocompleteComponent,
  SimpleHtmlEditorComponent,
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    // ChartsModule,
    // IonRangeSliderModule,
    // NgxChartsModule,
    ClipboardModule,
    PipesModule,
    RouterModule,
    StoreModule.forFeature('search', fromSearch.reducers),
    EffectsModule.forFeature([QueryEffects, FormEffects]),
    TranslateModule.forChild(),
    LocalizeRouterModule,
    FormsModule,
    ToastrModule,
    NgSelectModule,
    DynamicModule,
    // ToolModule,
  ],
  declarations: SEARCH_COMPONENTS,
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SearchRequestInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: DetailedRequestInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BasketRequestInterceptor,
      multi: true,
    },
  ],
  exports: SEARCH_COMPONENTS,
})
export class SearchModule {}
