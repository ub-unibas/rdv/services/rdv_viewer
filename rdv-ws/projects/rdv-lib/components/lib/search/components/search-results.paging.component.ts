/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

/**
 * Provides navigational aides for result list
 */
@Component({
  selector: 'app-search-results-paging',
  template: `
    <div class="search-results-pagination" role="group" aria-label="Pagination">
      <button
        class="fa fa-arrow-left"
        (click)="goToPage(currentPage - 1)"
        [disabled]="currentPage === 1"
      ></button>

      <div *ngFor="let pn of visiblePageList">
        <button class="page-button" [disabled]="currentPage === pn" (click)="goToPage(pn)">
          {{ pn }}
        </button>
      </div>

      <button
        class="fa fa-arrow-right"
        (click)="goToPage(currentPage + 1)"
        [disabled]="currentPage === numberOfPages"
      ></button>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchResultsPagingComponent {
  @Input() currentOffset: number;
  @Input() rowsPerPage: number;
  @Input() numberOfRows: number;
  @Input() pageButtonNr: number;
  // eslint-disable-next-line @angular-eslint/no-output-rename
  @Output('offset') offsetEmitter = new EventEmitter<number>();

  get numberOfPages(): number {
    return Math.ceil(this.numberOfRows / this.rowsPerPage);
  }

  get currentPage(): number {
    return this.currentOffset % this.rowsPerPage > 1
      ? Math.ceil(this.currentOffset / this.rowsPerPage) + 1
      : Math.floor(this.currentOffset / this.rowsPerPage) + 1;
  }

  goToPage(pnr: number) {
    const newPage = Math.min(this.numberOfPages, Math.max(1, pnr));
    const newOffset = this.rowsPerPage * (newPage - 1);
    this.offsetEmitter.emit(newOffset);
  }

  get visiblePageList(): number[] {
    const halfPageButtonNr = Math.floor(this.pageButtonNr / 2);
    let firstPage = this.currentPage - halfPageButtonNr;
    let lastPage = this.currentPage + halfPageButtonNr;
    if (firstPage < 1) {
      lastPage += 1 - firstPage;
      firstPage = 1;
    }
    if (lastPage > this.numberOfPages) {
      firstPage = Math.max(1, firstPage - (lastPage - this.numberOfPages));
      lastPage = this.numberOfPages;
    }
    const pages: number[] = [];
    for (let i = firstPage; i <= lastPage; i++) {
      pages.push(i);
    }
    return pages;
  }
}
