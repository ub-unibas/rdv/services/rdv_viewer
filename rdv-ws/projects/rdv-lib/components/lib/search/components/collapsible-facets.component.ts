/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { BehaviorSubject, Observable, combineLatest } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { environment } from '../../shared/Environment';
import { FacetFieldType, FacetFieldsModel, HistogramFieldModel, HistogramFieldType } from '../../shared/models/settings.model';
import { DisplayConstraintService } from '../../shared/services/display-constraint-checker.service';
import * as fromFormActions from '../actions/form.actions';
import * as fromQueryActions from '../actions/query.actions';
import * as fromSearch from '../reducers/index';

interface Group {
  configuredGroup: boolean; // if not: ad hoc group with single member
  labelI18nKey: string;
  members: string[];
  minMemberOrder: number;
}

interface OpenGroupLookup {
  [key: string]: boolean;
}

/**
 * Displays collapsible facets.
 */
@Component({
  selector: 'app-collapsible-facets',
  template: `
    <ng-template #renderFacetTemplate let-key="key">
      <ng-container [ngSwitch]="facetFieldsConfig[key].facetType">
        <app-collapsible-facet
          *ngSwitchCase="simpleType()"
          [open]="facetOpen(openFacet$ | async, key)"
          [key]="key"
          [title]="facetFieldsConfig[key].label | translate"
          (openFacetEmitter)="switchFacetOpenState($event)"
        ></app-collapsible-facet>
        <ng-container *ngSwitchCase="checkboxType()">
          <app-collapsible
            [title]="facetFieldsConfig[key].label | translate"
            [open]="facetOpen(openFacet$ | async, key)"
            [key]="key"
            (changed)="openFacetStateChanged({ key: key, open: $event })"
          >
            <app-checkbox-facet [key]="key"></app-checkbox-facet>
          </app-collapsible>
        </ng-container>
        <ng-container *ngSwitchCase="subcategoryType()">
          <app-collapsible
            [title]="facetFieldsConfig[key].label | translate"
            [open]="facetOpen(openFacet$ | async, key)"
            [key]="key"
            (changed)="openFacetStateChanged({ key: key, open: $event })"
          >
            <app-subcat-facet [key]="key"></app-subcat-facet>
          </app-collapsible>
        </ng-container>
        <ng-container *ngSwitchCase="histogramType()">
          <ng-container [ngSwitch]="facetFieldsConfig[key].data_type">
            <app-collapsible
              *ngSwitchCase="histogramIntType()"
              [open]="facetOpen(openFacet$ | async, key)"
              [title]="facetFieldsConfig[key].label | translate"
              [key]="key"
              (changed)="openFacetStateChanged({ key: key, open: $event })"
            >
              <app-int-histogram-facet *ngIf="!facetFieldsConfig[key].showAggs" [key]="key"></app-int-histogram-facet>
              <app-int-hierarchic-facet *ngIf="facetFieldsConfig[key].showAggs" [key]="key"></app-int-hierarchic-facet>
            </app-collapsible>
            <app-collapsible
              *ngSwitchCase="histogramDateType()"
              [open]="facetOpen(openFacet$ | async, key)"
              [title]="facetFieldsConfig[key].label | translate"
              [key]="key"
              (changed)="openFacetStateChanged({ key: key, open: $event })"
            >
              <app-date-histogram-facet *ngIf="!facetFieldsConfig[key].showAggs" [key]="key"></app-date-histogram-facet>
              <app-date-hierarchic-facet *ngIf="facetFieldsConfig[key].showAggs" [key]="key"></app-date-hierarchic-facet>
            </app-collapsible>
            <app-collapsible
              *ngSwitchCase="histogramGeoType()"
              [open]="facetOpen(openFacet$ | async, key)"
              [title]="facetFieldsConfig[key].label | translate"
              [key]="key"
              (changed)="openFacetStateChanged({ key: key, open: $event })"
            >
              <app-geo-histogram-facet *ngIf="!facetFieldsConfig[key].showAggs" [key]="key"></app-geo-histogram-facet>
              <app-geo-hierarchic-facet *ngIf="facetFieldsConfig[key].showAggs" [key]="key"></app-geo-hierarchic-facet>
            </app-collapsible>
          </ng-container>
        </ng-container>
        <app-collapsible
          *ngSwitchCase="hierarchicType()"
          [open]="facetOpen(openFacet$ | async, key)"
          [title]="facetFieldsConfig[key].label | translate"
          [key]="key"
          (changed)="openFacetStateChanged({ key: key, open: $event })"
        >
          <app-hierarchic-facet [key]="key"></app-hierarchic-facet>
        </app-collapsible>
      </ng-container>
    </ng-template>
    <div class="{{ facetClasses(showFacets) }}">
      <div class="collapsible-facets__container-overlay"></div>
      <div class="collapsible-facets__container-elements">
        <div class="d-block d-sm-none">
          <button class="btn collapsible-facets__use-filter link-black" (click)="closeFacet()">
            {{ 'collapsible-facets.use_filter' | translate }}
          </button>
        </div>
        <div *ngIf="showFacets" class="collapsible-facets__available" [class.collapsible-facets__available--toggable]="forceToggled">
          <ng-content></ng-content>
          <div class="collapsible-facets__top-margin">
            <h4 class="search-results__title d-sm-none">{{ 'search-results.title' | translate : { value: (searchCount$ | async) } }}</h4>
          </div>
          <ng-container
            *ngFor="
              let group of filterCountKeys(selectedFacetValueByKey$ | async, facetFieldCountByKey$ | async) as groupItems;
              last as isLast;
              index as currentIndex;
              trackBy: trackGroup
            "
          >
            <ng-container *ngIf="isSingleMemberGroup(group) && showSingleBoolFacet(facetFieldsConfig[key(group)].facetType)">
              <div
                [class.active]="facetFieldsConfig[key(group)].order === 1"
                [class.last]="isLast"
                [class.prev-group]="preceededByGroup(currentIndex, groupItems)"
                class="collapsible-facets"
                role="listitem"
              >
                <ng-container [ngTemplateOutlet]="renderFacetTemplate" [ngTemplateOutletContext]="{ key: group.members[0] }"></ng-container>
              </div>
            </ng-container>
            <ng-container *ngIf="!isSingleMemberGroup(group)">
              <div
                [class.active]="facetFieldsConfig[key(group)].order === 1"
                [class.last]="isLast"
                [class.next-no-group]="followedByNonGroup(currentIndex, groupItems)"
                class="collapsible-facets collapsible-facet-group"
                role="listitem"
              >
                <app-collapsible
                  [title]="group.labelI18nKey | translate"
                  [open]="groupOpen(openFacet$ | async, group)"
                  [key]="group.labelI18nKey"
                  [closeOnBlur]="false"
                  (changed)="switchFacetGroupOpenState(group.members, { key: group.labelI18nKey, open: $event })"
                >
                  <ng-container *ngFor="let key of group.members; trackBy: trackGroupMember">
                    <ng-container [ngTemplateOutlet]="renderFacetTemplate" [ngTemplateOutletContext]="{ key: key }"></ng-container>
                  </ng-container>
                </app-collapsible>
              </div>
            </ng-container>
          </ng-container>
        </div>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollapsibleFacetsComponent implements OnDestroy {
  @Input() forceToggled: boolean;
  @Input() showFacets: boolean;
  @Output() closeFacetEmitter = new EventEmitter<true>();

  facetFieldsConfig: any;

  selectedFacetValueByKey$: Observable<any>;
  facetFieldCountByKey$: Observable<any>;
  inFacetSearch$: Observable<any>;
  openFacet$: Observable<any>;
  searchCount$: Observable<number>;

  openGroups$: BehaviorSubject<OpenGroupLookup>;

  constructor(protected searchStore: Store<fromSearch.State>, protected displayConstraintService: DisplayConstraintService) {
    this.facetFieldsConfig = environment.facetFields;
    this.openGroups$ = new BehaviorSubject<OpenGroupLookup>({});

    this.selectedFacetValueByKey$ = combineLatest([
      searchStore.select(fromSearch.getFacetValues),
      searchStore.select(fromSearch.getHistogramValues),
      searchStore.select(fromSearch.getHierarchyValues),
    ]).pipe(
      map(([facets, histograms, hierarchies]) => (key: string) => {
        return hierarchies[key] ? hierarchies[key].values : histograms[key] ? histograms[key].values : facets[key].values;
      })
    );

    this.facetFieldCountByKey$ = combineLatest([
      searchStore.select(fromSearch.getFacetFieldCountByKey),
      searchStore.select(fromSearch.getFacetHistogramCountByKey),
      searchStore.select(fromSearch.getHierarchicFacetCountByKey),
    ]).pipe(
      map(([facets, histograms, hierarchies]) => (field: string) => {
        let v = hierarchies(field);
        if (v) {
          return v;
        }
        v = histograms(field);
        return v ? v : facets(field);
      })
    );

    this.inFacetSearch$ = searchStore.select(fromSearch.getAllInFacetSearchValues);
    this.openFacet$ = searchStore.select(fromSearch.getAllOpenFacet);
    this.searchCount$ = searchStore.pipe(select(fromSearch.getTotalResultsCount));
  }

  showSingleBoolFacet(type: string): boolean {
    if (type !== FacetFieldType.BOOLEAN) {
      return true;
    }
    // bool facet
    return environment.hideBoolFacetsInList !== true;
  }

  followedByNonGroup(currentIndex: number, groups: Group[]): boolean {
    if (currentIndex + 1 >= groups.length) {
      return true;
    }
    return this.isSingleMemberGroup(groups[currentIndex + 1]);
  }

  preceededByGroup(currentIndex: number, groups: Group[]): boolean {
    if (currentIndex === 0) {
      return false;
    }
    return !this.isSingleMemberGroup(groups[currentIndex - 1]);
  }

  public trackGroup(index, item: Group) {
    return item.labelI18nKey;
  }

  public trackGroupMember(index, item: string) {
    return item;
  }

  protected getOpenFacets(): string[] {
    let openFacets;
    this.openFacet$.pipe(take(1)).subscribe((v) => (openFacets = Object.keys(v)));
    if (!openFacets) {
      openFacets = [];
    }
    return openFacets;
  }

  filterCountKeys(selectedFacetValueByKey, facetFieldCountByKey): Group[] {
    let inFacetSearchFields;
    this.inFacetSearch$.pipe(take(1)).subscribe((v) => (inFacetSearchFields = Object.keys(v)));
    if (!inFacetSearchFields) {
      inFacetSearchFields = [];
    }
    const openFacets = this.getOpenFacets();
    const config: FacetFieldsModel = this.facetFieldsConfig;
    // console.log('---- FILTER', Object.keys(config));
    const filteredKeys = Object.keys(config)
      .filter((key) => {
        const fieldConfig = config[key];
        const canDisplay = this.displayConstraintService.allowed(fieldConfig.displayConstraints);
        // console.log('FIELD', key, canDisplay, fieldConfig);
        if (!canDisplay || fieldConfig.hidden) {
          return false;
        }
        const fieldName = fieldConfig.field;
        // always show int histogram facets
        if (fieldConfig.facetType === FacetFieldType.HISTOGRAM) {
          const histogramFacet = fieldConfig as HistogramFieldModel;
          if (histogramFacet.data_type === HistogramFieldType.INT || histogramFacet.data_type === HistogramFieldType.GEO) {
            // console.log('SHOW HISTO', key);
            return true;
          }
        }
        const values = facetFieldCountByKey(fieldName);
        const selectedFacetValues = selectedFacetValueByKey(key);
        // console.log('VALUES', key, 'values=', values, 'sel=', selectedFacetValues, 'in=', inFacetSearchFields.indexOf(fieldName) >= 0);
        // show facet if either has selected values or unselected values
        // but keep facet even if empty if an in-facet search was done;
        // show also facet if open and has values
        // console.log(
        //   'TAKE?',
        //   key,
        //   selectedFacetValues && selectedFacetValues.length > 0,
        //   values && values.length > 0,
        //   inFacetSearchFields.indexOf(fieldName) >= 0,
        //   openFacets.indexOf(fieldName) >= 0 && values && values.length > 0
        // );
        const useKey =
          (selectedFacetValues && selectedFacetValues.length > 0) ||
          (values && values.length > 0) ||
          inFacetSearchFields.indexOf(fieldName) >= 0 ||
          (openFacets.indexOf(fieldName) >= 0 && values && values.length > 0);
        // console.log('TAKE2?', !!useKey);
        return !!useKey;
      })
      .sort((a, b) => config[a].order - config[b].order);
    const groupMap: { [key: string]: Group } = {};
    filteredKeys.forEach((k) => {
      const groupKey = config[k].facetGroup ?? k;
      const adHocGroup = groupKey === k;
      if (!groupMap[groupKey]) {
        groupMap[groupKey] = { labelI18nKey: groupKey, members: [], minMemberOrder: config[k].order, configuredGroup: !adHocGroup };
      }
      groupMap[groupKey].members.push(k);
      groupMap[groupKey].minMemberOrder = Math.min(groupMap[groupKey].minMemberOrder, config[k].order);
    });
    const orderedGroups = Object.keys(groupMap)
      .map((k) => groupMap[k])
      .sort((a, b) => a.minMemberOrder - b.minMemberOrder);
    return orderedGroups;
  }

  public simpleType(): string {
    return FacetFieldType.SIMPLE.toString();
  }

  public checkboxType(): string {
    return FacetFieldType.CHECKBOX.toString();
  }

  public subcategoryType(): string {
    return FacetFieldType.SUBCATEGORY.toString();
  }

  public histogramType(): string {
    return FacetFieldType.HISTOGRAM.toString();
  }

  public histogramIntType(): string {
    return HistogramFieldType.INT.toString();
  }

  public histogramDateType(): string {
    return HistogramFieldType.DATE.toString();
  }

  public histogramGeoType(): string {
    return HistogramFieldType.GEO.toString();
  }

  public hierarchicType(): string {
    return FacetFieldType.HIERARCHIC.toString();
  }

  public key(group: Group): string {
    return group.members[0];
  }

  public isSingleMemberGroup(group: Group): boolean {
    return !group.configuredGroup;
  }

  facetClasses(show: boolean): string {
    const showClass = 'collapsible-facets--show-in-overlay';
    let cssClasses = 'collapsible-facets__container';
    if (show) {
      cssClasses += ' ' + showClass;
      document.documentElement.classList.add(showClass);
      document.body.classList.add(showClass);
    } else {
      document.documentElement.classList.remove(showClass);
      document.body.classList.remove(showClass);
    }
    return cssClasses;
  }

  closeFacet() {
    this.closeFacetEmitter.emit(true);
  }

  facetOpen(openFacet, key: string): boolean {
    return this.facetFieldsConfig[key].field in openFacet;
  }

  groupOpen(openFacet, group: Group): boolean {
    const groupLookup = this.getCurrentOpenGroupState();
    if (groupLookup[group.labelI18nKey]) {
      return true;
    }
    for (const key of group.members) {
      if (this.facetOpen(openFacet, key)) {
        return true;
      }
    }
    return false;
  }

  switchFacetOpenState({ key, open }) {
    this.searchStore.dispatch(new fromFormActions.SetFacetOpenedInUi({ field: this.facetFieldsConfig[key].field, opened: open }));
    if (open) {
      // load missing facets
      this.searchStore.dispatch(new fromQueryActions.SimpleSearch());
    }
  }

  protected getCurrentOpenGroupState(): OpenGroupLookup {
    let currentGroupState: OpenGroupLookup;
    this.openGroups$.pipe(take(1)).subscribe((v) => (currentGroupState = v));
    if (!currentGroupState) {
      currentGroupState = {};
    }
    return currentGroupState;
  }

  switchFacetGroupOpenState(memberFacetKeys: string[], { key, open }) {
    if (!open) {
      // close all members too
      const openFacets = this.getOpenFacets();
      memberFacetKeys.forEach((k) => {
        if (openFacets.indexOf(this.facetFieldsConfig[k].field) >= 0) {
          this.switchFacetOpenState({ key: k, open: false });
        }
      });
    }
    this.openGroups$.next({ ...this.getCurrentOpenGroupState(), [key]: open });
  }

  openFacetStateChanged(facetInfo) {
    this.switchFacetOpenState(facetInfo);
    if (!facetInfo.open) {
      // undefined == remove, because empty search "" is possible too!
      this.searchStore.dispatch(new fromFormActions.SetInFacetSearch(this.facetFieldsConfig[facetInfo.key].field, undefined));
    }
  }

  ngOnDestroy(): void {
    this.facetClasses(false);
  }
}
