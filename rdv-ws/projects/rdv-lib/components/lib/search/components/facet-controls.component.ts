/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { DateFormatService } from '../../shared/services/date-format.service';
import { ResetUtilService } from '../../shared/services/reset-util.service';
import * as fromSearch from '../reducers/index';
import { EmittedValues } from './abstract-histogram-facet-component.directive';

/**
 * Displays a row in the result or  list
 */
@Component({
  selector: 'app-facet-controls',
  template: `
    <div class="FacetControlLayout">
      <app-operator-selector
        [key]="key"
        (changed)="changedHandler()"
        [facetValueSelector]="fromSearch.getHierarchyValues"
      ></app-operator-selector>
      <app-facet-order-selector
        [key]="key"
        (changed)="changedHandler()"
        [facetValueSelector]="fromSearch.getHierarchyValues"
      ></app-facet-order-selector>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [
    `
      app-operator-selector,
      app-facet-order-selector {
        display: contents;
      }
      .FacetControlLayout {
        display: grid;
        grid-template-columns: auto auto;
        justify-items: start;
        justify-content: start;
        gap: 0.5rem 0.75rem;
      }
      /* below lg */
      @media (max-width: 991px) {
        .FacetControlLayout {
          grid-template-columns: auto;
        }
      }
    `,
  ],
})
export class FacetControlsComponent {
  @Input() key: string;
  @Output() changed = new EventEmitter<EmittedValues>(false);

  constructor(
    protected translate: TranslateService,
    protected dateFormatService: DateFormatService,
    protected searchStore: Store<fromSearch.State>,
    protected resetUtilService: ResetUtilService
  ) {}

  changedHandler(): void {
    this.changed.emit();
  }

  get fromSearch() {
    return fromSearch;
  }
}
