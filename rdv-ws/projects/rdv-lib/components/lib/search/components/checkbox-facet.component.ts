/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { TranslateService } from "@ngx-translate/core";
import { environment } from '../../shared/Environment';
import * as fromSearch from '../reducers/index';
import { SimpleFacetComponent } from './simple-facet.component';

/**
 * Displays checkbox facet. A variant of a simple facet.
 */
@Component({
  selector: 'app-checkbox-facet',
  template: `
    <ng-template #facetValueTemplate let-key="key" let-value="value" let-selected="selected">
      <button (click)="facetAction(key, value, selected)" type="button" class="facet__entry facet__entry--checkbox link-black">
        <span [className]="'facet-checkbox' + (selected ? ' facet-checkbox--selected' : '')"></span>
        <span *ngIf="!label"
          ><span class="facet__name">{{ value.label }}</span> <span class="facet__count"> ({{ value.count }})</span></span
        >
        <span *ngIf="label">{{label}}</span>
      </button>
    </ng-template>
    <div class="facet">
      <ul class="facet__list">
        <li *ngIf="loading" class="rotating"></li>
        <app-facet-expandable
          [key]="key"
          [itemTemplate]="facetValueTemplate"
          [itemsPerExpansion]="facetFieldsConfig[key].expandAmount"
          [items]="buildTemplateItems(facetFieldByKey$ | async, facetFieldCountByKey$ | async)"
        ></app-facet-expandable>
      </ul>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CheckboxFacetComponent extends SimpleFacetComponent {
  @Input() label?: string;

  constructor(protected _searchStore: Store<fromSearch.State>, protected translate: TranslateService) {
    super(_searchStore, translate);
    this.facetFieldByKey$ = _searchStore.pipe(select(fromSearch.getFacetValuesByKey));
    this.facetFieldCountByKey$ = _searchStore.pipe(select(fromSearch.getFacetFieldCountByKey));
    this.inFacetSearch$ = _searchStore.pipe(select(fromSearch.getAllInFacetSearchValues));
    this.facetFieldsConfig = environment.facetFields;
  }

  buildTemplateItems(facetFieldByKey, facetFieldCountByKey) {
    this.loading = false;
    const facet = facetFieldByKey(this.key);
    let values = facetFieldCountByKey(facet.field);
    if (!values) {
      values = [];
    }
    return values.map((v) => ({ key: this.key, value: v, selected: this.isSelected(facet, v.id) }));
  }
}
