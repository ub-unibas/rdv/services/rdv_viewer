/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component} from '@angular/core';

/**
 * Root component for all components with which search parameters can be manipulated
 */
@Component({
  selector: 'app-search-params',
  template: `
      <div class="d-flex flex-column flex-md-row g-0 mt-2">

          <div class="col-md p-2 me-md-2 mb-2 mb-md-0 d-flex flex-column justify-content-between g-0"
               style="border: 1px solid grey; border-radius:3px; margin-right: 20px;">
              <app-fields></app-fields>
              <app-filters></app-filters>
          </div>

          <!-- Facetten/Ranges-Bereich als Tabs (Pills) -->
          <div class="d-flex flex-column p-2 col-md"
               style="border: 1px solid grey; border-radius:3px;">
              <app-visual-search></app-visual-search>
          </div>

      </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchParamsComponent {
}
