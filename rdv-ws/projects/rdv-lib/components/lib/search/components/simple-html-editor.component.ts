/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { HtmlEditor } from '@ub-unibas/rdv_viewer-lib/tool';

/**
 * Provides a button to generate an URL containing JSON data in encoded form
 */
@Component({
  selector: 'app-simple-htmleditor',
  template: ` <textarea #stringTextField (change)="setNewHtml(stringTextField.value)"></textarea> `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SimpleHtmlEditorComponent implements HtmlEditor {
  @Input() html: string;
  @Input() settings: any;
  @Output() changedHtml = new EventEmitter<string>();

  setNewHtml(html: string) {
    this.changedHtml.emit(html);
  }
}
