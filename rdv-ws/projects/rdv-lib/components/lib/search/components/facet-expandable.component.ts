/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, take } from 'rxjs';
import { environment } from '../../shared/Environment';
import { FacetFieldsModel } from '../../shared/models/settings.model';
import * as fromFormActions from '../actions/form.actions';
import * as fromSearch from '../reducers/index';

/**
 * Displays collapsible container.
 */
@Component({
  selector: 'app-facet-expandable',
  template: `
    <ng-container *ngFor="let data of currentList((pageSize$ | async)(facetFieldsConfig[key].field))">
      <li>
        <ng-container [ngTemplateOutlet]="itemTemplate" [ngTemplateOutletContext]="data"></ng-container>
      </li>
    </ng-container>
    <li class="expandable__more" *ngIf="canShowMore((pageSize$ | async)(facetFieldsConfig[key].field))">
      <button type="button" class="expandable__title link-black" (mousedown)="expand($event)">
        {{ 'expandable.expand' | translate }}
      </button>
    </li>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FacetExpandableComponent {
  @Input() key: string;
  @Input() itemTemplate;
  @Input() itemsPerExpansion: number;
  @Input() items: [];

  pageSize$: Observable<any>;
  facetFieldsConfig: FacetFieldsModel;

  constructor(protected searchStore: Store<fromSearch.State>) {
    this.facetFieldsConfig = environment.facetFields;
    this.pageSize$ = this.searchStore.select(fromSearch.getFacetPageSize);
  }

  protected getPageSize(): number {
    let at: number;
    this.pageSize$.pipe(take(1)).subscribe((v) => (at = v(this.facetFieldsConfig[this.key].field)));
    if (at === null || at === undefined) {
      at = 0;
    }
    return at;
  }

  public expand(event) {
    event.preventDefault();
    const at = this.getPageSize() + this.itemsPerExpansion;
    this.searchStore.dispatch(new fromFormActions.ShowMoreFacetValuesInUi({ field: this.facetFieldsConfig[this.key].field, amount: at }));
  }

  currentList(at: number) {
    return this.items.slice(0, Math.min(this.items.length, at + this.itemsPerExpansion));
  }

  canShowMore(at: number) {
    return at + this.itemsPerExpansion < this.items.length;
  }
}
