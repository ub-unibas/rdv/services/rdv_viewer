/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {Action} from '@ngrx/store';

/***
 * @ignore
 */
/* eslint-disable @typescript-eslint/no-shadow */
export enum LayoutActionTypes {
  ShowFacetOrRange = '[Layout] Show Facet or Range',
  ToggleMapDisplay = '[Layout] Toggle display of map in search',
  ToggleTimelineDisplay = '[Layout] Toggle display of timeline in search',
}
/* eslint-enable @typescript-eslint/no-shadow */

/**
 * Display another range or field facet
 */
export class ShowFacetOrRange implements Action {
  /**
   * @ignore
   */
  readonly type = LayoutActionTypes.ShowFacetOrRange;

  /**
   * Contains id of new range or field facet
   * @param payload {string}
   */
  constructor(public payload: string) {
  }
}

/**
 * Display another range or field facet
 */
export class ToggleMapDisplay implements Action {
  /**
   * @ignore
   */
  readonly type = LayoutActionTypes.ToggleMapDisplay;
}

/**
 * Display another range or field facet
 */
export class ToggleTimelineDisplay implements Action {
  /**
   * @ignore
   */
  readonly type = LayoutActionTypes.ToggleTimelineDisplay;
}

/**
 * @ignore
 */
export type LayoutActions =
  ShowFacetOrRange
  | ToggleMapDisplay
  | ToggleTimelineDisplay;
