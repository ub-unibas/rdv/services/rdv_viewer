/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {Action} from '@ngrx/store';
import {DetailedResult} from '../models/detailed-result.model';

/***
 * @ignore
 */
/* eslint-disable @typescript-eslint/no-shadow */
export enum DetailedResultActionTypes {
  AddDetailedResult = '[DetailedResult] Add DetailedResult',
  DeleteDetailedResult = '[DetailedResult] Delete DetailedResult',
  ClearDetailedResults = '[DetailedResult] Clear DetailedResults',
}
/* eslint-enable @typescript-eslint/no-shadow */

/**
 * Adds detailed information of a record (i.e. detailed information is shown on UI)
 */
export class AddDetailedResult implements Action {
  /**
   * @ignore
   */
  readonly type = DetailedResultActionTypes.AddDetailedResult;

  /**
   * Contains detailed result information
   * @param payload {Object}
   */
  constructor(public payload: { detailedResult: DetailedResult }) {
  }
}

/**
 * Removes detailed information of a record (i.e. detailed information disappears on UI)
 */
export class DeleteDetailedResult implements Action {
  /**
   * @ignore
   */
  readonly type = DetailedResultActionTypes.DeleteDetailedResult;

  /**
   * Contains id of result of which detailed information is to be removed
   * @param payload {Object}
   */
  constructor(public payload: { id: string }) {
  }
}

/**
 * Removes detailed information of all available records
 */
export class ClearDetailedResults implements Action {
  /**
   * @ignore
   */
  readonly type = DetailedResultActionTypes.ClearDetailedResults;
}

/**
 * @ignore
 */
export type DetailedResultActions =
  | AddDetailedResult
  | DeleteDetailedResult
  | ClearDetailedResults;
