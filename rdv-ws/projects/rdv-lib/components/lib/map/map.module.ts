import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { LocalizeRouterModule } from '@gilsdav/ngx-translate-router';
import { StoreModule } from '@ngrx/store';
import { TranslateModule } from '@ngx-translate/core';
import { ToolModule, reducers as mapReducers } from '@ub-unibas/rdv_viewer-lib/tool';
import { ToastrModule } from 'ngx-toastr';
import {
  AbstractHistogramFacetComponentDirective,
  SetGeoClusterPrecision,
  SearchStateInterface,
  SettingsModel,
  buildId,
  RemoveAllHistogramBoundary,
  AddHistogramBoundaries,
  SetOffset,
  CoreSettings,
  RemoveAllFacetValue,
  AddFacetValue,
  getGeoClusterPrecision,
  Doc,
  getHistogramValues,
  getAllGeoResults,
  getFacetHistogramCount,
  GeoResult,
  PipesModule,
} from '@rdv-lib';
import { MapBoxComponent, MapLimits, MapboxEventHandler, MapboxImages, MapBoxModule } from '@mapbox';
import { MapComponent } from './map.component';
import { CountriesModule } from './countries/countries.module';
import { CoreModule } from '../core/core.module';
import { CollapsibleFacetsComponent } from '../search/components/collapsible-facets.component';
import { SearchModule } from '../search/search.module';

const MAP_PAGE_COMPONENTS = [MapComponent];

@NgModule({
  imports: [
    MapBoxModule,
    StoreModule.forFeature('map', mapReducers),
    CommonModule,
    SearchModule,
    TranslateModule.forChild(),
    FormsModule,
    ToastrModule,
    CoreModule,
    ToolModule,
    CountriesModule,
  ],
  declarations: [MapComponent],
  providers: [],
  exports: MAP_PAGE_COMPONENTS,
})
export class MapModule {}
