import { AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as mapboxgl from 'mapbox-gl';
import { MapBoxComponent, MapLimits, MapboxEventHandler, MapboxImages } from '@mapbox';
import { BehaviorSubject, Observable, Subject, Subscription, filter, take } from 'rxjs';
import { environment } from '../shared/Environment';
import { TranslateService } from '@ngx-translate/core';
import {
  AbstractHistogramFacetComponentDirective,
  SetGeoClusterPrecision,
  SearchStateInterface,
  SettingsModel,
  buildId,
  RemoveAllHistogramBoundary,
  AddHistogramBoundaries,
  SetOffset,
  SimpleSearch,
  CoreSettings,
  RemoveAllFacetValue,
  getTotalResultsCount,
  AddFacetValue,
  getGeoClusterPrecision,
  Doc,
  getHistogramValues,
  getAllGeoResults,
  getFacetHistogramCount,
  GeoResult,
  I18nToastrService,
} from '@rdv-lib';
import * as fromCountries from './countries/reducers/countries.reducer';
import { MapConfig, RenderHint, geoSnippetsToGeoJson, GeoPoint, geoFacetToGeoJson, GeoBucket } from '@ub-unibas/rdv_viewer-lib/tool';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';
import { getMapZoom, getMapCenter, precisionForZoom, SetCenter, SetZoom, GeoLocation, ZoomToPrecision } from '@tool';
import { Router } from '@angular/router';
import GeoHash from 'latlon-geohash';
import { Geometry, Point } from 'geojson';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Country, LoadCountries } from './countries/actions/countries.actions';
import { getCountriesState } from './countries/reducers';
import { RangeSliderComponent, Tick, ThumbData } from './range-slider/range-slider.component';

const root = document.querySelector(':root') as HTMLElement;

// 100vh mobile fix: sets css variable --vh100
const vh100Fix = () => {
  root.style.setProperty('--vh100', window.innerHeight + 'px');
  root.style.setProperty('--vw100', window.innerWidth + 'px');
  root.style.setProperty('--viw', Math.min(1680, window.innerWidth) + '');
};
window.addEventListener('resize', vh100Fix);
vh100Fix();

const defaultTimeSliderTicks: Tick[] = [
  { label: '', value: -25, hideTick: true },
  { label: '100', value: 100 },
  { label: '500', value: 500 },
  { label: '1000', value: 1000 },
  { label: '1500', value: 1500 },
  { label: '2000', value: 2000 },
  { label: '', value: 2200, hideTick: true },
];
const MIN_YEAR = 100;

const images: MapboxImages[] = [
  { url: '/assets/img/map-land.png', id: 'country' },
  { url: '/assets/img/map-geo-marker.png', id: 'point' },
  { url: '/assets/img/red-cross.png', id: 'flag' },
];

@Component({
  selector: 'app-map',
  template: `
    <div class="page-map">
      <app-mapbox
        #mapBox
        [mapConfig]="initialArea()"
        (mapChanged)="updateMap($event)"
        [images]="mapboxImages()"
        [layers]="mapboxLayers()"
        [eventHandlers]="mapboxEventHandlers()"
      ></app-mapbox>
      <div class="page-map__time-slider">
        <app-range-slider
          #timeSlider
          [ticks]="timeSliderTicks"
          (leftThumb)="setYear($event, startYear$)"
          (rightThumb)="setYear($event, endYear$)"
          (doSearch)="switchToSearch(true)"
          [initialThumbs]="initYearSlider()"
        ></app-range-slider>
        <div class="page-map__time-inputs">
          <span class="page-map__time-spacer-right">{{ 'min-max-date-input.from' | translate }}</span>
          <input
            class="page-map__time-input page-map__time-spacer-right"
            #startYear
            type="number"
            [min]="defaultStartYear"
            value="{{ startYear$ | async }}"
            (keyup)="closedInput($event)"
          />
          <span class="page-map__time-spacer-right">{{ 'min-max-date-input.to' | translate }}</span>
          <input
            class="page-map__time-input page-map__time-spacer-right"
            #endYear
            type="number"
            [min]="defaultStartYear"
            value="{{ endYear$ | async }}"
            (keyup)="closedInput($event)"
          />
          <button class="page-map__time-submit" (click)="selectTimeRange(startYear.value, endYear.value)">
            {{ 'map-page.OK' | translate }}
          </button>
        </div>
      </div>
      <div class="map-sidebar" [class.map-sidebar__close]="(showSidebar$ | async) === false">
        <div class="map-sidebar__header">
          <button class="map-sidebar__header__button" (click)="toggleSidebar()"></button>
          <div class="map-sidebar__header__buttons">
            <button (click)="goSearchPage()" type="button" class=" map-sidebar__header__switch map-sidebar__header__list_active"></button>
            <button disabled type="button" class="map-sidebar__header__switch map-sidebar__header__map_active"></button>
          </div>
        </div>
        <app-simple-search *ngIf="env.showSimpleSearch !== false" [showBoolFacets]="true"></app-simple-search>

        <h4 class="search-results__title">{{ 'search-results.title' | translate : { value: (searchCount$ | async) } }}</h4>

        <app-collapsible-facets
          (closeFacetEmitter)="toggleFacets()"
          [forceToggled]="smallFacet$ | async"
          [showFacets]="showFacets$ | async"
        >
        </app-collapsible-facets>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [Location, { provide: LocationStrategy, useClass: PathLocationStrategy }],
})
export class MapComponent implements OnDestroy, AfterViewInit, OnInit {
  @ViewChild('mapBox') mapBox: MapBoxComponent;
  @ViewChild('precision') precisionRef: ElementRef<HTMLSelectElement>;
  @ViewChild('timeSlider') timeSlider: RangeSliderComponent;

  protected mapZoom$: Observable<number>;
  protected mapCenter$: Observable<GeoLocation>;
  protected precision$: Observable<number>;
  protected showPreview$: BehaviorSubject<Doc | null>;
  protected yearFacetSubscription: Subscription;
  protected startYear$: BehaviorSubject<number | undefined>;
  protected endYear$: BehaviorSubject<number | undefined>;
  protected geoPointUpdateTimer: any;
  protected useCentroid$: BehaviorSubject<RenderHint[]>;
  protected facetFieldByKey$: Observable<any>;
  protected lastBuckets: GeoJSON.FeatureCollection<Geometry>;
  protected lastPoints: GeoJSON.FeatureCollection<Geometry>;
  protected lastMarkers: GeoJSON.FeatureCollection<Geometry>;
  protected lastCountries: GeoJSON.FeatureCollection<Geometry>;
  protected docs$: Observable<GeoResult[]>;
  protected docsSubscription: Subscription;
  protected clusterSubscription: Subscription;
  protected countries$: Observable<fromCountries.State>;
  protected countriesSubscription: Subscription;
  protected showFacetsSubject: Subject<boolean>;
  smallFacet$: Observable<boolean>;
  smallFacetSubject: Subject<boolean>;
  showFacets$: Observable<boolean>;
  searchCount$: Observable<number>;
  showSidebarSubject: Subject<boolean>;
  showSidebar$: Observable<boolean>;

  constructor(
    protected translate: TranslateService,
    protected toastr: I18nToastrService,
    protected searchStore: Store<SearchStateInterface>,
    protected router: Router,
    protected localize: LocalizeRouterService
  ) {
    this.useCentroid$ = new BehaviorSubject<RenderHint[]>(environment.mapConfig.positionHint);
    environment.mapConfig.credentials = (window as any).MAPBOX_TOKEN;
    if ((window as any).MAPBOX_STYLE) {
      environment.mapConfig.config.style = (window as any).MAPBOX_STYLE;
    }
    this.mapZoom$ = searchStore.pipe(select(getMapZoom));
    this.mapCenter$ = searchStore.pipe(select(getMapCenter));

    this.precision$ = searchStore.pipe(select(getGeoClusterPrecision));
    this.startYear$ = new BehaviorSubject<number | undefined>(undefined);
    this.endYear$ = new BehaviorSubject<number | undefined>(undefined);
    this.facetFieldByKey$ = searchStore.pipe(
      select(getFacetHistogramCount),
      filter((x) => typeof x === 'object' && x !== null)
    );
    this.lastBuckets = { type: 'FeatureCollection', features: [] };
    this.lastPoints = { type: 'FeatureCollection', features: [] };
    this.lastMarkers = { type: 'FeatureCollection', features: [] };
    this.lastCountries = { type: 'FeatureCollection', features: [] };
    this.searchCount$ = searchStore.pipe(select(getTotalResultsCount));
    this.clusterSubscription = this.facetFieldByKey$.subscribe((clusterFacets) => {
      if (this.map) {
        this.processGeoClusters(clusterFacets);
        this.setGeoPoints();
      }
    });

    this.countries$ = searchStore.pipe(select(getCountriesState));
    this.countriesSubscription = this.countries$.subscribe((countries) => {
      this.lastCountries = this.convertCountries(countries.de);
    });
    this.docs$ = searchStore.pipe(select(getAllGeoResults));
    this.docsSubscription = this.docs$.subscribe((docs) => {
      if (this.map) {
        this.processGeoPoints(docs);
        this.setGeoPoints();
      }
    });
    this.showPreview$ = new BehaviorSubject<Doc | null>(null);
    this.showFacetsSubject = new BehaviorSubject<boolean>(true);
    this.showFacets$ = this.showFacetsSubject.asObservable();
    this.smallFacetSubject = new BehaviorSubject<boolean>(false);
    this.smallFacet$ = this.smallFacetSubject.asObservable();
    this.showSidebarSubject = new BehaviorSubject<boolean>(true);
    this.showSidebar$ = this.showSidebarSubject.asObservable();
  }

  initYearSlider(): ThumbData {
    return { left: this.defaultStartYear, right: this.defaultEndYear };
  }

  ngOnDestroy(): void {
    if (this.docsSubscription) {
      this.docsSubscription.unsubscribe();
    }
    /*
    this.facetDisplayHelper.close();
    if (this.languageChangeSubscription) {
      this.languageChangeSubscription.unsubscribe();
    }
*/
    if (this.yearFacetSubscription) {
      this.yearFacetSubscription.unsubscribe();
    }
    if (this.clusterSubscription) {
      this.clusterSubscription.unsubscribe();
    }
  }
  ngAfterViewInit(): void {
    this.yearFacetSubscription = this.searchStore.select(getHistogramValues).subscribe((histogramFacets) => {
      const yearValue = this.decodeFacetValue(histogramFacets[environment.mapConfig.yearFacetKey]);

      let startYear: number | undefined;
      let endYear: number | undefined;
      if (yearValue) {
        const { gte: yearGte, lte: yearLte } = yearValue;
        startYear = yearGte;
        endYear = yearLte;
      }
      this.timeSlider.setLeftThumb(startYear);
      this.timeSlider.setRightThumb(endYear);
      this.startYear$.next(Number(startYear));
      this.endYear$.next(Number(endYear));
    });

    this.searchStore
      .select(getHistogramValues)
      .pipe(take(1))
      .subscribe((histogramFacets) => {
        const cooValue = this.decodeFacetValue(histogramFacets[environment.mapConfig.coordinateFacetKey]);

        // this.facetDisplayHelper.init(this.breakpointObserver);

        if (!cooValue) {
          this.updateCooSearchData();
          this.execSearch();
        } else {
          // has facet coordinate
          let geoCenter: GeoLocation;
          this.searchStore.pipe(take(1), select(getMapCenter)).subscribe((v) => (geoCenter = v));
          // if center is set to the inital value, prefer the facet's value
          if (geoCenter && environment.mapConfig.config.lat === geoCenter.lat && environment.mapConfig.config.lng === geoCenter.lon) {
            const { gte, lte } = cooValue;
            const facetLat = gte.lat + (lte.lat - gte.lat) / 2;
            const facetLon = gte.lon + (lte.lon - gte.lon) / 2;
            this.searchStore.dispatch(new SetCenter({ lat: facetLat, lon: facetLon }));
            this.map.getMap().flyTo({ center: { lat: facetLat, lng: facetLon } });
          }
          this.searchStore
            .select(getAllGeoResults)
            .pipe(take(1))
            .subscribe((docs) => {
              this.processGeoPoints(docs);
            });
          this.facetFieldByKey$.pipe(take(1)).subscribe((clusterFacets) => {
            this.processGeoClusters(clusterFacets);
          });
          this.setGeoPoints();
        }
      });
  }

  ngOnInit(): void {
    if (environment.countryFacetKey) {
      const countryFacetKey = environment.countryFacetKey;
      const facet = environment.facetFields[countryFacetKey];
      this.searchStore.dispatch(new LoadCountries({ facet, language: 'de' }));
    }
  }

  get env(): SettingsModel {
    return environment;
  }

  toggleFacets() {
    let val;
    this.showFacetsSubject.pipe(take(1)).subscribe((v) => (val = v));
    this.showFacetsSubject.next(!val);
  }

  toggleSidebar() {
    this.showSidebarSubject.pipe(take(1)).subscribe((v) => this.showSidebarSubject.next(!v));
  }

  toogleViewMode() {
    this.smallFacet$.pipe(take(1)).subscribe((v) => this.smallFacetSubject.next(!v));
  }

  protected convertCountries(facetCountries: Country[]): GeoJSON.FeatureCollection<GeoJSON.Geometry> {
    const features = [];
    facetCountries.forEach((c) => {
      const { enCountryName, translated } = this.getEnCountryName(c.label);
      const properties: { [key: string]: any } = {
        id: c.label,
        deLabel: c.label,
        enLabel: enCountryName,
        translated,
        facetValue: c.value,
        lng: c.geopoint.lon,
        lat: c.geopoint.lat,
        type: 'country',
      };
      features.push({
        type: 'Feature',
        properties,
        geometry: {
          type: 'Point',
          coordinates: [properties.lng, properties.lat],
        },
      });
    });
    return {
      type: 'FeatureCollection',
      features,
    };
  }
  protected goSearchPage() {
    this.router.navigate([this.localize.translateRoute('/search')]);
  }

  protected processGeoClusters(clusterFacets: { [key: string]: GeoBucket[] }) {
    const key = environment.facetFields[environment.mapConfig.coordinateFacetKey].field;
    const mapBounds = this.map.currentRectangle();
    const minLon = mapBounds.topLeft.lng;
    const maxLon = mapBounds.bottomRight.lng;
    const minLat = mapBounds.bottomRight.lat;
    const maxLat = mapBounds.topLeft.lat;
    let useCentroid;
    this.useCentroid$.pipe(take(1)).subscribe((v) => (useCentroid = v));
    const geoJson = geoFacetToGeoJson(clusterFacets[key], minLon, maxLon, minLat, maxLat, useCentroid);
    this.lastBuckets = geoJson;
  }

  protected processGeoPoints(docs: GeoResult[]) {
    const mapBounds = this.map.currentRectangle();
    const minLon = mapBounds.topLeft.lng;
    const maxLon = mapBounds.bottomRight.lng;
    const minLat = mapBounds.bottomRight.lat;
    const maxLat = mapBounds.topLeft.lat;
    let useCentroid;
    this.useCentroid$.pipe(take(1)).subscribe((v) => (useCentroid = v));
    const geoJson = geoSnippetsToGeoJson(docs as unknown as GeoPoint[], minLon, maxLon, minLat, maxLat, useCentroid);
    this.lastPoints = geoJson;
  }

  protected setGeoPoints() {
    // debounce map updates ...
    if (this.geoPointUpdateTimer) {
      clearTimeout(this.geoPointUpdateTimer);
    }

    this.geoPointUpdateTimer = setTimeout(() => {
      // use latest versions!
      const allPoints = this.lastPoints.features
        .concat(this.lastBuckets.features)
        .concat(this.lastMarkers.features)
        .concat(this.lastCountries.features);
      if ((window as any).DEBUG_COORDS) {
        console.log(
          'Showing total points:',
          allPoints.length,
          ', single:',
          this.lastPoints.features.length,
          ', cluster:',
          this.lastBuckets.features.length,
          ', countries:',
          this.lastCountries.features.length,
          ', markers:',
          this.lastMarkers.features.length
        );
      }
      this.map.setGeoPoints({
        type: 'FeatureCollection',
        features: allPoints,
      });
    }, 250);
  }

  protected decodeFacetValue(selectedFacetValues: any): { gte: any; lte: any } | undefined {
    const allValues = selectedFacetValues?.values;
    if (allValues === undefined || allValues.length === 0) {
      return undefined;
    }
    let { gte, lte } = allValues[allValues.length - 1].value;
    if (typeof gte === 'string') {
      gte = JSON.parse(gte);
    }
    if (typeof lte === 'string') {
      lte = JSON.parse(lte);
    }
    return { gte, lte };
  }

  mapboxImages(): MapboxImages[] {
    return images;
  }

  protected getEnCountryName(deCountryName: string): {
    enCountryName: string;
    translated: boolean;
  } {
    let enCountryName: string = this.translate.getParsedResult(
      this.translate.translations['en'],
      `env.facetFields.label.fct_countryname.${deCountryName}`
    );
    let translated = true;
    if (enCountryName.startsWith('env.')) {
      // console.log("Fix unknown country translation:", enCountryName);
      enCountryName = deCountryName;
      translated = false;
    }
    return { enCountryName, translated };
  }

  closePreview() {
    this.showPreview$.next(null);
  }

  closedInput($event) {
    if ($event.code === 'Enter') {
      $event.target.blur();
    }
  }

  mapboxEventHandlers(): MapboxEventHandler<any>[] {
    return [
      {
        type: 'click',
        layer: 'country',
        listener: (map, e) => {
          const features = map.queryRenderedFeatures(e.point);
          if (features[0].layer.id === 'country') {
            const countryValue = JSON.parse(e.features[0].properties.facetValue);
            const countryLabel = features[0].properties.deLabel;
            const cooKey = environment.mapConfig.coordinateFacetKey;
            this.searchStore.dispatch(new RemoveAllHistogramBoundary(cooKey, true));
            const countryKey = environment.countryFacetKey;
            this.searchStore.dispatch(new RemoveAllFacetValue(countryKey));
            this.searchStore.dispatch(
              new AddFacetValue({
                facet: countryKey,
                id: countryLabel,
                label: countryLabel,
                value: countryValue,
              })
            );
            this.goToSearch();
          }
        },
      },

      {
        type: 'click',
        layer: 'clusters',
        listener: (map, e) => {
          const features = map.queryRenderedFeatures(e.point, {
            layers: ['clusters'],
          });
          const geoHash = features[0].properties.geoHash;
          let precision: number;
          this.precision$.pipe(take(1)).subscribe((p) => (precision = p));
          if ((window as any).DEBUG_COORDS) {
            console.log('Show geohash:', geoHash, precision, this.map.getZoom());
          }
          const mapZoomToPrecision: ZoomToPrecision[] = environment.mapConfig?.zoomPrecisionMapping ?? [];
          const lastMapping = mapZoomToPrecision.length > 0 ? mapZoomToPrecision[mapZoomToPrecision.length - 1] : undefined;
          const switchToSearch =
            mapZoomToPrecision.length > 0 &&
            precision === lastMapping?.precision &&
            Math.abs(this.map.getZoom() - lastMapping?.zoomEnd) < 0.01;
          if (switchToSearch) {
            this.goToSearch();
          } else {
            // zoom in ...
            const geoHashRect = GeoHash.neighbours(geoHash);
            const sw = GeoHash.decode(geoHashRect.sw);
            const ne = GeoHash.decode(geoHashRect.ne);
            map.fitBounds(new mapboxgl.LngLatBounds(sw, ne));
          }
        },
      } as MapboxEventHandler<'click'>,

      {
        type: 'click',
        layer: 'unclustered-point',
        listener: (map, e) => {
          const coordinates = this.featureCoo(e.features).slice() as [number, number];
          const docData = e.features[0].properties;
          // const geoHash = docData.geoHash;
          // const lat = docData.lat;
          // const lng = docData.lng;

          // Ensure that if the map is zoomed out such that
          // multiple copies of the feature are visible, the
          // popup appears over the copy being pointed to.
          while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
          }

          let currentSnippet;
          this.showPreview$.pipe(take(1)).subscribe((s) => (currentSnippet = s));

          try {
            const snippet = JSON.parse(docData.snippet);
            if (currentSnippet?.id === snippet?.id && snippet !== null && typeof snippet === 'object') {
              this.closePreview();
            } else {
              this.showPreview$.next(snippet);
            }
          } catch (ex) {
            console.error("Can't parse:\n", docData.snippet);
          }
        },
      } as MapboxEventHandler<'click'>,

      {
        type: 'mouseenter',
        layer: ['clusters', 'unclustered-point', 'country'],
        listener: (map, e) => {
          map.getCanvas().style.cursor = 'pointer';
          if ((window as any).DEBUG_COORDS) {
            console.log('GEOHASH', e.features[0].properties.geoHash);
          }
        },
      } as MapboxEventHandler<'mouseenter'>,

      {
        type: 'mouseleave',
        layer: ['clusters', 'unclustered-point', 'country'],
        listener: (map) => {
          map.getCanvas().style.cursor = '';
        },
      } as MapboxEventHandler<'mouseleave'>,
    ];
  }

  // also called on every year scale thumb move!
  initialArea(): MapConfig {
    const customMapConfig = { ...this.env.mapConfig };
    customMapConfig.config = { ...customMapConfig.config };
    // use last zoom
    this.mapZoom$.pipe(take(1)).subscribe((z) => (customMapConfig.config.zoom = z));
    // use global store ... because selected facet coords were perhaps modified for search
    this.mapCenter$.pipe(take(1)).subscribe((c) => {
      customMapConfig.config.lat = typeof c.lat === 'number' && !isNaN(c.lat) ? c.lat : customMapConfig.config.lat;
      customMapConfig.config.lng = typeof c.lon === 'number' && !isNaN(c.lon) ? c.lon : customMapConfig.config.lng;
    });

    return customMapConfig;
  }

  protected handleAutoPrecision(zoom: number) {
    const precision = precisionForZoom(zoom, environment.mapConfig.zoomPrecisionMapping);
    if ((window as any).DEBUG_COORDS) {
      console.log('Using precision', precision);
    }
    this.searchStore.dispatch(new SetGeoClusterPrecision(precision));
  }

  protected featureCoo(features: mapboxgl.MapboxGeoJSONFeature[]): [number, number] {
    return (features[0].geometry as Point).coordinates as [number, number];
  }

  mapboxLayers(): mapboxgl.AnyLayer[] {
    const step1 = 100;
    const step2 = 250;
    const lang = this.translate.currentLang;

    return [
      {
        id: 'country',
        type: 'symbol',
        source: 'earthquakes',
        filter: ['all', ['==', ['get', 'type'], 'country'], ['>=', ['zoom'], 4]],
        layout: {
          'icon-image': ['get', 'type'],
          'icon-size': 1,
          'icon-anchor': 'bottom-left',
          'text-field': ['get', lang + 'Label'],
          'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
          'text-size': 20,
          'text-offset': [0, 1],
          'icon-allow-overlap': true,
          'text-allow-overlap': true,
        },
        paint: {
          'text-color': '#555453',
        },
      },
      {
        id: 'unclustered-point',
        type: 'symbol',
        source: 'earthquakes',
        filter: ['==', ['get', 'type'], 'point'],
        layout: {
          'icon-image': ['get', 'type'],
          'icon-size': 0.5,
          'icon-allow-overlap': true,
          'icon-anchor': 'bottom',
          'text-allow-overlap': true,
        },
      },
      {
        id: 'clusters',
        type: 'circle',
        source: 'earthquakes',
        filter: ['has', 'point_count'],
        paint: {
          // Use step expressions (https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
          'circle-color': ['step', ['get', 'point_count'], '#19B9A9', step1, '#19B9A9', step2, '#19B9A9'],
          'circle-radius': ['step', ['get', 'point_count'], 15, step1, 20, step2, 25],
        },
      },
      {
        id: 'cluster-count',
        type: 'symbol',
        source: 'earthquakes',
        filter: ['has', 'point_count'],
        layout: {
          'text-field': ['get', 'point_count_abbreviated'],
          'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
          'text-size': 12,
          'icon-allow-overlap': true,
          // "text-allow-overlap": true,
        },
      },
      {
        id: 'user-markers',
        type: 'symbol',
        source: 'earthquakes',
        filter: ['==', ['get', 'type'], 'flag'],
        layout: {
          'icon-image': ['get', 'type'],
          'icon-size': 0.1,
          'icon-allow-overlap': true,
        },
      },
    ];
  }

  updateMap(mapLimits: MapLimits) {
    const newCenter = mapLimits.center;
    this.searchStore.dispatch(new SetCenter({ lat: newCenter.lat, lon: newCenter.lng }));

    this.searchStore.dispatch(new SetZoom(mapLimits.zoom));

    const precisionSelect = this.precisionRef?.nativeElement;
    const precisionMode = precisionSelect ? precisionSelect.selectedOptions[precisionSelect.selectedIndex].value : 'auto';
    if (precisionMode === 'auto') {
      this.handleAutoPrecision(mapLimits.zoom);
    }
    this.updateCooSearchData();

    this.execSearch();
  }

  setYear(v: number, year$: BehaviorSubject<number>) {
    const currentYear = new Date().getFullYear();
    year$.next(Math.max(MIN_YEAR, Math.min(v, currentYear)));
  }

  execSearch() {
    this.searchStore.dispatch(new SetOffset(0));
    this.searchStore.dispatch(new SimpleSearch());
  }

  switchToSearch(setCoordFacet: boolean) {
    if (setCoordFacet) {
      this.updateCooSearchData();
    }
    this.router.navigate([this.localize.translateRoute('/search')]);
  }

  protected goToSearch() {
    this.switchToSearch(false);
  }

  protected prepareYearInput(year: string | undefined): number | undefined {
    if (year === undefined) {
      return undefined;
    }
    year = year.trim();
    if (year.length === 0) {
      return undefined;
    } else {
      const onlyDigits = year.match(/^[0-9]{3,4}$/);
      if (onlyDigits === null) {
        this.showYearError('map-page.year-bad-format', { year });
        return undefined;
      }
    }
    const yearNum = parseInt(year, 10);
    if (yearNum < MIN_YEAR) {
      this.showYearError('map-page.year-too-early', { year });
      return undefined;
    }
    const currentYear = new Date().getFullYear();
    if (yearNum > currentYear) {
      this.showYearError('map-page.future-year', { year });
      return undefined;
    }
    return Math.min(currentYear, yearNum);
  }

  selectTimeRange(startYearRaw: string, endYearRaw: string) {
    const yearValidity = this.checkTimeRange(startYearRaw, endYearRaw, (startYear, endYear) => {
      this.updateYearSearchData(startYear, endYear);
      this.execSearch();
    });
    // user hit "OK", but both input fields are empty
    // clear the facet value
    console.log('yearValidity', yearValidity);
    if (yearValidity === 'UNDEFINED') {
      this.searchStore.dispatch(new RemoveAllHistogramBoundary(environment.mapConfig.yearFacetKey, true));
      this.execSearch();
    }
  }

  protected updateYearSearchData(startYear: number | undefined, endYear: number | undefined) {
    if (startYear !== undefined || endYear !== undefined) {
      const yearKey = environment.mapConfig.yearFacetKey;
      const { label: yearLabel, value: yearValue } = AbstractHistogramFacetComponentDirective.buildLabelAndValue(
        startYear !== undefined ? '' + startYear : undefined,
        endYear !== undefined ? '' + endYear : undefined
      );
      const yearId = buildId(yearValue);
      const year = {
        key: yearKey,
        label: yearLabel,
        id: yearId,
        value: yearValue,
      };
      this.searchStore.dispatch(new RemoveAllHistogramBoundary(year.key, true));
      this.searchStore.dispatch(new AddHistogramBoundaries(year));
    }
  }
  /**
   * Returns "UNDEFINED" if both year params are undefined.
   * Returns "OK" if one of the year params is not undefined and a valid number.
   * Returns "INVALID" otherwise.
   *
   * @param startYearRaw
   * @param endYearRaw
   * @param validAction
   * @returns "OK" or "INVALID" or "UNDEFINED"
   */
  checkTimeRange(
    startYearRaw: string | undefined,
    endYearRaw: string | undefined,
    validAction: (startYear: number, endYear: number) => void
  ): 'OK' | 'INVALID' | 'UNDEFINED' {
    try {
      const startYear = this.prepareYearInput(startYearRaw);
      const endYear = this.prepareYearInput(endYearRaw);

      if (startYear !== undefined || endYear !== undefined) {
        // can't use pipe expression in action handler!
        const wrongOrder = startYear !== undefined && endYear !== undefined && startYear > endYear;
        if (!wrongOrder) {
          validAction(startYear, endYear);
          if (startYear !== undefined) {
            this.setYear(startYear, this.startYear$);
            this.timeSlider.setLeftThumb(startYear);
          }
          if (endYear !== undefined) {
            this.setYear(endYear, this.endYear$);
            this.timeSlider.setRightThumb(endYear);
          }
          return 'OK';
        } else {
          this.showYearError('map-page.wrong-year-order', {
            startYear,
            endYear,
          });
          return 'INVALID';
        }
      } else {
        return 'UNDEFINED';
      }
    } catch (e) {
      console.error('Bad range input', e);
      return 'INVALID';
    }
  }

  protected showYearError(i18nKey: string, values: { [key: string]: string | number }) {
    this.toastr.error({ key: i18nKey, values: values }, 'map-page.year-error-toast-title');
  }

  protected convertLatLon(coo: mapboxgl.LngLat): { lat: number; lon: number } {
    const newCoo: { lat: number; lon: number } = {
      lat: coo.lat,
      lon: coo.lng,
    };
    return newCoo;
  }

  get defaultStartYear() {
    return MIN_YEAR;
  }

  get defaultEndYear() {
    const currentYear = new Date().getFullYear();
    return Math.min(currentYear, defaultTimeSliderTicks[defaultTimeSliderTicks.length - 1].value);
  }

  protected get map(): MapBoxComponent {
    return this.mapBox;
  }

  get timeSliderTicks() {
    return defaultTimeSliderTicks;
  }

  protected updateCooSearchData() {
    const { topLeft, bottomRight } = this.map.currentRectangle();
    const { label: cooLabel, value: cooValue } = AbstractHistogramFacetComponentDirective.buildLabelAndValue(
      JSON.stringify(this.convertLatLon(topLeft as mapboxgl.LngLat)),
      JSON.stringify(this.convertLatLon(bottomRight as mapboxgl.LngLat))
    );
    const cooId = buildId(cooValue);

    const coo = {
      key: environment.mapConfig.coordinateFacetKey,
      label: cooLabel,
      id: cooId,
      value: cooValue,
    };

    this.searchStore.dispatch(new RemoveAllHistogramBoundary(coo.key, true));
    this.searchStore.dispatch(new AddHistogramBoundaries(coo));
  }
}
