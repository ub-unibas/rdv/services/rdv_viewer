import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "@env/environment";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { catchError, filter, map, of, switchMap } from "rxjs";
import {
  SetCountries,
  CountriesActionTypes,
  CountriesResponseError,
  CountriesResponseOk,
  LoadCountries,
  SendCountriesRequest,
} from "../actions/countries.actions";

@Injectable()
export class CountriesEffects {
  requestSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CountriesActionTypes.CountriesResponseOk),
      map((action: CountriesResponseOk) => action.payload),
      filter((x) => !!x),
      switchMap((result) => [new SetCountries(result)])
    )
  );

  requestFailure$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CountriesActionTypes.CountriesResponseError),
      map((action: CountriesResponseError) => action.payload),
      switchMap((err) => {
        console.error("Can't load countries", err);
        return [];
      })
    )
  );

  makeRequest$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CountriesActionTypes.SendCountriesRequest),
      map((action: SendCountriesRequest) => action.payload),
      switchMap(({ url, query, countryField }) => {
        const qmPos = url.indexOf("?");
        const urlSeparator = qmPos >= 0 ? "&" : "?";
        url += urlSeparator + "ic=1";

        return this.http
          .post(url, JSON.stringify(query), {
            headers: {
              "Content-Type": "application/json",
              "X-Request-Type": "countries",
            },
            withCredentials: true,
          })
          .pipe(
            map(
              (result: any) =>
                new CountriesResponseOk({
                  countries: result?.facets[countryField] ?? [],
                  language: query.lang,
                })
            ),
            catchError((err) => of(new CountriesResponseError(err)))
          );
      })
    )
  );

  loadCountries$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CountriesActionTypes.LoadCountries),
      map((action: LoadCountries) => action.payload),
      switchMap(({ facet, language }) => {
      return [
        new SendCountriesRequest({
          url: environment.countriesProxyUrl + "countries/",
          countryField: facet.field,
          query: {
            query_params: {
              size: 20,
              sort: [
                {
                  field: "_score",
                  order: "desc",
                },
              ],
              start: 0,
              source: [],
            },
            match_all: true,
            facets: {
              country_coordinates: {
                field: facet.field,
                values: [],
                operator: facet.operator,
                order: "count",
                facet_type: "basic",
                size: facet.size ?? 200,
              },
            },
            lang: language,
            open_facets: { [facet.field]: "" },
            viewer: "List",
          },
        }),
      ];
    })
    )
  );

  constructor(protected actions$: Actions, protected http: HttpClient) {}
}
