import { Action } from "@ngrx/store";
import {
  FacetFieldModel,
  FacetResponse,
} from "@rdv-lib";

export type Country = FacetResponse & {
  geopoint: { lat: number; lon: number };
};

/* eslint-disable @typescript-eslint/no-shadow */
export enum CountriesActionTypes {
  SetCountries = "[Countries] Set countries",
  LoadCountries = "[Countries] Load countries",
  SendCountriesRequest = "[Countries] Send countries request",
  CountriesResponseOk = "[Countries] Countries response ok",
  CountriesResponseError = "[Countries] Countries response error",
}
/* eslint-enable @typescript-eslint/no-shadow */

/**
 * Set de countries
 */
export class SetCountries implements Action {
  /**
   * @ignore
   */
  readonly type = CountriesActionTypes.SetCountries;

  /**
   * Contains all results to be displayed
   * @param payload
   */
  constructor(public payload: { countries: Country[]; language: string }) {}
}

/**
 * Loads countries
 */
export class LoadCountries implements Action {
  /**
   * @ignore
   */
  readonly type = CountriesActionTypes.LoadCountries;

  /**
   * Contains all results to be displayed
   * @param payload
   */
  constructor(public payload: { facet: FacetFieldModel; language: string }) {}
}

/**
 * Loads countries
 */
export class SendCountriesRequest implements Action {
  /**
   * @ignore
   */
  readonly type = CountriesActionTypes.SendCountriesRequest;

  /**
   * Contains all results to be displayed
   * @param payload
   */
  constructor(
    public payload: { url: string; query: any; countryField: string }
  ) {}
}

/**
 * Ok countries reponse
 */
export class CountriesResponseOk implements Action {
  /**
   * @ignore
   */
  readonly type = CountriesActionTypes.CountriesResponseOk;

  /**
   * Contains all results to be displayed
   * @param payload
   */
  constructor(public payload: { countries: Country[]; language: string }) {}
}

/**
 * Ok countries reponse
 */
export class CountriesResponseError implements Action {
  /**
   * @ignore
   */
  readonly type = CountriesActionTypes.CountriesResponseError;

  /**
   * Contains all results to be displayed
   * @param payload
   */
  constructor(public payload: string) {}
}

export type CountriesActions =
  | SetCountries
  | LoadCountries
  | SendCountriesRequest
  | CountriesResponseOk
  | CountriesResponseError;
