/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AboutComponent } from './components/about.component';
// import {DetailedViewComponent} from './components/detailed-view.component';
import { Title } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';
import { AbstractAppComponent } from './components/app.component';
import { BetaHeaderComponent } from './components/beta-header.component';
import { FooterComponent } from './components/footer.component';
import { HeaderComponent } from './components/header.component';
import { RangeSliderComponent } from '../map/range-slider/range-slider.component';
import { SearchModule } from '../search/search.module';

const COMPONENTS = [
  BetaHeaderComponent,
  AboutComponent,
  AbstractAppComponent,
  FooterComponent,
  HeaderComponent,
  RangeSliderComponent,
];

/**
 * Contains components for core functionalities and handles state for dynamically loaded filter settings
 */
@NgModule({
  imports: [CommonModule, TranslateModule.forChild(),SearchModule],
  providers: [Title],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})
export class CoreModule {}
