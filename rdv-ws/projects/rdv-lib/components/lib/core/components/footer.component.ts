/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import { AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { environment } from '../../shared/Environment';
import {
  FooterSettingsModel,
  footerSettingsModelByLanguage,
  HeaderSettingsModel,
  headerSettingsModelByLanguage,
} from '../../shared/models/settings.model';
import { ResetUtilService } from '../../shared/services/reset-util.service';
import { UrlParamNames } from '../../shared/services/url-params.service';

/**
 * Application footer component
 */
@Component({
  selector: 'app-footer',
  template: `
    <ng-container>
      <div class="footer row g-0 justify-content-start pb-5 pb-sm-0 align-items-center pt-3 pt-sm-0">
        <div class="col-xs-auto col-sm-auto" *ngIf="showsPermalink()">
          <a class="Permalink" href="{{ url$ | async }}" target="_blank">{{ 'footer.permalink' | translate }}</a>
        </div>
        <div class="col">
          <div class="d-flex justify-content-end">
            <div class="to-top-container me-3 me-sm-0" *ngIf="(footerSettings$ | async)?.displayToTop !== false">
              <button title="{{ 'footer.to_top' | translate }}" (click)="resetUtilService.scrollToTop()" class="btn to-top">
                {{ 'footer.to_top_hovered' | translate }}
              </button>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer-container" *ngIf="(footerSettings$ | async)?.disable !== true">
        <div class="footer row g-0 justify-content-start pb-5 pb-sm-0 align-items-center pt-3 pt-sm-0">
          <div>
            <span class="d-xs-block d-sm-inline" *ngIf="(footerSettings$ | async)?.displayHostUrl !== false">
              <a href="{{ hostUrl(footerSettings$ | async) | translate }}" target="_blank">
                {{ hostName(footerSettings$ | async) | translate }}
              </a>
              <span class="footer__link-separator"> /&nbsp;</span>
            </span>
            <div class="d-xs-block d-sm-none" style="width: 100%;"></div>
            <div class="d-xs-block d-sm-inline" *ngIf="(footerSettings$ | async)?.displayPortalUrl !== false">
              <button class="button--no-decoration" (click)="resetFilters()" title="{{ 'top.headerSettings.resetSearch' | translate }}">
                {{ envName(headerSettings$ | async) | translate }}
              </button>
              <span class="footer__link-separator"> /&nbsp;</span>
            </div>
            <span class="d-xs-block d-sm-inline col-12 col-sm">
              <a href="{{ 'https://ub.unibas.ch/' + currentLanguage() + '/impressum/' }}" target="_blank">
                {{ 'footer.footerSettings.impress' | translate }}
              </a>
            </span>
          </div>
        </div>
      </footer>
    </ng-container>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterComponent implements OnDestroy, AfterViewInit {
  footerSettingsSubject: BehaviorSubject<FooterSettingsModel>;
  footerSettings$: Observable<FooterSettingsModel>;
  headerSettingsSubject: BehaviorSubject<HeaderSettingsModel>;
  headerSettings$: Observable<HeaderSettingsModel>;
  languageChangeSubscription: Subscription;
  urlSubject: BehaviorSubject<string>;
  url$: Observable<string>;

  routerSubscription: Subscription;

  constructor(
    protected translate: TranslateService,
    protected resetUtilService: ResetUtilService,
    protected route: ActivatedRoute,
    protected router: Router
  ) {
    this.footerSettingsSubject = new BehaviorSubject(undefined);
    this.headerSettingsSubject = new BehaviorSubject(undefined);
    this.languageChanged(translate.currentLang);
    this.footerSettings$ = this.footerSettingsSubject.asObservable();
    this.headerSettings$ = this.headerSettingsSubject.asObservable();
    this.urlSubject = new BehaviorSubject(undefined);
    this.url$ = this.urlSubject.asObservable();
    this.routerSubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        const url = event.url;
        const urlObj = new URL(url, location.protocol + location.host);
        const searchParams = urlObj.searchParams;
        searchParams.delete(UrlParamNames.HIDE_HEADER.toString());
        const noHeaderSearchParams = searchParams.toString();
        let noHeaderUrl = urlObj.pathname;
        if (noHeaderSearchParams.length > 0) {
          noHeaderUrl += '?' + noHeaderSearchParams;
        }
        this.urlSubject.next(noHeaderUrl);
      }
    });
  }

  hostName(footerSettingsModel: FooterSettingsModel): string {
    return (footerSettingsModel && footerSettingsModel.hostName) || 'footer.footerSettings.host.name';
  }

  hostUrl(footerSettingsModel: FooterSettingsModel): string {
    return (footerSettingsModel && footerSettingsModel.hostUrl) || 'footer.footerSettings.host.url';
  }

  currentLanguage(): string {
    // currently www.unibas.ch only supports "de"
    return 'de';
    // return this.translate.currentLang;
  }

  ngOnDestroy(): void {
    if (this.languageChangeSubscription) {
      this.languageChangeSubscription.unsubscribe();
    }
    if (this.routerSubscription) {
      this.routerSubscription.unsubscribe();
    }
  }

  protected languageChanged(language) {
    const footerSettings = footerSettingsModelByLanguage(language, environment.footerSettings);
    this.footerSettingsSubject.next(footerSettings);
    const headerSettings = headerSettingsModelByLanguage(language, environment.headerSettings);
    this.headerSettingsSubject.next(headerSettings);
  }

  ngAfterViewInit(): void {
    this.languageChangeSubscription = this.translate.onLangChange.subscribe((params: LangChangeEvent) => this.languageChanged(params.lang));
  }

  envName(headerSettingsModel: HeaderSettingsModel): string {
    return (headerSettingsModel && headerSettingsModel.name) || 'top.headerSettings.name';
  }

  resetFilters() {
    this.resetUtilService.reload(true);
  }

  showsPermalink(): boolean {
    // const hs = headerSettingsModelByLanguage(this.translate.currentLang, environment.headerSettings);
    // return hs.disable === true;
    return true;
  }

  currentUrl(): string {
    return this.router.url;
  }
}
