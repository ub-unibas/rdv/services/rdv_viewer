import { CoreSettings, Endpoints, ExtendedSettings } from './settings.model';

export interface ParcEndpoints extends Endpoints {
  readonly countriesProxyUrl: string;
}

export type ParcBaseSettings = CoreSettings & ParcEndpoints;

export type MapSettingsModel = ParcBaseSettings &
  ExtendedSettings & {
    /**
     * country facet key in environment.facetFields
     */
    countriesProxyUrl: string;
    countryFacetKey: string;
  };
