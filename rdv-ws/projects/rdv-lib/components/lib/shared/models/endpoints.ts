import { Endpoints } from './settings.model';

export function endpointBasePaths(hostVersion: string): Endpoints {
  return {
    proxyUrl: hostVersion + '/rdv_query/es_proxy/',
    geoSearchProxyUrl: hostVersion + '/rdv_query/es_proxy/',
    moreProxyUrl: hostVersion + '/rdv_query/further_snippets/',
    inFacetSearchProxyUrl: hostVersion + '/rdv_query/facet_search/',
    detailProxyUrl: hostVersion + '/rdv_object/object_view/',
    documentViewerProxyUrl: hostVersion + '/rdv_query/iiif_flex_pres/',
    navDetailProxyUrl: hostVersion + '/rdv_query/next_objectview/',
    popupQueryProxyUrl: hostVersion + '/rdv_query/popup_query/',
    detailSuggestionProxyUrl: hostVersion + '/rdv_query/form_query/',
    suggestSearchWordProxyUrl: hostVersion + '/rdv_query/autocomplete/',
    detailEditProxyUrl: hostVersion + '/rdv_object/object_edit/',
    detailNewProxyUrl: hostVersion + '/rdv_object/object_new/',
  };
}

/**
 * Complete all base endpoint paths.
 * IMPORTANT: All endpoint paths have to end with 'roxyUrl'
 *
 * @param project
 * @param basePaths
 * @returns
 */
export function endpoints(project: string, basePaths: Endpoints): Endpoints {
  const endpointPaths = { ...basePaths };
  for (const key in basePaths) {
    if (key.endsWith('roxyUrl') && typeof basePaths[key] === 'string') {
      endpointPaths[key] = basePaths[key] + project + '/';
    }
  }
  return endpointPaths;
}
