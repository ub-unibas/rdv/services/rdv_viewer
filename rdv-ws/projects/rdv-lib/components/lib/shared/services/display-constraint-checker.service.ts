import { Injectable } from '@angular/core';
import { DisplayConstraint, DisplayConstraintType } from '../models/settings.model';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root',
})
export class DisplayConstraintService {
  public constructor(protected translate: TranslateService) {}

  public isTrue(constraint: DisplayConstraint): boolean {
    switch (constraint.type) {
      case DisplayConstraintType.LANGUAGE:
        const currentLang = this.translate.currentLang;
        return (constraint.value as string[]).includes(currentLang);
      default:
        console.error('Unknown constraint type: ' + JSON.stringify(constraint));
        return false;
    }
  }

  public allowed(constraints: DisplayConstraint[] | undefined): boolean {
    if (constraints !== undefined) {
      for (const c of constraints) {
        if (!this.isTrue(c)) {
          return false;
        }
      }
    }
    return true;
  }
}
