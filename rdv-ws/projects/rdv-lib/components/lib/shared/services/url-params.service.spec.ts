/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {initFacetReducer} from "../../search/reducers/facet.reducer";
import {initFormReducer} from "../../search/reducers/form.reducer";
import {initLayoutReducer} from "../../search/reducers/layout.reducer";
import {initQueryReducer} from "../../search/reducers/query.reducer";
import {initRemoteFilterConfigsReducer} from "../../core/reducers/remote-filter-configs.reducer";
import {initCoreReducers} from "../../reducers/index";

import {initEnv} from "../Environment";

initEnv({

  // Defaultsprache für fehlende Übersetzungen in anderen Sprachen
  defaultLanguage: 'de',

  // optional pro Sprache environment-spezifische Übersetzungen
  // i18n: {
  //   "de": {
  //     "beta-header.contact": "XXX Kontaktieren Sie bei Fragen: ",
  //   },
  //   "en": {
  //     "beta-header.contact": "YYY Contact: ",
  //   }
  // },

  production: false,

  showSimpleSearch: true,
  // per Default werden Preview Images angezeigt
  // showImagePreview: false,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "https://ub-zas.ub.unibas.ch/",

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    default: {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: true,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: true
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  // footerSettings: {
  //   "default": {
  //     // den ganzen Footer ausblenden
  //     disable: false,
  //     // den "Nach Oben"-Link anzeigen
  //     displayToTop: true,
  //     // Host des Projekts im Footer anzeigen ("Universität Basel")
  //     displayHostUrl: true,
  //     // i18n key der URL des Host-Links (bei displayHostUrl: true)
  //     hostUrl: "footer.footerSettings.host.url",
  //     // i18n key des Namens des Host-Links (bei displayHostUrl: true)
  //     hostName: "footer.footerSettings.host.name",
  //     // Portal-URL im Footer anzeigen
  //     displayPortalUrl: true
  //   },
  //   // sprachspezifische Einstellungen (ohne Übersetzungen)
  //   "de": {
  //   },
  //   "en": {
  //   }
  // },


  // Such-Ansicht:
  // UV/Mirador verwenden nur maximale Breite der app (false, undefined).
  // Wird diese Option auf true gesetzt, dann wird die volle Browser-Viewport-Breite verwendet.
  viewerExtraWide: false,

  // Dokument-Detail-Ansicht:
  // Mit 'right' oder 'left', wird in der Detail-Ansicht ein IIIF Viewer rechts oder links
  // neben dem Feldern eines Dokuments angezeigt. false schaltet die gleichzeitige Ansicht ab.
  // embeddedIiifViewer: 'left',

  // Dokument-Detail-Ansicht:
  // Mit 'bottom' oder 'top', wird in der Detail-Ansicht ein IIIF Viewer unterhalb oder oberhalb
  // der Felder eines Dokuments angezeigt. false schaltet die gleichzeitige Ansicht ab.
  narrowEmbeddedIiiFViewer: 'bottom',

  // Such-Ansicht:
  // Wenn diese Option auf true gesetzt ist, wird nach einer Auswahl eines IIIF Viewers, die
  // Darstellung in einer Art Pseudo Fullscreen Modus versetzt. Die Anzeige wird dabei vertikal
  // auf die Anzeige von "Treffer" ausgerichtet und das Scrolling der ganzen Seite deaktiviert.
  // Dabei nimmt der Viewer die komplette verbleibende Höhe ein. Dies ist nur möglich, wenn die
  // Viewport-Höhe mindestens 575 Pixel und die -Breite mindestens 400 Pixel beträgt. Darunter
  // wird dieser Modus automatisch deaktiviert.
  // enablePseudoFullscreen: true,

  // die konkreten Werte, werden noch gesetzt
  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,

  // Ein link zu einer externen Website, die das Projekt kurz erklärt.
  // Wird nur eine URL (String) angegeben, dann wird diese URL für alle Sprachen verwendet.
  // Wird ein Objekt angegeben, so wird anhand der aktuellen Sprache die URL ausgewählt.
  // Hinweis: Für sprachspezifische URLs muss es mindestens einen Eintrag für die
  // "defaultSprache" geben.
  // externalHelpUrl: "https://ub-easyweb.ub.unibas.ch/de/ub-wirtschaft-swa/swa-446/",

  // Ein link zu einer externen Website, mit einer ausführlichen Hilfe.
  // Wird nur eine URL (String) angegeben, dann wird diese URL für alle Sprachen verwendet.
  // Wird ein Objekt angegeben, so wird anhand der aktuellen Sprache die URL ausgewählt.
  // Hinweis: Für sprachspezifische URLs muss es mindestens einen Eintrag für die
  // "defaultSprache" geben.
  externalAboutUrl: {
    de: "https://ub.unibas.ch/de/historische-bestaende/wirtschaftsdokumentation/zas-portal-infos/",
    en: "https://ub.unibas.ch/en/historical-holdings/economic-documentation/search-portal-newspaper-clippings/"
  },

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    options: {
      Quellname: "Zeitung",
      Textdatum: "Datum-Textform",
      all_text: "Freitext",
      descr_fuv: "Firmen und Verb\u00e4nde",
      descr_person: "Personen",
      descr_sach: "Sachdeskriptor",
      fulltext: "Volltext",
      title: "Dossiertitel"
    },
    preselect: [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "select-sort-fields.score_desc"
    },
    {
      field: "Quelldatum",
      order: SortOrder.ASC,
      display: "select-sort-fields.date_asc"
    },
    {
      field: "Quelldatum",
      order: SortOrder.DESC,
      display: "select-sort-fields.date_desc"
    },
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    Quelldatum: {
      field: "Quelldatum",
      facetType: FacetFieldType.HISTOGRAM,
      data_type: HistogramFieldType.DATE,
      label: "env.facetFields.Quelldatum",
      operator: "AND",
      showAggs: true,
      order: 10,
      size: 31,
      expandAmount: 31
    } as HistogramFieldModel,
    descr_fuv: {
      field: "descr_fuv.id.keyword",
      facetType: FacetFieldType.SIMPLE,
      label: "env.facetFields.descr_fuv",
      operator: "OR",
      operators: [
        "OR"
      ],
      order: 11,
      size: 100,
      expandAmount: 10,
      searchWithin: true,
      autocomplete_size: 3
    },
    descr_person: {
      field: "descr_person.id.keyword",
      facetType: FacetFieldType.SIMPLE,
      label: "env.facetFields.descr_person",
      operator: "OR",
      operators: [
        "OR"
      ],
      order: 12,
      size: 100,
      expandAmount: 10,
      searchWithin: true,
      autocomplete_size: 3
    },
    stw_ids: {
      field: "stw_ids.id.keyword",
      facetType: FacetFieldType.SIMPLE,
      label: "env.facetFields.descr_sach_prefLabel",
      operator: "OR",
      operators: [
        "OR"
      ],
      order: 13,
      size: 100,
      expandAmount: 10,
      searchWithin: true,
      autocomplete_size: 3
    },
    hierarchy_filter: {
      field: "hierarchy_filter.keyword",
      facetType: FacetFieldType.HIERARCHIC,
      label: "env.facetFields.descr_sach_hierarchy_filter",
      operator: "AND",
      order: 14,
      size: 100,
      expandAmount: 100
    },
    descr_place: {
      field: "descr_place.id.keyword",
      facetType: FacetFieldType.SIMPLE,
      label: "env.facetFields.fct_place",
      operator: "OR",
      operators: [
        "OR"
      ],
      order: 15,
      size: 100,
      expandAmount: 10,
      searchWithin: true,
      help: "env.facetFields.fct_place.help",
    },
    title: {
      field: "title.id.keyword",
      facetType: FacetFieldType.SIMPLE,
      label: "env.facetFields.title",
      operator: "OR",
      operators: [
        "OR"
      ],
      order: 16,
      expandAmount: 5,
      searchWithin: true,
    },
    Quellname: {
      field: "Quellname.keyword",
      facetType: FacetFieldType.SIMPLE,
      label: "env.facetFields.Quellname",
      operator: "OR",
      operators: [
        "OR"
      ],
      order: 17,
      size: 100,
      expandAmount: 10,
      searchWithin: true,
    },
    object_type: {
      field: "object_type.keyword",
      facetType: FacetFieldType.SUBCATEGORY,
      label: "env.facetFields.object_type",
      operator: "AND",
      operators: [
        "OR"
      ],
      order: 18,
      size: 100,
      expandAmount: 10
    },
    Sprache: {
      field: "Sprache.keyword",
      facetType: FacetFieldType.SIMPLE,
      label: "env.facetFields.Sprache",
      operator: "OR",
      operators: [
        "AND"
      ],
      order: 19,
      size: 100,
      expandAmount: 5,
    },

  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {
    Quelldatum: {
      field: "Quelldatum.keyword",
      facetType: FacetFieldType.RANGE,
      label: "Quelldatum",
      from: "1960-01-01",
      to: "2200-01-01",
      min: "1960-01-01",
      max: "2200-01-01",
      showMissingValues: true,
      order: 1,
      expandAmount: 5
    }
  },

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [10, 50, 100, 200],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    rows: 50,
    offset: 0,
    sortField: "_score",
    sortOrder: SortOrder.DESC
  },

  //Config fuer Merkliste
  basketConfig: {
    queryParams: {
      rows: 10,
      sortField: "_id",
      sortOrder: SortOrder.ASC
    }
  },

  showExportList: {
    basket: false,
    table: false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [
    {
      css: "col-sm-4 col-lg-4 text-start",
      extraInfo: true,
      field: "title",
      label: "Dossiertitel",
      landingpage: true,
      sort: "title.keyword"
    },
    {
      css: "col-sm-4 col-lg-4 text-start",
      field: "Quellname",
      label: "Zeitung",
      sort: "Quellname.keyword"
    },
    {
      css: "col-sm-4 col-lg-4 text-start",
      field: "Quelldatum",
      label: "Quelldatum",
      sort: "Quelldatum.keyword"
    }
  ],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
    descr_fuv: {
      display: ExtraInfoFieldType.TEXT,
      field: "descr_fuv",
      label: "Firmen und Verb\u00e4nde"
    },
    descr_person: {
      display: ExtraInfoFieldType.TEXT,
      field: "descr_person",
      label: "Personen"
    },
    descr_sach: {
      display: ExtraInfoFieldType.TEXT,
      field: "descr_sach",
      label: "Sachdeskriptor"
    },
    object_type: {
      display: ExtraInfoFieldType.TEXT,
      field: "object_type",
      label: "Objekttyp"
    }
  },

  i18n: {
    de: {
      "top.headerSettings.name": "Rechercheportal Zeitungsausschnitte",
      "top.headerSettings.name.Dev": "Rechercheportal Zeitungsausschnitte (Dev)",
      "top.headerSettings.name.Loc": "Rechercheportal Zeitungsausschnitte (Loc)",
      "top.headerSettings.name.Test": "Rechercheportal Zeitungsausschnitte (Test)",
      "top.headerSettings.betaBarContact.name": "UB Wirtschaft - SWA",
      "top.headerSettings.betaBarContact.email": "info-ubw-swa@unibas.ch",
    },
    en: {
      "top.headerSettings.name": "Search Portal Press Clippings",
      "top.headerSettings.name.Dev": "Search Portal Press Clippings (Dev)",
      "top.headerSettings.name.Loc": "Search Portal Press Clippings (Loc)",
      "top.headerSettings.name.Test": "Search Portal Press Clippings (Test)",
      "top.headerSettings.betaBarContact.name": "UB Wirtschaft - SWA",
      "top.headerSettings.betaBarContact.email": "info-ubw-swa@unibas.ch",
    }
  },

  documentViewer: {
    UV: {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}, {view: Page.Detail, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/zas_en.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "object_type",
          messageTitleKey: "search-results.view-mode.UV.object_type.message.title",
          messageTextKey: "search-results.view-mode.UV.object_type.message.text",
          values: [
            {label: "Zeitungsausschnitt", value: {id: "Zeitungsausschnitt"}},
            {label: "retrodigitalisierter Zeitungsausschnitt", value: {id: "retrodigitalisierter Zeitungsausschnitt"}},
            {
              label: "elektronischer Zeitungsausschnitt (ab 2013)",
              value: {id: "elektronischer Zeitungsausschnitt (ab 2013)"}
            }
          ]
        }
      ],
    },
    'UV-3.0.36': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 25}, {view: Page.Detail, order: 25}], // default: on all views
      viewerUrl: "assets/uv-3.0.36/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/zas_en.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "object_type",
          messageTitleKey: "search-results.view-mode.UV-3.0.36.object_type.message.title",
          messageTextKey: "search-results.view-mode.UV-3.0.36.object_type.message.text",
          values: [
            {label: "Zeitungsausschnitt", value: {id: "Zeitungsausschnitt"}},
            {label: "retrodigitalisierter Zeitungsausschnitt", value: {id: "retrodigitalisierter Zeitungsausschnitt"}},
            {
              label: "elektronischer Zeitungsausschnitt (ab 2013)",
              value: {id: "elektronischer Zeitungsausschnitt (ab 2013)"}
            }
          ]
        }
      ],
    },
    UVScroll: {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/scroll_view.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "object_type",
          messageTitleKey: "search-results.view-mode.UVScroll.object_type.message.title",
          messageTextKey: "search-results.view-mode.UVScroll.object_type.message.text",
          values: [
            {label: "Zeitungsausschnitt", value: {id: "Zeitungsausschnitt"}},
            {label: "retrodigitalisierter Zeitungsausschnitt", value: {id: "retrodigitalisierter Zeitungsausschnitt"}},
            {
              label: "elektronischer Zeitungsausschnitt (ab 2013)",
              value: {id: "elektronischer Zeitungsausschnitt (ab 2013)"}
            }
          ]
        }
      ],
    },
    'UV-3.0.36-Scroll': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 25}], // default: on all views
      viewerUrl: "assets/uv-3.0.36/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/scroll_view.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "object_type",
          messageTitleKey: "search-results.view-mode.UV-3.0.36-Scroll.object_type.message.title",
          messageTextKey: "search-results.view-mode.UV-3.0.36-Scroll.object_type.message.text",
          values: [
            {label: "Zeitungsausschnitt", value: {id: "Zeitungsausschnitt"}},
            {label: "retrodigitalisierter Zeitungsausschnitt", value: {id: "retrodigitalisierter Zeitungsausschnitt"}},
            {
              label: "elektronischer Zeitungsausschnitt (ab 2013)",
              value: {id: "elektronischer Zeitungsausschnitt (ab 2013)"}
            }
          ]
        }
      ],
    },
    MiradorScroll: {
      enabled: false,
      enabledForPage: [{view: Page.Search, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // mögliche Werte: "share", "download","image-tools","text-overlay"
      disabledPlugins: ["text-overlay"],

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_scroll.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "object_type",
          messageTitleKey: "search-results.view-mode.Mirador.object_type.message.title",
          messageTextKey: "search-results.view-mode.Mirador.object_type.message.text",
          values: [
            {label: "Zeitungsausschnitt", value: {id: "Zeitungsausschnitt"}},
            {label: "retrodigitalisierter Zeitungsausschnitt", value: {id: "retrodigitalisierter Zeitungsausschnitt"}},
            {
              label: "elektronischer Zeitungsausschnitt (ab 2013)",
              value: {id: "elektronischer Zeitungsausschnitt (ab 2013)"}
            }
          ]
        }
      ],
    },
    'Mirador-3.0.0-rc.5-Scroll': {
      enabled: false,
      enabledForPage: [{view: Page.Search, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador-3.0.0-rc.5/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // mögliche Werte: "share", "download","image-tools","text-overlay"
      disabledPlugins: ["text-overlay"],

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_scroll.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "object_type",
          messageTitleKey: "search-results.view-mode.Mirador.object_type.message.title",
          messageTextKey: "search-results.view-mode.Mirador.object_type.message.text",
          values: [
            {label: "Zeitungsausschnitt", value: {id: "Zeitungsausschnitt"}},
            {label: "retrodigitalisierter Zeitungsausschnitt", value: {id: "retrodigitalisierter Zeitungsausschnitt"}},
            {
              label: "elektronischer Zeitungsausschnitt (ab 2013)",
              value: {id: "elektronischer Zeitungsausschnitt (ab 2013)"}
            }
          ]
        }
      ],
    },
    MiradorSinglePage: {
      enabled: true,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_single.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "object_type",
          messageTitleKey: "search-results.view-mode.Mirador.object_type.message.title",
          messageTextKey: "search-results.view-mode.Mirador.object_type.message.text",
          values: [
            {label: "Zeitungsausschnitt", value: {id: "Zeitungsausschnitt"}},
            {label: "retrodigitalisierter Zeitungsausschnitt", value: {id: "retrodigitalisierter Zeitungsausschnitt"}},
            {
              label: "elektronischer Zeitungsausschnitt (ab 2013)",
              value: {id: "elektronischer Zeitungsausschnitt (ab 2013)"}
            }
          ]
        }
      ],
    },
    'Mirador-3.0.0-rc.5-SinglePage': {
      enabled: true,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador-3.0.0-rc.5/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_single.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "object_type",
          messageTitleKey: "search-results.view-mode.Mirador.object_type.message.title",
          messageTextKey: "search-results.view-mode.Mirador.object_type.message.text",
          values: [
            {label: "Zeitungsausschnitt", value: {id: "Zeitungsausschnitt"}},
            {label: "retrodigitalisierter Zeitungsausschnitt", value: {id: "retrodigitalisierter Zeitungsausschnitt"}},
            {
              label: "elektronischer Zeitungsausschnitt (ab 2013)",
              value: {id: "elektronischer Zeitungsausschnitt (ab 2013)"}
            }
          ]
        }
      ],
    }
  }
});
initFacetReducer();
initFormReducer();
initLayoutReducer();
initQueryReducer();
initRemoteFilterConfigsReducer();
initCoreReducers();

import {inject, TestBed} from '@angular/core/testing';
import {Injectable} from '@angular/core';

import {
  FacetValueProcessor,
  SearchActions,
  SearchValueProcessor,
  UrlParamNames,
  UrlParamsService
} from './url-params.service';
import {Store, StoreModule} from "@ngrx/store";

import * as fromSearch from '../../search/reducers/index';
import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  FacetValueOrder, HistogramFieldModel,
  HistogramFieldType, Page,
  SortOrder, ViewerType
} from "../models/settings.model";
import {ReplaceUrlParams, ViewerValueProcessor} from "./url-params.service";
import {BehaviorSubject} from "rxjs";


class TestFacetProc implements FacetValueProcessor {
  collectedInfos = {facets: [], histogram: [], hierarhic: [], operators: [], facetValueOrders: []};

  processSimpleFacet?(field: string, value: string): SearchActions[] {
    this.collectedInfos.facets.push({field, value});
    return [];
  }

  processHistogramFacet?(field: string, from: string, to: string): SearchActions[] {
    this.collectedInfos.histogram.push({field, from, to});
    return [];
  }

  processHierarchicFacet?(field: string, label: string, values: string[]): SearchActions[] {
    this.collectedInfos.hierarhic.push({field, values});
    return [];
  }

  processFacetOperator?(field: string, operator: string): SearchActions[] {
    this.collectedInfos.operators.push({field, operator});
    return [];
  }

  processFacetValueOrder?(field: string, order: string): SearchActions[] {
    this.collectedInfos.facetValueOrders.push({field, order});
    return [];
  }
}

class TestSearchProc implements SearchValueProcessor {
  collectedInfos = {searchText: [], sorting: []};

  processSimpleSearchValue(searchText: string[], searchField: string): SearchActions[] {
    this.collectedInfos.searchText.push({searchText, searchField});
    return [];
  }

  processSorting(sortField: string, sortDir: SortOrder): SearchActions[] {
    this.collectedInfos.searchText.push({sortField, sortDir});
    return [];
  }

}

class TestViewerProc implements ViewerValueProcessor {
  collectedInfos = {viewer: []};

  processViewer(name: string): SearchActions[] {
    this.collectedInfos.viewer.push({name});
    return [];
  }

}

@Injectable()
export class StoreMock<T = any> extends Store<T> {

  public source = new BehaviorSubject<any>({});

  public overrideState(allStates: any): void {
    this.source.next(allStates);
  }

}

describe('UrlParamService', () => {
  let store: StoreMock<fromSearch.State>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({
        ...fromSearch.reducers,
      })],
      providers: [UrlParamsService, {provide: Store, useClass: StoreMock}]
    });
    store = TestBed.inject(Store) as StoreMock<fromSearch.State>;
    store.overrideState({
      search: {
        form: {
          searchFields: {
            search0: {
              field: "all",
              value: []
            }
          },
          facetFields: {},
          histogramFields: {},
          hierarchyFields: {}
        },
        query: {
          sortField: "",
          sortDir: ""
        }
      }
    });
  });

  it('should be created', inject([UrlParamsService], (service: UrlParamsService) => {
    expect(service).toBeTruthy();
    const queryParams = "facets=Quellname%7ELe+Temps%21Der+Bund%0A";
    const up = new URLSearchParams(queryParams);
    const proc: TestFacetProc = new TestFacetProc();
    // don't use decodeURIComponent(), because "+" aren't resolved!
    service.parseFacet(up.get("facets"), proc);
    expect(proc.collectedInfos.facets.length).toEqual(2);
    expect(proc.collectedInfos.facets[0]).toEqual({field: 'Quellname', value: 'Le Temps'});
    expect(proc.collectedInfos.facets[1]).toEqual({field: 'Quellname', value: 'Der Bund\x0a'});
  }));

  it('should un-/escape',
    inject([UrlParamsService],
      (service: UrlParamsService) => {
        expect(service).toBeTruthy();

        // url encoding - decoding round trip
        const replaceUrls = new ReplaceUrlParams(store, null);
        // "!" is the search text separator in the URL and needs to be escaped
        const searchedText = ["A!B!", "C!D!"];

        // encode ...
        replaceUrls.setSearch(searchedText, "all");
        const params = replaceUrls.toUrlParams();
        const searchTextParam = params[UrlParamNames.SEARCHED_TEXT];
        expect(searchTextParam).toContain(searchedText[0].replace(/!/g, "\\!"));
        expect(searchTextParam).toContain(searchedText[1].replace(/!/g, "\\!"));
        const searchFieldParam = params[UrlParamNames.SEARCHED_FIELD];

        // decode ...
        const queryParams = UrlParamNames.SEARCHED_TEXT + "=" + searchTextParam
          + "&" + UrlParamNames.SEARCHED_FIELD + "=" + searchFieldParam;
        const up = new URLSearchParams(queryParams);
        const proc: TestSearchProc = new TestSearchProc();
        service.parseSearch(up.get(UrlParamNames.SEARCHED_TEXT), up.get(UrlParamNames.SEARCHED_FIELD), {all: true}, proc);
        expect(proc.collectedInfos.searchText.length).toEqual(1);
        expect(proc.collectedInfos.searchText[0].searchText.length).toEqual(2);
        expect(proc.collectedInfos.searchText[0].searchText).toContain(searchedText[0]);
        expect(proc.collectedInfos.searchText[0].searchText).toContain(searchedText[1]);
      }));

  it('should process viewer',
    inject([UrlParamsService],
      (service: UrlParamsService) => {
        expect(service).toBeTruthy();
        const replaceUrls = new ReplaceUrlParams(store, null);
        const selectedViewer = "MiradorScroll"; // has to be defined in env!

        // encode
        replaceUrls.setViewer(selectedViewer);
        const params = replaceUrls.toUrlParams();
        const viewerParam = params[UrlParamNames.VIEWER];
        expect(viewerParam).toEqual(selectedViewer);

        // decode
        const queryParams = UrlParamNames.VIEWER + "=" + selectedViewer;
        const up = new URLSearchParams(queryParams);
        const proc: TestViewerProc = new TestViewerProc();
        service.parseViewer(up.get(UrlParamNames.VIEWER), proc);
        expect(proc.collectedInfos.viewer.length).toEqual(1);
        expect(proc.collectedInfos.viewer[0].name).toEqual(selectedViewer);
      }));

  function opChecker(replaceUrls: ReplaceUrlParams,
                     service: UrlParamsService,
                     searchedFacet: string,
                     searchedOp: string): void {

    // encode
    const params = replaceUrls.toUrlParams();
    const searchParam = params[UrlParamNames.OPS];
    expect(searchParam).toBeDefined("Found no ops");
    expect(searchParam.length).toEqual(1, "Expected 1 op in url");
    expect(searchParam[0]).toContain(searchedFacet);
    expect(searchParam[0]).toContain(searchedOp);

    // decode
    const queryParams = UrlParamNames.OPS + "="
      + searchedFacet + ReplaceUrlParams.SEPARATOR_FACET_OPERATOR + searchedOp;
    const up = new URLSearchParams(queryParams);
    const proc: TestFacetProc = new TestFacetProc();
    service.parseOperator(up.get(UrlParamNames.OPS), proc);
    expect(proc.collectedInfos.operators.length).toEqual(1);
    expect(proc.collectedInfos.operators[0].field).toEqual(searchedFacet);
    expect(proc.collectedInfos.operators[0].operator).toEqual(searchedOp);
  }

  it('should process simple facet operator',
    inject([UrlParamsService],
      (service: UrlParamsService) => {
        expect(service).toBeTruthy();
        const replaceUrls = new ReplaceUrlParams(store, null);
        const searchedFacet = "Quellname";
        const searchedOp = "OR";

        replaceUrls.setFacetOperator(searchedFacet, searchedOp);
        opChecker(replaceUrls, service, searchedFacet, searchedOp);
      }));

  it('should process hierarchic facet operator',
    inject([UrlParamsService],
      (service: UrlParamsService) => {
        expect(service).toBeTruthy();
        const replaceUrls = new ReplaceUrlParams(store, null);
        const searchedFacet = "Quellname";
        const searchedOp = "AND";

        replaceUrls.setHierarchicOperator(searchedFacet, searchedOp);
        opChecker(replaceUrls, service, searchedFacet, searchedOp);
      }));

  it('should process histogram facet operator',
    inject([UrlParamsService],
      (service: UrlParamsService) => {
        expect(service).toBeTruthy();
        const replaceUrls = new ReplaceUrlParams(store, null);
        const searchedFacet = "Quellname";
        const searchedOp = "NOT";

        replaceUrls.setHistogramOperator(searchedFacet, searchedOp);
        opChecker(replaceUrls, service, searchedFacet, searchedOp);
      }));

  function facetValueOrderChecker(replaceUrls: ReplaceUrlParams,
                                  service: UrlParamsService,
                                  searchedFacet: string,
                                  searchedOrder: string): void {
    // encode
    const params = replaceUrls.toUrlParams();
    const searchParam = params[UrlParamNames.FACET_VALUE_ORDER];
    expect(searchParam.length).toEqual(1);
    expect(searchParam[0]).toContain(searchedFacet);
    expect(searchParam[0]).toContain(searchedOrder);

    // decode
    const queryParams = UrlParamNames.FACET_VALUE_ORDER + "="
      + searchedFacet + ReplaceUrlParams.SEPARATOR_FACET_VALUE_ORDER + searchedOrder;
    const up = new URLSearchParams(queryParams);
    const proc: TestFacetProc = new TestFacetProc();
    service.parseFacetValueOrder(up.get(UrlParamNames.FACET_VALUE_ORDER), proc);
    expect(proc.collectedInfos.facetValueOrders.length).toEqual(1);
    expect(proc.collectedInfos.facetValueOrders[0].field).toEqual(searchedFacet);
    expect(proc.collectedInfos.facetValueOrders[0].order).toEqual(searchedOrder);
  }

  it('should process simple facet value order',
    inject([UrlParamsService],
      (service: UrlParamsService) => {
        expect(service).toBeTruthy();
        const replaceUrls = new ReplaceUrlParams(store, null);
        const searchedFacet = "Quellname";
        const searchedOrder = FacetValueOrder.LABEL;

        replaceUrls.setFacetValueOrder(searchedFacet, searchedOrder);
        facetValueOrderChecker(replaceUrls, service, searchedFacet, searchedOrder);
      }));

  it('should process hierarchic facet value order',
    inject([UrlParamsService],
      (service: UrlParamsService) => {
        expect(service).toBeTruthy();
        const replaceUrls = new ReplaceUrlParams(store, null);
        const searchedFacet = "Quellname";
        const searchedOrder = FacetValueOrder.COUNT;

        replaceUrls.setHierarchicFacetValueOrder(searchedFacet, searchedOrder);
        facetValueOrderChecker(replaceUrls, service, searchedFacet, searchedOrder);
      }));
});
