import { MapSettingsModel } from './models/MapSettingsModel';
import { SettingsModel, updateShowHeader, ViewerType } from './models/settings.model';
import { UrlParamNames } from './services/url-params.service';

export let environment: MapSettingsModel;

export enum MiradorPlugins {
  PLUGIN_SHARE = 'share',
  PLUGIN_DOWNLOAD = 'download',
  PLUGIN_IMAGE_TOOLS = 'image-tools',
  PLUGIN_TEXT_OVERLAY = 'text-overlay',
  PLUGIN_CANVAS_LINK = 'canvas-link',
  PLUGIN_IMAGE_CROPPER = 'image-cropper',
  PLUGIN_OCR_HELPER = 'ocr-helper',
}

function disableMiradorPlugins(e: any, pluginsToDisable?: MiradorPlugins[]): SettingsModel {
  if (pluginsToDisable?.length > 0 && e.documentViewer) {
    const dvConfig = e.documentViewer;
    Object.keys(dvConfig)
      .filter((dvKey) => dvConfig[dvKey].type === ViewerType.MIRADOR && dvConfig[dvKey].enabled)
      .forEach((dvKey) => {
        const dv = dvConfig[dvKey];
        pluginsToDisable.forEach((pn) => {
          if (!dv.disabledPlugins || !dv.disabledPlugins.includes(pn.toString())) {
            dv.disabledPlugins = (dv.disabledPlugins ?? []).concat(pn.toString());
          }
        });
      });
  }
  return e;
}

export function initEnv(e: any, pluginsToDisable?: MiradorPlugins[]): void {
  environment = e;
  const params = new URL(document.location.toString()).searchParams;
  const noHeader = params.get(UrlParamNames.HIDE_HEADER.toString());
  updateShowHeader(!!noHeader, environment.headerSettings);
  if (pluginsToDisable) {
    disableMiradorPlugins(e, pluginsToDisable);
  }
}
