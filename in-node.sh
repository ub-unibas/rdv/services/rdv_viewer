#!/bin/bash

nodeRunnng=`docker ps |grep rdv_viewer-node-rdv`

if [ -z "${nodeRunnng}" ]; then
    # --service-ports enables http://localhost:3000 access!
    docker compose run --rm --service-ports --name rdv_viewer-node-rdv node-rdv "$@"
else
    docker exec -it rdv_viewer-node-rdv "$@"
fi