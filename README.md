# Research Data Viewer (RDV)

## Configuration

The application is highly configurable by providing a configuration file. A couple of examples
can be found in `src/environments`:

* `elastic` (default) und `production` (production-ready, i.e. optimized for performance)
* `freidok` and `freidok-prod` (production-ready)
* `bwsts` and `bwsts-prod` (production-ready)
* `elastic-mh` and `elastic-mh-prod` (production-ready)

In order to use another configuration than the default one, start the server with the `-c` option and 
provide the name of the desired configuration, e.g. `ng serve -c freidok-prod`.

### Additional configurations

1. Copy an existing configuration and change the deviating values. The new configuration should implement
`SettingsModel`.
2. In the `angular.json` file, add respective configuration blocks in the `targets` where you want to use
your new configuration (i.e. at least the `build` and `serve` targets with paths 
`targets.build.configurations.new_config` and `targets.serve.configurations.new_config`).

An example configuration:

```json
{
  "targets": {
    "build": {
      "configurations": {
        "new_config": {
          "optimization": true,
          "outputHashing": "all",
          "sourceMap": false,
          "extractCss": true,
          "namedChunks": false,
          "aot": true,
          "extractLicenses": true,
          "vendorChunk": false,
          "buildOptimizer": true,
          "fileReplacements": [
            {
              "replace": "src/environments/environment.ts",
              "with": "src/environments/environment.new_config.ts"
            }
          ]
        }
      }
    } 
  }
}
```

Apart from `fileReplacements` all fields can be left out. To avoid repetition, you can also link
a settings block for a configuration from another target definition:

```json
{
  "targets": {
    "serve": {
      "configurations": {
        "new_config": {
          "browserTarget": "rdv:build:new_config"
        }
      }
    }
  }
}
```

__Beware__: If you adjust the index and/or the type of the Elasticsearch cluster, you have to change also the respective settings in `angularx_elasticsearch_proxy_unibas.php`.


## Building documentation

You can build documentation locally with [compodoc](https://compodoc.github.io/compodoc/):

1. Install: `yarn global add @compodoc/compodoc` or `npm -g install @compodoc/compodoc`
2. Run in application root folder: `compodoc -swp src/tsconfig.app.json`


## Fonts

"Universität Basel" licensed some fonts which can't be published under
MIT license. To use your own fonts, edit rdv-ws/projects/rdv-lib/src/scss/rdv-lib-styles.scss,
comment out '@import "fonts"' and uncomment '@import "fonts-custom"'.

Finally place your font faces and declarations in rdv-ws/projects/rdv-lib/src/scss/_fonts-custom.scss and the fonts in rdv-ws/projects/rdv-lib/src/assets/font/.

## Build dev application

Initially: for e.g. zas-dev env

1. yarn install # once
1. yarn run build-mirador # once
1. ng serve -c zas-dev --host 0.0.0.0

Note: If you use the provided docker-compose env, simply
prefix each command with "./in-node.sh" which targets
the outer project. Stay in the project's main dir
to call ./in-node.sh".

Hot compile/deploy works on changes in rdv-ws/projects/rdv-lib too, because the import "@rdv-lib" points to "../rdv-ws/projects/rdv-lib/src/public-api.ts" (see "compilerOptions": > "paths": in tsconfig.json).

## For Afrika-Portal

`ng serve -c afrikaportal-loc --host 0.0.0.0`

see it on http://localhost:4200

it will use the query builder on http://127.0.0.1:5000/v1/rdv_query/es_proxy/, cf. environment.type-loc.ts

## Requirements

1. you need angular cli : sudo npm i -g @angular/cli@14
2. you need a recent version of node (at least 14.20.X)
   1. sudo npm install -g n
   2. sudo n stable
3. might need to clear npm cache mpm cache clean -f

## Build prod application

Example for zas-dev env:

1. yarn install # once
1. yarn run build-mirador # once
1. ng build --configuration=production,zas-dev
1. cp .htaccess dist/

The whole app is in dist/ ready for distribution.

## Dockerize and deploy on k8s 
1. build the prod application (see above)
2. docker build --network host --no-cache --rm=true -t rdv_africa_viewer_[environment] .
3. docker tag rdv_africa_viewer_[environment] registry.gitlab.com/ub-unibas/package-registry/rdv_africa_viewer_[environment]
4. docker login https://registry.gitlab.com/ub-unibas/package-registry
5. docker push registry.gitlab.com/ub-unibas/package-registry/rdv_africa_viewer_[environment]:latest
6. kubectl apply -f k8s-manifests/afrikaportal/[environment]/

## Run locally in docker

docker run -p 80:80 rdv_africa_viewer_[environment]

and go to localhost in your browser

## Custom App based on rdv_viewer

Create a new directory beneath your rdv_viewer directory and unpack rdv-app.tgz in it:

1. cd rdv_viewer
2. mkdir ../rdv-app
3. cd ../rdv-app
4. tar zxf ../rdv_viewer/rdv-app.tgz
5. optionally: docker-compose up -d
6. optionally: ./in-node.sh sh
7. yarn install
8. yarn run start
9. Open your browser at http://localhost:4300/

The example app rdv-app is based on the same dependency versions rdv_viewer currently uses and shows how to create a customized rdv_viewer app. It is possible to replace scss, i18n definitions, images, environments and of course ts code.

For demo purposes only, rdv-app

- shows a black custom header at the top showing "CUSTOM APP" (ts change),
- changes the background color of the language section (sccs change),
- uses "Suche" instead of "Einfache Suche" as title above the main search input field (i18n change) and
- replaces the content of uni-basel-logo.svg with the simple text "Afrikaportal" (img change).

Because of technical limitiations code changes of rdv_viewer/rdv-ws/projects/ are not immediately available in rdv-app. After a change in rdv_viewer:

1. ctrl-c the rdv-app
2. yarn run upd-lib # and/or "yarn run upd-mirador"
3. yarn run start

Only a little more convenient, but not essentially faster and not well tested:

```
npx nodemon --watch ../rdv_viewer -e ts --delay 200ms --exec 'yarn run upd-lib && ng serve -c zas_int-out --host 0.0.0.0 --port 4300 --live-reload=false'
```

The second command will run `yarn run upd-lib` on *.ts changes in rdv_viewer followed by a re-start of rdv-app. Unfortunately `ng serve` isn't picking up newer versions of rdv_viewer even if the project's version changes, so a re-start is necessary. Finally reload the page in your browser.

END
